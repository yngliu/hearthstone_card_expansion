#!/usr/bin/env python3

################################################################################
# "Visual Studio Code" auto-completes dict names in global scope               #
################################################################################

legacy                  = 0
naxxramas               = 1
goblin_vs_gnomes        = 2
blackrock_mountains     = 3
grand_tournament        = 4
league_of_explorers     = 5
old_gods                = 6
karazhan                = 7
gadgetzan               = 8
un_goro                 = 9
frozen_throne           = 10
kobolds                 = 11
witchwood               = 12
boomsday                = 13
rumble                  = 14
rise_of_shadows         = 15
uldum                   = 16
dragons                 = 17
galakronds_awakening    = 18
initiate                = 19
outland                 = 20
scholomance             = 21
darkmoon                = 22
barrens                 = 23
stormwind               = 24
alterac_valley          = 25
caverns                 = 26
core                    = 27
sunken_city             = 28
castle_nathria          = 29
path_of_arthas          = 30
lich_king               = 31
festival                = 32
titans                  = 33
badlands                = 34
event                   = 35
whizbangs_workshop      = 36

# <-- minimize cards here!
cards = {
# death knight
    "Lord Marrowgar":               legacy,

# demon hunter
    "Shadowhoof Slayer":            legacy,
    "Chaos Strike":                 legacy,
    "Kor'vas Bloodthorn":           legacy,
    "Sightless Watcher":            legacy,

    "Aldrachi Warblades":           legacy,
    "Coordinated Strike":           legacy,
    "Satyr Overseer":               legacy,
    "Soul Cleave":                  legacy,

    "Chaos Nova":                   legacy,
    "Glaivebound Adept":            legacy,
    "Inner Demon":                  legacy,

# druid
    "Innervate":                    legacy,
    "Moonfire":                     legacy,
    "Claw":                         legacy,
    "Naturalize":                   legacy,

    "Savagery":                     legacy,
    "Mark of the Wild":             legacy,
    "Power of the Wild":            legacy,
    "Wild Growth":                  legacy,
 
    "Wrath":                        legacy,
    "Healing Touch":                legacy,
    "Mark of Nature":               legacy,
    "Savage Roar":                  legacy,

    "Soul of the Forest":           legacy,
    "Swipe":                        legacy,
    "Bite":                         legacy,
    "Keeper of the Grove":          legacy,

    "Nordrassil Druid":             legacy,
    "Force of Nature":              legacy,
    "Nourish":                      legacy,
    "Starfall":                     legacy,

    "Druid of the Claw":            legacy,
    "Starfire":                     legacy,
    "Ancient of Lore":              legacy,
    "Ancient of War":               legacy,

    "Cenarius":                     legacy,
    "Gift of the Wild":             legacy,
    "Ironbark Protector":           legacy,

# hunter
    "Arcane Shot":                  legacy,
    "Bestial Wrath":                legacy,
    "Flare":                        legacy,
    "Hunter's Mark":                legacy,

    "Timber Wolf":                  legacy,
    "Tracking":                     legacy,
    "Explosive Trap":               legacy,
    "Freezing Trap":                legacy,

    "Misdirection":                 legacy,
    "Scavenging Hyena":             legacy,
    "Snake Trap":                   legacy,
    "Snipe":                        legacy,

    "Starving Buzzzard":            legacy,
    "Animal Companion":             legacy,
    "Deadly Shot":                  legacy,
    "Eaglehorn Bow":                legacy,

    "Kill Command":                 legacy,
    "Unleash the Hounds":           legacy,
    "Houndmaster":                  legacy,
    "Multi-Shot":                   legacy,

    "Explosive Shot":               legacy,
    "Tundra Rhino":                 legacy,
    "Savannah Highmane":            legacy,
    "Gladiator's Longbow":          legacy,

    "King Krush":                   legacy,

# mage
    "Arcane Missiles":              legacy,
    "Ice Lance":                    legacy,
    "Mana Wyrm":                    legacy,
    "Mirror Image":                 legacy,

    "Tome of Intellect":            legacy,
    "Arcane Explosion":             legacy,
    "Frostbolt":                    legacy,
    "Icicle":                       legacy,

    "Arcane Intellect":             legacy,
    "Cone of Cold":                 legacy,
    "Counterspell":                 legacy,
    "Frost Nova":                   legacy,

    "Ice Barrier":                  legacy,
    "Ice Block":                    legacy,
    "Kirin Tor Mage":               legacy,
    "Mirror Entity":                legacy,

    "Spellbender":                  legacy,
    "Vaporize":                     legacy,
    "Ethereal Arcanist":            legacy,
    "Fireball":                     legacy,

    "Polymorph":                    legacy,
    "Sorcerer's Apprentice":        legacy,
    "Water Element":                legacy,
    "Blizzard":                     legacy,

    "Archmage Antonidas":           legacy,
    "Flamestrike":                  legacy,
    "Pyroblast":                    legacy,

# paladin
    "Blessing of Might":            legacy,
    "Blessing of Wisdom":           legacy,
    "Eye for an Eye":               legacy,
    "Hand of Protection":           legacy,

    "Humility":                     legacy,
    "Light's Justice":              legacy,
    "Noble Sacrifice":              legacy,
    "Reckoning":                    legacy,

    "Redemption":                   legacy,
    "Repeniance":                   legacy,
    "Argent Protection":            legacy,
    "Equality":                     legacy,

    "Holy Light":                   legacy,
    "Pursuit of Justice":           legacy,
    "Aldon Peacekeeper":            legacy,
    "Consecration":                 legacy,

    "Divine Favor":                 legacy,
    "Hammer of Wrath":              legacy,
    "Sword of Justice":             legacy,
    "Blessing of Kings":            legacy,

    "Truesilver Champion":          legacy,
    "Blessed Champion":             legacy,
    "Holy Wrath":                   legacy,
    "Righteousness":                legacy,
 
    "Avenging Wrath":               legacy,
    "Lay of Hands":                 legacy,
    "Guardian of Lings":            legacy,
    "Tirion Fordring":              legacy,

# priest
    "Circle of Healing":            legacy,
    "Silence":                      legacy,
    "Crimson Clergy":               legacy,
    "Focesed Will":                 legacy,

    "Holy Smite":                   legacy,
    "Inner Fire":                   legacy,
    "Mind Vision":                  legacy,
    "Northshire Cleric":            legacy,

    "Power Word: Shield":           legacy,
    "Psychic Conjurer":             legacy,
    "Radiance":                     legacy,
    "Scarlet Subjugator":           legacy,

    "Divine Spirit":                legacy,
    "Kul Tiran Chaplain":           legacy,
    "Lightwell":                    legacy,
    "Mind Blast":                   legacy,

    "Shadow Word: Death":           legacy,
    "Shadow Word: Pain":            legacy,
    "Shadowform":                   legacy,
    "Thoughsteal":                  legacy,

    "Holy Nova":                    legacy,
    "Lightspawn":                   legacy,
    "Shadow Madness":               legacy,
    "Auchenai Soulpriest":          legacy,

    "Mass dispel":                  legacy,
    "Mindgames":                    legacy,
    "Power Infusion":               legacy,
    "Shadow Word: Ruin":            legacy,

    "Temple Enforcer":              legacy,
    "Cabal Shadow Priest":          legacy,
    "Holy Fire":                    legacy,
    "Prophet Velen":                legacy,

    "Natalie Seline":               legacy,
    "Mind Control":                 legacy,

# rogue
    "Backstab":                     legacy,
    "Preparation":                  legacy,
    "Shadowstep":                   legacy,
    "Cold Blood":                   legacy,

    "Conceal":                      legacy,
    "Deadly Poison":                legacy,
    "Pilfer":                       legacy,
    "Sinister Strike":              legacy,

    "Betrayal":                     legacy,
    "Blade Flurry":                 legacy,
    "Defias Ringleader":            legacy,
    "Eviscerate":                   legacy,

    "Fan of Knives":                legacy,
    "Patient Assassin":             legacy,
    "Sap":                          legacy,
    "Shiv":                         legacy,

    "Vanessa VanCleef":             legacy,
    "Edwin VanCleef":               legacy,
    "Headcrack":                    legacy,
    "Perdition's Blade":            legacy,

    "SI:7 Agent":                   legacy,
    "Assassinate":                  legacy,
    "Assassin's Blade":             legacy,
    "Master of Disguise":           legacy,

    "Plaguebringer":                legacy,
    "Sprint":                       legacy,
    "Kidnapper":                    legacy,
    "Vanish":                       legacy,

# shaman
    "Ancestral Healing":            legacy,
    "Totemic Might":                legacy,
    "Dust Devil":                   legacy,
    "Earth Shock":                  legacy,

    "Forked Lightning":             legacy,
    "Frost Shock":                  legacy,
    "Lightning Bolt":               legacy,
    "Ancestral Spirit":             legacy,
 
    "Flametongue Totem":            legacy,
    "Rockbiter Weapon":             legacy,
    "Stormforged Axe":              legacy,
    "Windfury":                     legacy,

    "Far Sight":                    legacy,
    "Feral Spirit":                 legacy,
    "Hex":                          legacy,
    "Lava Burst":                   legacy,

    "lighning Storm":               legacy,
    "Mana Tide Totem":              legacy,
    "Unbound Elemental":            legacy,
    "Windspeaker":                  legacy,

    "Bloodlust":                    legacy,
    "Doomhammer":                   legacy,
    "Earth Elemental":              legacy,
    "Fire Elemental":               legacy,
 
    "Al'Akir the Windlord":         legacy,

# warlock
    "Ritual of Doom":               legacy,
    "Sacrificial Pact":             legacy,
    "Blood Imp":                    legacy,
    "Call of the Void":             legacy,

    "Corruption":                   legacy,
    "Flame Imp":                    legacy,
    "Mortal Coil":                  legacy,
    "Power Overwhelming":           legacy,

    "Soulfire":                     legacy,
    "Voidwalker":                   legacy,
    "Demonfire":                    legacy,
    "Felstalker":                   legacy,

    "Drain Life":                   legacy,
    "Felguard":                     legacy,
    "Hellfire":                     legacy,
    "Sense Demons":                 legacy,

    "Shadow Bolt":                  legacy,
    "Void Terror":                  legacy,
    "Pit Lord":                     legacy,
    "Shadowflame":                  legacy,

    "Siphon Soul":                  legacy,
    "Summoning Portal":             legacy,
    "Bane of Doom":                 legacy,
    "Doomguard":                    legacy,

    "Felsoul Jailer":               legacy,
    "Dread Infernal":               legacy,
    "Enslaved Fel Lord":            legacy,
    "Siegebreaker":                 legacy,

    "Lord Jaraxxus":                legacy,
    "Twisting Nether":              legacy,

# warrior
    "Inner Rage":                   legacy,
    "Bloodsail Deckhand":           legacy,
    "Execute":                      legacy,
    "Shield Slam":                  legacy,

    "Slam":                         legacy,
    "Upgrade!":                     legacy,
    "Whirlwind":                    legacy,
    "Armorsmith":                   legacy,

    "Battle Rage":                  legacy,
    "Cleave":                       legacy,
    "Commanding Shout":             legacy,
    "Cruel Taskmaster":             legacy,

    "Fiery War Axe":                legacy,
    "Heroic Strike":                legacy,
    "Rampage":                      legacy,
    "Shield Block":                 legacy,

    "Charge":                       legacy,
    "Frothing Berserker":           legacy,
    "War Cache":                    legacy,
    "Warsong Commander":            legacy,

    "Arathi Weaponsmith":           legacy,
    "Kor'kron Elite":               legacy,
    "Mortal Strike":                legacy,
    "Warsong Outrider":             legacy,

    "Arcanite Reaper":              legacy,
    "Brawl":                        legacy,
    "Gorehowl":                     legacy,
    "Grommash Hellscream":          legacy,

# neutral
    "Wisp":                         legacy,
    "Abusive Sergeant":             legacy,
    "Angry Chicken":                legacy,
    "Argent Squire":                legacy,

    "Bloodsail Corsair":            legacy,
    "Elven Archer":                 legacy,
    "Goldshire Footman":            legacy,
    "Grimscale Oracle":             legacy,

    "Hungry Crab":                  legacy,
    "Leper Gnome":                  legacy,
    "Lightwarden":                  legacy,
    "Murloc Raider":                legacy,

    "Murloc Tidecaller":            legacy,
    "Secretkeeper":                 legacy,
    "Shieldbearer":                 legacy,
    "Southsea Deckhand":            legacy,

    "Stonetusk Boar":               legacy,
    "Voodoo Doctor":                legacy,
    "Worgen Infiltrator":           legacy,
    "Young Dragonhawk":             legacy,

    "Young Priestess":              legacy,
    "Acidic Swamp Ooze":            legacy,
    "Amani Berserker":              legacy,
    "Ancient Watcher":              legacy,

    "Bloodfen Raptor":              legacy,
    "Bloodmage Thalnos":            legacy,
    "Bloodsail Raider":             legacy,
    "Bluegill Warrior":             legacy,

    "Captain's Parrot":             legacy,
    "Crazed Alchemist":             legacy,
    "Dire Wolf Alpha":              legacy,
    "Doomsayer":                    legacy,

    "Faerie Dragon":                legacy,
    "Frostwolf Grunt":              legacy,
    "Knife Juggler":                legacy,
    "Kobold Geomancer":             legacy,

    "Lost Hoarder":                 legacy,
    "Lorewalker Cho":               legacy,
    "Mad Bomber":                   legacy,
    "Mana Addict":                  legacy,

    "Mana Wraith":                  legacy,
    "Master Swordsmith":            legacy,
    "Millhouse Manastorm":          legacy,
    "Murloc Tidehunter":            legacy,

    "Nat Pagle":                    legacy,
    "Novice Engineer":              legacy,
    "Pint-Sized Summoner":          legacy,
    "River Crocolisk":              legacy,

    "Sunfury Protector":            legacy,
    "Wild Pyromancer":              legacy,
    "Youthful Brewmaster":          legacy,
    "Acolyte of Pain":              legacy,

    "Alarm-o-Bot":                  legacy,
    "Arcane Golem":                 legacy,
    "Blood Knight":                 legacy,
    "Brightwing":                   legacy,

    "Coldlight Oracle":             legacy,
    "Coldlight Seer":               legacy,
    "Dalaran Mage":                 legacy,
    "Demolisher":                   legacy,

    "Earthen Ring Farseer":         legacy,
    "Emperor Cobra":                legacy,
    "Flesheating Ghoul":            legacy,
    "Harvest Golem":                legacy,

    "Imp Master":                   legacy,
    "Injured Blademaster":          legacy,
    "Ironbeak Owl":                 legacy,
    "Ironforge Rifleman":           legacy,

    "Ironfur Grizzly":              legacy,
    "Jungle Panther":               legacy,
    "King Mukla":                   legacy,
    "Magma Rager":                  legacy,

    "Murloc Warleader":             legacy,
    "Questing Adventurer":          legacy,
    "Raging Wargen":                legacy,
    "Raid Leader":                  legacy,

    "Razorfen Hunter":              legacy,
    "Scarlet Crusader":             legacy,
    "Shattered Sun Cleric":         legacy,
    "Silverback Patriarch":         legacy,

    "Southsea Captain":             legacy,
    "Tauren Warrior":               legacy,
    "Thrallmar Farsee":             legacy,
    "Tinkmaster Overspark":         legacy,

    "Wolfrider":                    legacy,
    "Ancient Brewmaster":           legacy,
    "Ancient Mage":                 legacy,
    "Big Game Hunter":              legacy,

    "Chillwind Yeti":               legacy,
    "Cult Master":                  legacy,
    "Dark Iron Dwarf":              legacy,
    "Defender of Argus":            legacy,

    "Dragonling Mechanic":          legacy,
    "Dread Corsair":                legacy,
    "Footman":                      legacy,
    "Gnomish Inventor":             legacy,

    "Mogu'shan Warden":             legacy,
    "Oasis Snapjaw":                legacy,
    "Ogre Magi":                    legacy,
    "One Murk-Eye":                 legacy,

    "Sen'jin Shieldmasta":          legacy,
    "SI:7 Infiltrator":             legacy,
    "Silvermoon Guardian":          legacy,
    "Spellbreaker":                 legacy,

    "Stormwind Knight":             legacy,
    "The Black Knight":             legacy,
    "Twilight Drake":               legacy,
    "Violet Teacher":               legacy,

    "Abomination":                  legacy,
    "Azure Drake":                  legacy,
    "Booty Bay Bodyguard":          legacy,
    "Captain Greenskin":            legacy,

    "Darkscale Healer":             legacy,
    "Elite Tauren Chieftain":       legacy,
    "Faceless Manipulator":         legacy,
    "Fen Creeper":                  legacy,

    "Frostwolf Warlord":            legacy,
    "Gurubashi Berserker":          legacy,
    "Harrison Jones":               legacy,
    "Leeroy Jenkins":               legacy,

    "Mind Control Tech":            legacy,
    "Night Elf Huntress":           legacy,
    "Nightblade":                   legacy,
    "Silver Hand Knight":           legacy,

    "Spiteful Smith":               legacy,
    "Stampeding Kodo":              legacy,
    "Stormpike Commando":           legacy,
    "Stranglethorn Tiger":          legacy,

    "Venture Co. Mercenary":        legacy,
    "Warsong Grunt":                legacy,
    "Archmage":                     legacy,
    "Argent Commander":             legacy,

    "Boulderfist Ogre":             legacy,
    "Cairne Bloodhoof":             legacy,
    "Frost Elemental":              legacy,
    "Gelbin Mekkatorque":           legacy,

    "Gnomelia, S.A.F.E. Pilot":     legacy,
    "High Inquisitor Whitemane":    legacy,
    "hogger":                       legacy,
    "Lord of the Arena":            legacy,
 
    "Priestess of Elune":           legacy,
    "Reckless Rocketeer":           legacy,
    "Sunwalker":                    legacy,
    "Sylvanas Windrunner":          legacy,

    "The Beast":                    legacy,
    "Windfury Harpy":               legacy,
    "Xavius":                       legacy,
    "Baron Geddon":                 legacy,

    "Barrens Stablehand":           legacy,
    "Core Hound":                   legacy,
    "Gadgetzen Auctioneer":         legacy,
    "Ravenholdt Assassin":          legacy,

    "Stormwind Champion":           legacy,
    "War Golem":                    legacy,
    "Arcane Devourer":              legacy,
    "Gruul":                        legacy,

    "Ragnaros the Firelord":        legacy,
    "Alexstrasza":                  legacy,
    "Malygos":                      legacy,
    "Nozdormu":                     legacy,

    "Onyxia":                       legacy,
    "Ysera":                        legacy,
    "Deathwing":                    legacy,
    "Sea Giant":                    legacy,

    "Mountain Giant":               legacy,
    "Molten Giant":                 legacy,

# druid
    "Poison Seeds":                 naxxramas,

# hunter
    "Webspinner":                   naxxramas,

# mage
    "Duplicate":                    naxxramas,

# paladin
    "Avenge":                       naxxramas,

# priest
    "Dark Cultist":                 naxxramas,

# rogue
    "Anub'ar Ambusher":             naxxramas,

# shaman
    "Reincarnate":                  naxxramas,

# warlock
    "Voidcaller":                   naxxramas,

# warrior
    "Death's Bite":                 naxxramas,

# neutral
    "Undertaker":                   naxxramas,
    "Zombie Chow":                  naxxramas,
    "Echoing Ooze":                 naxxramas,
    "Haunted Creeper":              naxxramas,

    "Mad Scientist":                naxxramas,
    "Nerub'ar Weblord":             naxxramas,
    "Nerubian Egg":                 naxxramas,
    "Unstable Ghost":               naxxramas,

    "Dancing Swords":               naxxramas,
    "Deathlord":                    naxxramas,
    "Shade of Naxxramas":           naxxramas,
    "Stoneskin Gargoyle":           naxxramas,

    "Baron Rivendare":              naxxramas,
    "Wailing Soul":                 naxxramas,
    "Feugen":                       naxxramas,
    "Loatheb":                      naxxramas,

    "Sludge Belcher":               naxxramas,
    "Spectral Knight":              naxxramas,
    "Stalagg":                      naxxramas,
    "Maexxna":                      naxxramas,

    "Kel'thuzad":                   naxxramas,

# druid
    "Anodized Robo Cub":            goblin_vs_gnomes,
    "Grove Tender":                 goblin_vs_gnomes,
    "Druid of the Fang":            goblin_vs_gnomes,
    "Dark Wispers":                 goblin_vs_gnomes,

    "Mech-Bear-Cat":                goblin_vs_gnomes,
    "Recycle":                      goblin_vs_gnomes,
    "Malorne":                      goblin_vs_gnomes,
    "Tree of Life":                 goblin_vs_gnomes,

# hunter
    "Call Pet":                     goblin_vs_gnomes,
    "Feign Death":                  goblin_vs_gnomes,
    "Glaivezooka":                  goblin_vs_gnomes,
    "Steamwheedle Sniper":          goblin_vs_gnomes,

    "King of Beasts":               goblin_vs_gnomes,
    "Metaltooth Leaper":            goblin_vs_gnomes,
    "Cobra Shot":                   goblin_vs_gnomes,
    "Gahz'rilla":                   goblin_vs_gnomes,

# mage
    "Flamecannon":                  goblin_vs_gnomes,
    "Snowchugger":                  goblin_vs_gnomes,
    "Unstable Portal":              goblin_vs_gnomes,
    "Soot Spewer":                  goblin_vs_gnomes,

    "Echo of Medivh":               goblin_vs_gnomes,
    "Goblin Blastmage":             goblin_vs_gnomes,
    "Wee Spellstopper":             goblin_vs_gnomes,
    "Flame Leviathan":              goblin_vs_gnomes,

# paladin
    "Seal of Light":                goblin_vs_gnomes,
    "Shielded Minibot":             goblin_vs_gnomes,
    "Coghammer":                    goblin_vs_gnomes,
    "Muster for Battle":            goblin_vs_gnomes,

    "Scarlet Purifier":             goblin_vs_gnomes,
    "Bolvar Fordragon":             goblin_vs_gnomes,
    "Cobalt Guardian":              goblin_vs_gnomes,
    "Quartermaster":                goblin_vs_gnomes,

# priest
    "Light of the Naaru":           goblin_vs_gnomes,
    "Shadowbomber":                 goblin_vs_gnomes,
    "Shadowboxer":                  goblin_vs_gnomes,
    "Shrinkmeister":                goblin_vs_gnomes,

    "Velen's Chosen":               goblin_vs_gnomes,
    "Upgraded Repair Bot":          goblin_vs_gnomes,
    "Vol'jin":                      goblin_vs_gnomes,
    "Lightbomb":                    goblin_vs_gnomes,

# rogue
    "Goblin Auto-Barber":           goblin_vs_gnomes,
    "One-eyed Cheat":               goblin_vs_gnomes,
    "Cogmaster's Wrench":           goblin_vs_gnomes,
    "Iron Sensei":                  goblin_vs_gnomes,

    "Sabotage":                     goblin_vs_gnomes,
    "Tinker's Sharpsword Oil":      goblin_vs_gnomes,
    "Ogre Ninja":                   goblin_vs_gnomes,
    "Trade Prince Gallywix":        goblin_vs_gnomes,

# shaman
    "Crackle":                      goblin_vs_gnomes,
    "Vitality Totem":               goblin_vs_gnomes,
    "Whirling Zap-o-matic":         goblin_vs_gnomes,
    "Powermace":                    goblin_vs_gnomes,

    "Ancestor's Call":              goblin_vs_gnomes,
    "Dunemaul Shaman":              goblin_vs_gnomes,
    "Silifin Spiritwalker":         goblin_vs_gnomes,
    "Neptulon":                     goblin_vs_gnomes,

# warlock
    "Darkbomb":                     goblin_vs_gnomes,
    "Queem of Pain":                goblin_vs_gnomes,
    "Fel Cannon":                   goblin_vs_gnomes,
    "Imp-losion":                   goblin_vs_gnomes,

    "Demonheart":                   goblin_vs_gnomes,
    "Floating Watcher":             goblin_vs_gnomes,
    "Anima Golem":                  goblin_vs_gnomes,
    "Mal'ganis":                    goblin_vs_gnomes,

# warrior
    "Warbot":                       goblin_vs_gnomes,
    "Bouncing Blade":               goblin_vs_gnomes,
    "Ogre Warmaul":                 goblin_vs_gnomes,
    "Screwjank Clunker":            goblin_vs_gnomes,

    "Shieldmaiden":                 goblin_vs_gnomes,
    "Siege engine":                 goblin_vs_gnomes,
    "Iron Juggernaut":              goblin_vs_gnomes,
    "Crush":                        goblin_vs_gnomes,

# neutral
    "Target Dummy":                 goblin_vs_gnomes,
    "Clockwork Gnome":              goblin_vs_gnomes,
    "Cogmaster":                    goblin_vs_gnomes,
    "Annoy-o-Tron":                 goblin_vs_gnomes,

    "Explosive Sheep":              goblin_vs_gnomes,
    "Gilblin Stalker":              goblin_vs_gnomes,
    "Micro Machine":                goblin_vs_gnomes,
    "Puddlestomper":                goblin_vs_gnomes,

    "Recombobulator":               goblin_vs_gnomes,
    "Ship's Cannon":                goblin_vs_gnomes,
    "Stonesplinter Trogg":          goblin_vs_gnomes,
    "Flying Maching":               goblin_vs_gnomes,

    "Gnomeregan Infanitry":         goblin_vs_gnomes,
    "Gnomish Experimenter":         goblin_vs_gnomes,
    "Goblin Sapper":                goblin_vs_gnomes,
    "Hobgoblim":                    goblin_vs_gnomes,

    "Olluminator":                  goblin_vs_gnomes,
    "Lil' Exorcist":                goblin_vs_gnomes,
    "Mini-Mage":                    goblin_vs_gnomes,
    "Ogre Brute":                   goblin_vs_gnomes,

    "Spider-Tank":                  goblin_vs_gnomes,
    "Tinkertown Technician":        goblin_vs_gnomes,
    "Arcane Nullifier X-21":        goblin_vs_gnomes,
    "Burly Rockjaw Trogg":          goblin_vs_gnomes,

    "Enhance-o Mechano":            goblin_vs_gnomes,
    "Jeeves":                       goblin_vs_gnomes,
    "Kezan Mystic":                 goblin_vs_gnomes,
    "Lost Tallstrider":             goblin_vs_gnomes,

    "Mechanical Yeti":              goblin_vs_gnomes,
    "Mechwarper":                   goblin_vs_gnomes,
    "Piloted Shredder":             goblin_vs_gnomes,
    "Antique Healbot":              goblin_vs_gnomes,

    "Blingtron 3000":               goblin_vs_gnomes,
    "Bomb Lobber":                  goblin_vs_gnomes,
    "Fel Reaver":                   goblin_vs_gnomes,
    "Hemet Nesingwary":             goblin_vs_gnomes,

    "Junkbot":                      goblin_vs_gnomes,
    "Madder Bomber":                goblin_vs_gnomes,
    "Mimiron's Head":               goblin_vs_gnomes,
    "Salty Dog":                    goblin_vs_gnomes,

    "Gazlowe":                      goblin_vs_gnomes,
    "Mogor the Ogre":               goblin_vs_gnomes,
    "Piloted Skygolem":             goblin_vs_gnomes,
    "Toshley":                      goblin_vs_gnomes,

    "Dr. Boom":                     goblin_vs_gnomes,
    "Troggzor the Earthinator":     goblin_vs_gnomes,
    "Foe Reaper 4000":              goblin_vs_gnomes,
    "Force-Tank MAX":               goblin_vs_gnomes,

    "Sneed's Old Shredder":         goblin_vs_gnomes,
    "Mekgineerthermaplugg":         goblin_vs_gnomes,
    "Clockwork Giant":              goblin_vs_gnomes,

# druid
    "Druid of the Flame":           blackrock_mountains,
    "Volcanic Lumberer":            blackrock_mountains,

# hunter
    "Quick Shot":                   blackrock_mountains,
    "Core Rager":                   blackrock_mountains,

# mage
    "Flamewaker":                   blackrock_mountains,
    "Dragon's Breath":              blackrock_mountains,

# paladin
    "Dragon Consort":               blackrock_mountains,
    "Solemn Vigil":                 blackrock_mountains,

# priest
    "Twilight Whelp":               blackrock_mountains,
    "Resurrect":                    blackrock_mountains,

# rogue
    "Gang Up":                      blackrock_mountains,
    "Dark Iron Skulker":            blackrock_mountains,

# shamen
    "Lava Shock":                   blackrock_mountains,
    "Fireguard Destroyer":          blackrock_mountains,

# warlock
    "Demonwrath":                   blackrock_mountains,
    "Imp Gang Boss":                blackrock_mountains,

# warrior
    "Revenge":                      blackrock_mountains,
    "Axe Flinger":                  blackrock_mountains,

# neutral
    "Dragon Egg":                   blackrock_mountains,
    "Blackwing Technician":         blackrock_mountains,
    "Dragonkin Sorcerer":           blackrock_mountains,
    "Hungry Dragon":                blackrock_mountains,

    "Blackwing Corruptor":          blackrock_mountains,
    "Emperor Thaurissan":           blackrock_mountains,
    "Grim Patron":                  blackrock_mountains,
    "Drakonid Crusher":             blackrock_mountains,

    "Volcanic Drake":               blackrock_mountains,
    "Rend Blackhand":               blackrock_mountains,
    "Chromaggus":                   blackrock_mountains,
    "Majordomo Executus":           blackrock_mountains,

    "Nefarian":                     blackrock_mountains,

# druid
    "Living Roots":                 grand_tournament,
    "Darnassus Aspirant":           grand_tournament,
    "Druid of the Saber":           grand_tournament,
    "Mulch":                        grand_tournament,
 
    "Astral Communion":             grand_tournament,
    "Savage Combatant":             grand_tournament,
    "Wildwalker":                   grand_tournament,
    "Knight of the Wild":           grand_tournament,

    "Aviana":                       grand_tournament,

# hunter
    "Lock and Load":                grand_tournament,
    "Brave Archer":                 grand_tournament,
    "Bear Trap":                    grand_tournament,
    "King's Elekk":                 grand_tournament,

    "Acidmaw":                      grand_tournament,
    "Ball of Spiders":              grand_tournament,
    "Dreadscale":                   grand_tournament,
    "Powershot":                    grand_tournament,

    "Stablemaster":                 grand_tournament,
    "Ram Wrangler":                 grand_tournament,

# mage
    "Arcane Blast":                 grand_tournament,
    "Fallen Hero":                  grand_tournament,
    "Effigy":                       grand_tournament,
    "Polymorph: Boar":              grand_tournament,

    "Spellslinger":                 grand_tournament,
    "Dalaran Aspirant":             grand_tournament,
    "Flame Lance":                  grand_tournament,
    "Coldarra Drake":               grand_tournament,

    "Rhonin":                       grand_tournament,

# paladin
    "Competitive Spirit":           grand_tournament,
    "Argent Lance":                 grand_tournament,
    "Enter the Colliseum":          grand_tournament,
    "Seal of Champions":            grand_tournament,

    "Warhorse Trainer":             grand_tournament,
    "Murloc Knight":                grand_tournament,
    "Mysterious Challenger":        grand_tournament,
    "Tuskarr Jouster":              grand_tournament,

    "Eadric the Pure":              grand_tournament,

# priest
    "Flash Heal":                   grand_tournament,
    "Power Word: Glory":            grand_tournament,
    "Confuse":                      grand_tournament,
    "Holy Champion":                grand_tournament,

    "Shadowfiend":                  grand_tournament,
    "Wyrmrest Agent":               grand_tournament,
    "Convert":                      grand_tournament,
    "Spawn of Shadows":             grand_tournament,

# rogue
    "Buccaneer":                    grand_tournament,
    "Cutpurse":                     grand_tournament,
    "Poisoned Blade":               grand_tournament,
    "Undercity Valiant":            grand_tournament,

    "Beneath the Grounds":          grand_tournament,
    "Burgle":                       grand_tournament,
    "Shady Dealer":                 grand_tournament,
    "Shado-Pan Rider":              grand_tournament,

# shaman
    "Ancestral Knowledge":          grand_tournament,
    "Totem Golem":                  grand_tournament,
    "Tuskarr Totemic":              grand_tournament,
    "Charged Hammer":               grand_tournament,

    "Elemental Destruction":        grand_tournament,
    "Healing Wave":                 grand_tournament,
    "Draenei Totemcarver":          grand_tournament,
    "Thunder Bluff Valiant":        grand_tournament,

# warlock
    "Demenfuse":                    grand_tournament,
    "Tiny Knight of Evil":          grand_tournament,
    "Wrathguard":                   grand_tournament,
    "Dark Bargain":                 grand_tournament,

    "Dreadsteed":                   grand_tournament,
    "Fist of Jaraxxus":             grand_tournament,
    "Void Crusher":                 grand_tournament,
    "Wilfred Fizzlebang":           grand_tournament,

    "Fearsome Doomguard":           grand_tournament,

# warrior
    "Alexstrasza's Champion":       grand_tournament,
    "Bash":                         grand_tournament,
    "Bolster":                      grand_tournament,
    "Sparring Partner":             grand_tournament,

    "King's Defender":              grand_tournament,
    "Orgrimmar Aspirant":           grand_tournament,
    "Magnataur Alpha":              grand_tournament,
    "Sea Reaver":                   grand_tournament,

    "Varian Wrynn":                 grand_tournament,

# neutral
    "Gadgetzan Jouster":            grand_tournament,
    "Injured Kvaldir":              grand_tournament,
    "Lowly Squire":                 grand_tournament,
    "Tournament Attendee":          grand_tournament,

    "Argent Watchman":              grand_tournament,
    "Boneguard Lieutenant":         grand_tournament,
    "Flame Juggler":                grand_tournament,
    "Garrison Commmander":          grand_tournament,

    "Lance Carrier":                grand_tournament,
    "Argent Horserider":            grand_tournament,
    "Coliseum Manager":             grand_tournament,
    "Dragonhawk Rider":             grand_tournament,

    "Eydis Darkbane":               grand_tournament,
    "Fencing Coach":                grand_tournament,
    "Fjola Lightbase":              grand_tournament,
    "Ice Rager":                    grand_tournament,

    "Light's Champion":             grand_tournament,
    "Master of Ceremonies":         grand_tournament,
    "Saboteur":                     grand_tournament,
    "Silent Knight":                grand_tournament,

    "Silver Hand Regent":           grand_tournament,
    "Armour Warhorse":              grand_tournament,
    "Crowd Favorite":               grand_tournament,
    "Evil Heckler":                 grand_tournament,

    "Frigid Snobold":               grand_tournament,
    "Gromok the Impaler":           grand_tournament,
    "Maiden of the Lake":           grand_tournament,
    "Refreshment Vendor":           grand_tournament,

    "Tournament Medic":             grand_tournament,
    "Twilight Guardian":            grand_tournament,
    "Clockwork Knight":             grand_tournament,
    "Justicar Trueheart":           grand_tournament,

    "Kvaldir Raider":               grand_tournament,
    "Mukla's Champion":             grand_tournament,
    "Nexus-Champion Saraad":        grand_tournament,
    "Pit Fighter":                  grand_tournament,

    "Recruiter":                    grand_tournament,
    "Bolf Ramshield":               grand_tournament,
    "Grand Crusader":               grand_tournament,
    "Kodorider":                    grand_tournament,

    "Master Jouster":               grand_tournament,
    "Mogor's Champion":             grand_tournament,
    "Sideshow Spelleater":          grand_tournament,
    "The Skeleton Knight":          grand_tournament,

    "Captured Jormungar":           grand_tournament,
    "Chillmaw":                     grand_tournament,
    "Skycap'n Kragg":               grand_tournament,
    "Icehowl":                      grand_tournament,

    "North Sea Kraken":             grand_tournament,
    "Frost Giant":                  grand_tournament,

# druid
    "Raven Idol":                   league_of_explorers,
    "Mounted Raptor":               league_of_explorers,
    "Jungle Moonkin":               league_of_explorers,

# hunter
    "Explorer's Hat":               league_of_explorers,
    "Dart Trap":                    league_of_explorers,
    "Desert Camel":                 league_of_explorers,

# mage
    "Forgotten Torch":              league_of_explorers,
    "Animated Armor":               league_of_explorers,
    "Ethereal Conjurer":            league_of_explorers,

# paladin
    "Sacred Trial":                 league_of_explorers,
    "Keeper of Uldaman":            league_of_explorers,
    "Anyfin Cab Happen":            league_of_explorers,

# priest
    "Museum Curator":               league_of_explorers,
    "Excavated Evil":               league_of_explorers,
    "Entomb":                       league_of_explorers,

# rogue
    "Pit Snake":                    league_of_explorers,
    "Unearthed Raptor":             league_of_explorers,
    "Tomb Pillager":                league_of_explorers,

# shamen
    "Tunnel Trogg":                 league_of_explorers,
    "Rumbling Elemental":           league_of_explorers,
    "Everyfin is Awesome":          league_of_explorers,

# warlock
    "Reliquary Seeker":             league_of_explorers,
    "Curse of Rafaam":              league_of_explorers,
    "Dark Peddler":                 league_of_explorers,

# warrior
    "Cursed Blade":                 league_of_explorers,
    "Fierce Monkey":                league_of_explorers,
    "Obsidian Destroyer":           league_of_explorers,

# neutral
    "Murloc Tinyfin":               league_of_explorers,
    "Sir Finley Mrrgglton":         league_of_explorers,
    "Huge Toad":                    league_of_explorers,
    "Jeweled Scarab":               league_of_explorers,

    "Brann Bronzebeard":            league_of_explorers,
    "Gorillabot A-3":               league_of_explorers,
    "Ancient Shade":                league_of_explorers,
    "Eerie Statue":                 league_of_explorers,

    "Elise Starseeker":             league_of_explorers,
    "Tomb Spider":                  league_of_explorers,
    "Anubisath Sentinel":           league_of_explorers,
    "Djinni of Zephyrs":            league_of_explorers,

    "Summoning Stone":              league_of_explorers,
    "Reno Jackson":                 league_of_explorers,
    "Wobbling Runts":               league_of_explorers,
    "Fossilized Devilsaur":         league_of_explorers,

    "Naga Sea Witch":               league_of_explorers,
    "Arch-Thief Rafaam":            league_of_explorers,

# druid
    "Forbidden Ancient":            old_gods,
    "Addled Grizzly":               old_gods,
    "Mark of Y'Shaarj":             old_gods,
    "Feral Rage":                   old_gods,

    "Fandral Staghelm":             old_gods,
    "Klaxxi Amber-Weaver":          old_gods,
    "Mire Keeper":                  old_gods,
    "Dark Arakkoa":                 old_gods,

    "Wisps of the Old Gods":        old_gods,

# hunter
    "Fiery Bat":                    old_gods,
    "On the Hunt":                  old_gods,
    "Carrion Grub":                 old_gods,
    "Forlorn Stalker":              old_gods,

    "Infest":                       old_gods,
    "Infested Wolf":                old_gods,
    "Princess Huhuran":             old_gods,
    "Call of the Wild":             old_gods,

    "Giant Sand Worm":              old_gods,

# mage
    "Forbidden Flame":              old_gods,
    "Cult Sorcerer":                old_gods,
    "Shatter":                      old_gods,
    "Twilight Flamecaller":         old_gods,

    "Cabalist's Tome":              old_gods,
    "Demented Frostcaller":         old_gods,
    "Servant of Yogg-Saron":        old_gods,
    "Faceless Summoner":            old_gods,

    "Anomalus":                     old_gods,

# paladin
    "Forbidden Healing":            old_gods,
    "Divine Strength":              old_gods,
    "Selfless Hero":                old_gods,
    "Vilefin Inquisitor":           old_gods,

    "A Light in the Darkness":      old_gods,
    "Rallying Blade":               old_gods,
    "Steward of Darkshine":         old_gods,
    "Stand Against Darkness":       old_gods,
 
    "Ragnaros, Lightlord":          old_gods,

# priest
    "Forbidden Shapimg":            old_gods,
    "Embrace the Shadow":           old_gods,
    "Hooded Acolyte":               old_gods,
    "Shadow Word: Horror":          old_gods,

    "Shifting Shade":               old_gods,
    "Darkshire Alchemist":          old_gods,
    "Power Word: Tentacles":        old_gods,
    "Twilight Darkmender":          old_gods,

    "Herald Volazj":                old_gods,

# rogue
    "Bladed Cultist":               old_gods,
    "Journey Below":                old_gods,
    "Undercity Huckster":           old_gods,
    "Shadow Strike":                old_gods,

    "Southsea Squidface":           old_gods,
    "Xaril, Poisoned Mind":         old_gods,
    "Shadowcaster":                 old_gods,
    "Thistle Tea":                  old_gods,

    "Blade of C'Thun":              old_gods,

# shaman
    "Evolve":                       old_gods,
    "Primal Fusion":                old_gods,
    "Eternal Sentinel":             old_gods,
    "Stormcrack":                   old_gods,

    "Flamewreathed Faceless":       old_gods,
    "Master of Evolution":          old_gods,
    "Hallazeal the Ascended":       old_gods,
    "Hammer of Twilight":           old_gods,

    "Thing from Below":             old_gods,

# warlock
    "Forbidden Ritual":             old_gods,
    "Possessd Villager":            old_gods,
    "Darkshire Librarian":          old_gods,
    "Renounce Darkness":            old_gods,

    "Darkshire Councilman":         old_gods,
    "Spreading Madness":            old_gods,
    "Usher of Souls":               old_gods,
    "Cho'gall":                     old_gods,

    "DOOM!":                        old_gods,

# warrior
    "Blood to Ichor":               old_gods,
    "N'Zoth's First Mate":            old_gods,
    "Bloody Warriors":              old_gods,
    "Bloodsail Cultist":            old_gods,

    "Ravaging Ghoul":               old_gods,
    "Bloodhoof Brave":              old_gods,
    "Tentacles for Arms":           old_gods,
    "Ancient Shieldbearer":         old_gods,
 
    "Malkorok":                     old_gods,

# neutral
    "Shifter Zerus":                old_gods,
    "Tentacle of N'Zoth":           old_gods,
    "Zealous Initiate":             old_gods,
    "Beckoner of Evil":             old_gods,

    "Bilefin Tidehunter":           old_gods,
    "Duskbear":                     old_gods,
    "Nat, the Darkfisher":          old_gods,
    "Twilight Geomancer":           old_gods,

    "Twisted Worgen":               old_gods,
    "Am'gam Rager":                 old_gods,
    "Disciple of C'Thun":           old_gods,
    "Silithid Swarmer":             old_gods,

    "Spawn of N'Zoth":              old_gods,
    "Squirming Tentacle":           old_gods,
    "Twilight Elder":               old_gods,
    "Aberrant Berserker":           old_gods,

    "Blackwater Pirate":            old_gods,
    "Crazed Worshipper":            old_gods,
    "C'thun's Chosen":              old_gods,
    "Cult Apothecary":              old_gods,

    "Cyclopian Horror":             old_gods,
    "Eater of Secrets":             old_gods,
    "Evolved Kobold":               old_gods,
    "Faceless Shambler":            old_gods,

    "Infested Tauren":              old_gods,
    "Midnight Drake":               old_gods,
    "Polluted Hoarder":             old_gods,
    "Twilight Summoner":            old_gods,

    "Corrupted Healbot":            old_gods,
    "Darkspeaker":                  old_gods,
    "Psych-o-Tron":                 old_gods,
    "Validated Doomsayer":          old_gods,

    "Ancient Harbinger":            old_gods,
    "Corrupted Seer":               old_gods,
    "Mukla, Tyrant of the Vale":    old_gods,
    "Nerubian Prophet":             old_gods,

    "Scaled Nightmare":             old_gods,
    "Skeram Cultist":               old_gods,
    "Bog Creeper":                  old_gods,
    "Grotesque Dragonhawk":         old_gods,

    "Hogger, Doom of Elwynn":       old_gods,
    "Twin emperor Vek'lor":         old_gods,
    "C'Thun":                       old_gods,
    "Doomcaller":                   old_gods,

    "Eldritch Horror":              old_gods,
    "The Boogeymonster":            old_gods,
    "Blood of the Ancient One":     old_gods,
    "Soggoth the Slitherer":        old_gods,

    "Deathwing, Dragonlord":        old_gods,
    "Faceless Behemoth":            old_gods,
    "N'Zoth, the Corruptor":        old_gods,
    "Yogg-Saron, Hope's End":       old_gods,

    "Y'Shaarj, Rage Unbound":       old_gods,

# druid
    "Enchanted Raven":              karazhan,
    "Menagerie Warden":             karazhan,
    "Moonglade Portal":             karazhan,

# hunter
    "Cat Trick":                    karazhan,
    "Kindly Grandmother":           karazhan,
    "Cloaked Huntress":             karazhan,

# mage
    "Babbling Book":                karazhan,
    "Medivh's Valet":               karazhan,
    "Firelands Portal":             karazhan,

# paladin
    "Nightbane Templar":            karazhan,
    "Silvermoon Portal":            karazhan,
    "Ivory Knight":                 karazhan,

# priest
    "Purify":                       karazhan,
    "Onyx Bishop":                  karazhan,
    "Priest of the Feast":          karazhan,

# rogue
    "Swashburglar":                 karazhan,
    "Deadly Fork":                  karazhan,
    "Ethereal Peddler":             karazhan,

# shaman
    "Spirit Claws":                 karazhan,
    "Maelstrom Portal":             karazhan,
    "Wicked Witchdoctor":           karazhan,

# warlock
    "Malchezaar's Imp":             karazhan,
    "Silverware Golem":             karazhan,
    "Kara Kazsham!":                karazhan,

# warrior
    "Protect the King!":            karazhan,
    "Ironforge Portal":             karazhan,
    "Fool's Bane":                  karazhan,

# neutral
    "Arcane Anomaly":               karazhan,
    "Runic Egg":                    karazhan,
    "Netherspite Historian":        karazhan,
    "Pompous Thespians":            karazhan,

    "Moroes":                       karazhan,
    "Pantry Spider":                karazhan,
    "Violet Illusionist":           karazhan,
    "Zoobot":                       karazhan,

    "Arcanosmith":                  karazhan,
    "Avian Watcher":                karazhan,
    "Barnes":                       karazhan,
    "Menagerie Magician":           karazhan,

    "Prince Malchezaar":            karazhan,
    "Book Wyrm":                    karazhan,
    "Moat Lurker":                  karazhan,
    "The Curator":                  karazhan,

    "Medivh, the Guardian":         karazhan,
    "Arcane Giant":                 karazhan,

# druid
    "Jade Idol":                    gadgetzan,
    "Mark of the Lotus":            gadgetzan,
    "Celestial Dreamer":            gadgetzan,
    "Jade Blossum":                 gadgetzan,

    "Lotus Agents":                 gadgetzan,
    "Pilfered Power":               gadgetzan,
    "Jade Spirit":                  gadgetzan,
    "Virmen Sensei":                gadgetzan,

    "Jade Behemoth":                gadgetzan,
    "Lunar Visions":                gadgetzan,
    "Aya Blackpaw":                 gadgetzan,
    "Kun the Forgotten King":       gadgetzan,

# hunter
    "Smuggler's Crate":             gadgetzan,
    "Alleycat":                     gadgetzan,
    "Grimestreet Informant":        gadgetzan,
    "Hidden Cache":                 gadgetzan,

    "Trogg Beastrager":             gadgetzan,
    "Grimestreet Smuggler":         gadgetzan,
    "Rat Pack":                     gadgetzan,
    "Shaky Zipgunner":              gadgetzan,

    "Dispatch Kodo":                gadgetzan,
    "Don Han'Cho":                  gadgetzan,
    "Knuckles":                     gadgetzan,
    "Piranha Launches":             gadgetzan,

# mage
    "Freezing Potion":              gadgetzan,
    "Kabal Lackey":                 gadgetzan,
    "Kabal Courier":                gadgetzan,
    "Manic Soulcaster":             gadgetzan,

    "Potion of Polymorph":          gadgetzan,
    "Volcanic Potion":              gadgetzan,
    "Kabal Chemist":                gadgetzan,
    "Kazakus":                      gadgetzan,

    "Cryomancer":                   gadgetzan,
    "Kabal Crystal Runner":         gadgetzan,
    "Greater Arcane Missiles":      gadgetzan,
    "Inkmaster Solia":              gadgetzan,

# paladin
    "Getaway Kodo":                 gadgetzan,
    "Grimscale Chum":               gadgetzan,
    "Meanstreet Mashal":            gadgetzan,
    "Smuggler's Run":               gadgetzan,

#    "Grimestreet Informant":        gadgetzan,
    "Grimestreet Outfitter":        gadgetzan,
#    "Grimestreet Smuggler":         gadgetzan,
    "Small-Time Recruits":          gadgetzan,

    "Wickerflame Burmbristle":      gadgetzan,
    "Grimestreet Enforcer":         gadgetzan,
#    "Don Han'Cho":                  gadgetzan,
    "Grimestreet Protector":        gadgetzan,

# priest
    "Pint-Size Potion":             gadgetzan,
    "Potion of Nadness":            gadgetzan,
#    "Kabal Courier":                gadgetzan,
    "Mana Geode":                   gadgetzan,

    "Kabal Talompriest":            gadgetzan,
    "Drakonid Operative":           gadgetzan,
    "Greater Healing Potion":       gadgetzan,
#    "Kabal Chemist":                gadgetzan,

#    "Kazakus":                      gadgetzan,
    "Dragonfire Potion":            gadgetzan,
    "Kabal Songstealer":            gadgetzan,
    "Raza the Chained":             gadgetzan,

# rogue
    "Counterfeit Coin":             gadgetzan,
    "Jade Swarmer":                 gadgetzan,
    "Gadgetzan Ferryman":           gadgetzan,
    "Jade Shuriken":                gadgetzan,

#    "Lotus Agents":                 gadgetzan,
    "Shadow Rager":                 gadgetzan,
    "Shaku, the Collector":         gadgetzan,
#    "Jade Spirit":                  gadgetzan,

    "Shadow Sensei":                gadgetzan,
    "Lotus Assassin":               gadgetzan,
#    "Aya Blackpaw":                 gadgetzan,
    "Luckydo Buccaneer":            gadgetzan,

# shaman
    "Finders Keepers":              gadgetzan,
    "Devolve":                      gadgetzan,
    "Jade Claws":                   gadgetzan,
    "Call in the Finishers":        gadgetzan,

    "Jade Lightning":               gadgetzan,
#    "Lotus Agents":                 gadgetzan,
#    "Jade Spirit":                  gadgetzan,
    "Jinyu Waterspeaker":           gadgetzan,

    "Lotus Illusionist":            gadgetzan,
    "White Eyes":                   gadgetzan,
#    "Aya Blackpaw":                 gadgetzan,
    "Jade Chieftain":               gadgetzan,

# warlock
#    "Kabal Courier":                gadgetzan,
    "Bloodfury Potion":             gadgetzan,
    "Unlicensed Apothecary":        gadgetzan,
    "Blastcrystal Potion":          gadgetzan,

    "Crystalweaver":                gadgetzan,
#    "Kabal Chemist":                gadgetzan,
#    "Kazakun":                      gadgetzan,
    "Seadevil Stinger":             gadgetzan,

    "Felfire Potion":               gadgetzan,
    "Kabal Trafficker":             gadgetzan,
    "Abyssal Enforcer":             gadgetzan,
    "Krul the Unshackled":          gadgetzan,    

# warrior
    "I Know a Guy":                 gadgetzan,
#    "Grimestreet Informant":        gadgetzan,
    "Public Defender":              gadgetzan,
    "Sleep with the Fishes":        gadgetzan,

    "Stolen Goods":                 gadgetzan,
    "Brass Knuckles":               gadgetzan,
    "Grimestreet Pawnbrokers":      gadgetzan,
#    "Grimestreet Smuggler":         gadgetzan,

    "Grimy Gadgeteer":              gadgetzan,
    "Hobart Grapplehammer":         gadgetzan,
    "Alley Armorsmith":             gadgetzan,
#    "Don Han'Cho":                  gadgetzan,

# neutral
    "Mistress of Mixtures":         gadgetzan,
    "Patches the Pirates":          gadgetzan,
    "Small-Time Buccaneer":         gadgetzan,
    "Weasel Tunneler":              gadgetzan,

    "Blowgill Sniper":              gadgetzan,
    "Dirty Rat":                    gadgetzan,
    "Friendly Bartender":           gadgetzan,
    "Gadgetzan Socialite":          gadgetzan,

    "Auctionmaster Beardo":         gadgetzan,
    "Backstreet Leper":             gadgetzan,
    "Blubber Baron":                gadgetzan,
    "Fel Orc Soulfiend":            gadgetzan,

    "Hired Gun":                    gadgetzan,
    "Sergeant Sally":               gadgetzan,
    "Street Trickster":             gadgetzan,
    "Toxic Sewer Ooza":             gadgetzan,

    "Backroom Bouncer":             gadgetzan,
    "Daring Reporter":              gadgetzan,
    "Genzo, the Shark":             gadgetzan,
    "Hozen Healer":                 gadgetzan,

    "Kooky Chemist":                gadgetzan,
    "Naga Corsair":                 gadgetzan,
    "Tanaris Hogchopper":           gadgetzan,
    "Worgen Greaser":               gadgetzan,

    "Bomb Squad":                   gadgetzan,
    "Burgly Bully":                 gadgetzan,
    "Doppelgangster":               gadgetzan,
    "Finja, the Flying Star":       gadgetzan,

    "Grook Fu Master":              gadgetzan,
    "Red Mana Wyrm":                gadgetzan,
    "Second-Rate Bruiser":          gadgetzan,
    "Spiked Hogrider":              gadgetzan,

    "Streetwise Investigator":      gadgetzan,
    "Ancient of Blossoms":          gadgetzan,
    "Big-Time Racketeer":           gadgetzan,
    "Defias Cleaner":               gadgetzan,

    "Fight Promoter":               gadgetzan,
    "Leatherclad Hogleader":        gadgetzan,
    "Madam Goya":                   gadgetzan,
    "Wind-up Burglebot":            gadgetzan,

    "Wrathion":                     gadgetzan,
    "Mayor Noggenfogger":           gadgetzan,

# druid
    "Jungle Giants":                un_goro,
    "Earten Scales":                un_goro,
    "Tortollan Forager":            un_goro,
    "Elder Longeck":                un_goro,

    "Evolving Spore":               un_goro,    
    "Shellshifer":                  un_goro,
    "Living Mana":                  un_goro,
    "Verdant Longneck":             un_goro,

    "Giant Anaconda":               un_goro,
    "Tyrantus":                     un_goro,

# hunter
    "Stampedge":                    un_goro,
    "Jeweled Macaw":                un_goro,
    "Raptor Hatching":              un_goro,
    "The Marsh Queen":              un_goro,

    "Crackling Razormaw":           un_goro,
    "Dinomancu":                    un_goro,
    "Grievous Bite":                un_goro,
    "Terrorscale Stalker":          un_goro,

    "Tol'vir Warden":               un_goro,
    "Swamp King Dred":              un_goro,

# mage
    "Flame Geyser":                 un_goro,
    "Open the Waygate":             un_goro,
    "Arcanologist":                 un_goro,
    "Primordial Glyph":             un_goro,

    "Pyros":                        un_goro,
    "Shimmering Tempest":           un_goro,
    "Mana Bind":                    un_goro,
    "Molten Reflection":            un_goro,

    "Steam Surger":                 un_goro,
    "Meteor":                       un_goro,

# paladin
    "Adaptation":                   un_goro,
    "Lost in the Jungle":           un_goro,
    "The Last Kaleidosaur":         un_goro,
    "Hydrologist":                  un_goro,

    "Primalfin Champion":           un_goro,
    "Lightfused Stegodon":          un_goro,
    "Spikeridged Steed":            un_goro,
    "Sunkeeper Tarim":              un_goro,

    "Vinecleaver":                  un_goro,
    "Dinosize":                     un_goro,

# priest
    "Awaken the Makers":            un_goro,
    "Binding Heal":                 un_goro,
    "Crystalline Oracle":           un_goro,
    "Radiant Elemental":            un_goro,

    "Shadow Vision":                un_goro,
    "Curious Glimmerroot":          un_goro,
    "Mirage Caller":                un_goro,
    "Tortollan Shellraiser":        un_goro,

    "Lyra the Sunshard":            un_goro,
    "Free From Amber":              un_goro,

# rogue
    "Hallucination":                un_goro,
    "The Canvern Below":            un_goro,
    "Biteweed":                     un_goro,
    "Envenom Weapon":               un_goro,

    "Mimic Pod":                    un_goro,
    "Razorpetal Laser":             un_goro,
    "Razorpetal Volley":            un_goro,
    "Obsidian Shard":               un_goro,

    "Sherazin Corpse Flower":       un_goro,
    "Vilespine Slayer":             un_goro,

# shaman
    "Air Elemental":                un_goro,
    "Unite the Murloc":             un_goro,
    "Fire Plume Harbinger":         un_goro,
    "Primalfin Totem":              un_goro,

    "Spirit Echo":                  un_goro,
    "Hot Spring Guardian":          un_goro,
    "Tidal Surge":                  un_goro,
    "Stone Sentinel":               un_goro,

    "Volcano":                      un_goro,
    "Kalimos, Primal Lord":         un_goro,

# warlock
    "Lakkari Sacrifice":            un_goro,
    "Chittering Tunneler":          un_goro,
    "Clutchmother Zavas":           un_goro,
    "Corruption Mist":              un_goro,

    "Ravenous Pterrordax":          un_goro,
    "Bloodbloom":                   un_goro,
    "Feeding Time":                 un_goro,
    "Lakkari Felhound":             un_goro,
 
    "Cruel Dinomancer":             un_goro,
    "Tar Lurker":                   un_goro,

# warrior
    "Fire Plume's Heart":           un_goro,
    "Iron Hide":                    un_goro,
    "Molten Blade":                 un_goro,
    "Cornered Sentry":              un_goro,

    "Explore Un'Goro":              un_goro,
    "Sudden Genesis":               un_goro,
    "Direhorn Hatchling":           un_goro,
    "Ornery Direhorn":              un_goro,

    "Tar Lord":                     un_goro,
    "King Mosh":                    un_goro,

# neutral
    "Emeld Hive Queen":             un_goro,
    "Emerald Reaver":               un_goro,
    "Fire Fly":                     un_goro,
    "Glacial Shard":                un_goro,

    "Golakka Crawler":              un_goro,
    "Primalfin Lookout":            un_goro,
    "Ravasaur Runt":                un_goro,
    "Rockpool Hunter":              un_goro,

    "Stubborn Gastropod":           un_goro,
    "Volatile Elemental":           un_goro,
    "Bright-Eyed Scout":            un_goro,
    "Devilsaur Egg":                un_goro,

    "Eggnapper":                    un_goro,
    "Giant Wasp":                   un_goro,
    "Gluttonous Ooze":              un_goro,
    "Humongous Razorleaf":          un_goro,

    "Igneous Elemental":            un_goro,
    "Pterrordax Hatchling":         un_goro,
    "Stonehill Defender":           un_goro,
    "Tar Creeper":                  un_goro,

    "The Voraxx":                   un_goro,
    "Thunder Lizard":               un_goro,
    "Vicious Fledgling":            un_goro,
    "Fire Plume Phoenix":           un_goro,

    "Gentle Negasaur":              un_goro,
    "Spiritsinger Umbra":           un_goro,
    "Stegodon":                     un_goro,
    "Tol'vir Stoneshaper":          un_goro,

    "Bittertide Hydra":             un_goro,
    "Elise the Trailblazer":        un_goro,
    "Frozen Crusher":               un_goro,
    "Nesting Roc":                  un_goro,

    "Servant of Kalimos":           un_goro,
    "Blazescaller":                 un_goro,
    "Hemet, Jungle Hunter":         un_goro,
    "Sabretooth Stalker":           un_goro,

    "Volcanosaur":                  un_goro,
    "Charged Devilsaur":            un_goro,
    "Sated Threshadon":             un_goro,
    "Stormwatcher":                 un_goro,

    "Ozruk":                        un_goro,
    "Primordial Drake":             un_goro,
    "Tortollan Primalist":          un_goro,
    "Giant Mastodon":               un_goro,

    "Ultrasaur":                    un_goro,

# druid
    "Druid of the Swarm":           frozen_throne,
    "Crypt Lord":                   frozen_throne,
    "Gnash":                        frozen_throne,
    "Strongshell Scavenger":        frozen_throne,

    "Fatespinner":                  frozen_throne,
    "Webweave":                     frozen_throne,
    "Spreading Plagues":            frozen_throne,
    "Malfurion the Pestilent":      frozen_throne,

    "Hadronox":                     frozen_throne,
    "Ultimate Infestation":         frozen_throne,

# hunter
    "Play Dead":                    frozen_throne,
    "Toxic Arrow":                  frozen_throne,
    "Venomstrike Trap":             frozen_throne,
    "Bearshark":                    frozen_throne,

    "Stitched Tracker":             frozen_throne,
    "Exploding Bloatbat":           frozen_throne,
    "Professor Putricide":          frozen_throne,
    "Corpse Widow":                 frozen_throne,

    "Deathstalker Rexxar":          frozen_throne,
    "Abominable Bowman":            frozen_throne,

# mage
    "Breath of Sindragosa":         frozen_throne,
    "Ice Walker":                   frozen_throne,
    "Coldwraith":                   frozen_throne,
    "Doomed Apprentice":            frozen_throne,

    "Frozen Clone":                 frozen_throne,
    "Simulacrum":                   frozen_throne,
    "Ghastly Conjurer":             frozen_throne,
    "Glacial Mysteries":            frozen_throne,

    "Sindragosa":                   frozen_throne,
    "Frost Lich Jaina":             frozen_throne,

# paladin
    "Righteous Protector":          frozen_throne,
    "Dark Conviction":              frozen_throne,
    "Desperate Stand":              frozen_throne,
    "Howling Commander":            frozen_throne,

    "Arrogant Crusader":            frozen_throne,
    "Chillblade Champion":          frozen_throne,
    "Light's Sorrow":               frozen_throne,
    "Bolvar, Fireblood":            frozen_throne,

    "Blackguard":                   frozen_throne,
    "Uther of the Ebon Blade":       frozen_throne,

# priest
    "Shadow Ascendant":             frozen_throne,
    "Spirit Lash":                  frozen_throne,
    "Acolyte of Agony":             frozen_throne,
    "Eternal Servitude":            frozen_throne,

    "Devour Mind":                  frozen_throne,
    "Embrace Darkness":             frozen_throne,
    "Archbishop Benedicus":         frozen_throne,
    "Shadow Essence":               frozen_throne,

    "Shadowreaper Anduin":          frozen_throne,
    "Obsidian Statue":              frozen_throne,

# rogue
    "Doomerang":                    frozen_throne,
    "Leeching Poison":              frozen_throne,
    "Roll the Bones":               frozen_throne,
    "Plague Scientist":             frozen_throne,

    "Shadowblade":                  frozen_throne,
    "Lilian Voss":                  frozen_throne,
    "Runeforge Haunter":            frozen_throne,
    "Bone Baron":                   frozen_throne,

    "Spectral Pillager":            frozen_throne,
    "Valeera the Hollow":           frozen_throne,

# shaman
    "Brrrloc":                      frozen_throne,
    "Cryostasis":                   frozen_throne,
    "Ice Fishing":                  frozen_throne,
    "Drakkari Defender":            frozen_throne,

    "Ice Breaker":                  frozen_throne,
    "Avalanche":                    frozen_throne,
    "Thrall, Deathseer":            frozen_throne,
    "Voodoo Hexxer":                frozen_throne,
 
    "Moorabi":                      frozen_throne,
    "Snowfury Giant":               frozen_throne,

# warlock
    "Sanguine Reveler":             frozen_throne,
    "Defile":                       frozen_throne,
    "Drain Soul":                   frozen_throne,
    "Gnomeferratu":                 frozen_throne,

    "Howlfiend":                    frozen_throne,
    "Treachery":                    frozen_throne,
    "Unwilling Sacrifice":          frozen_throne,
    "Blood-Queen Lana'thel":        frozen_throne,

    "Despicable Dreadlord":         frozen_throne,
    "Bloodreaver Gul'dan":          frozen_throne,

# warrior
    "Animated Berserker":           frozen_throne,
    "Bring It On!":                 frozen_throne,
    "Dead Man's Hand":              frozen_throne,
    "Forge of Souls":               frozen_throne,

    "Mountainfire Armor":           frozen_throne,
    "Val'kyr Soulclaimer":          frozen_throne,
    "Blood Razor":                  frozen_throne,
    "Death Revenant":               frozen_throne,

    "Rotface":                      frozen_throne,
    "Scourgelord Garrosh":          frozen_throne,

# neutral
    "Snowflipper Penguin":          frozen_throne,
    "Acherus Veteran":              frozen_throne,
    "Deadscale Knight":             frozen_throne,
    "Wretched Tiller":              frozen_throne,

    "Fallen Sun Cleric":            frozen_throne,
    "Prince Keleseth":              frozen_throne,
    "Tainted Zealot":               frozen_throne,
    "Tuskarr Fisherman":            frozen_throne,
 
    "Deathspeaker":                 frozen_throne,
    "Drakkari Enchanter":           frozen_throne,
    "Happy Ghost":                  frozen_throne,
    "Hyldnir Frostrider":           frozen_throne,

    "Mindbreaker":                  frozen_throne,
    "Prince Taldaram":              frozen_throne,
    "Shallow Gravedigger":          frozen_throne,
    "Vryghoul":                     frozen_throne,

    "Arfus":                        frozen_throne,
    "Corpsetaker":                  frozen_throne,
    "Deathaxe Punisher":            frozen_throne,
    "Grave Shambler":               frozen_throne,

    "Grim Necromancer":             frozen_throne,
    "Keening Banshee":              frozen_throne,
    "Meat Wagon":                   frozen_throne,
    "Night Howler":                 frozen_throne,

    "Phantom Freebooter":           frozen_throne,
    "Prince Valanar":               frozen_throne,
    "Rattling Rascal":              frozen_throne,
    "Saronite Chain Gang":          frozen_throne,

    "Ticking Abomination":          frozen_throne,
    "Wicked Skeleton":              frozen_throne,
    "Bloodworm":                    frozen_throne,
    "Cobalt Scalebane":             frozen_throne,

    "Corpse Raiser":                frozen_throne,
    "Skelemancer":                  frozen_throne,
    "Sunborne Val'kyr":             frozen_throne,
    "Tomb Lurker":                  frozen_throne,

    "Venomancer":                   frozen_throne,
    "Bone Drake":                   frozen_throne,
    "Furnacefire Colossus":         frozen_throne,
    "Necrotic Geist":               frozen_throne,

    "Nerubian Unraveier":           frozen_throne,
    "Skulking Geist":               frozen_throne,
    "Spellweaver":                  frozen_throne,
    "Bonemare":                     frozen_throne,

    "The Lich King":                frozen_throne,

# druid
    "Barkskin":                     kobolds,
    "Lesser Jasper Spellstone":     kobolds,
    "Greedy Spirit":                kobolds,
    "Astral Tiger":                 kobolds,

    "Branching Path":               kobolds,
    "Ironwood Golem":               kobolds,
    "Oaken Summons":                kobolds,
    "Twig of the World Tree":       kobolds,

    "Ixlid, Fungal Lord":           kobolds,
    "Grizzled Guardian":            kobolds,

# hunter
    "Candleshot":                   kobolds,
    "Wandering Monster":            kobolds,
    "Cave Hydra":                   kobolds,
    "Flanking Strike":              kobolds,

    "Lesser Emerald Spellstone":    kobolds,
    "Seeping Oozeling":             kobolds,
    "To My Side!":                  kobolds,
    "Crushing Walls":               kobolds,

    "Rhok'delar":                   kobolds,
    "Kathrena Winterwisp":          kobolds,

# mage
    "Shifting Scroll":              kobolds,
    "Arcane Artificer":             kobolds,
    "Lesser Ruby Spellstone":       kobolds,
    "Raven Familiar":               kobolds,

    "Explosive Runes":              kobolds,
    "Leyline Manipulator":          kobolds,
    "Deck of Wonders":              kobolds,
    "Dragon's Fury":                kobolds,

    "Aluneth":                      kobolds,
    "Dragoncaller Atlanna":         kobolds,

# paladin
    "Drygulch Jailor":              kobolds,
    "Lesser Pearl Spellstone":      kobolds,
    "Potion of Heroism":            kobolds,
    "Benevolent Djinn":             kobolds,

    "Unidentified Maul":            kobolds,
    "Call to Arms":                 kobolds,
    "Level Up!":                    kobolds,
    "Crystal Lion":                 kobolds,

    "Val'anyr":                     kobolds,
    "Lynessa Sunsorrow":            kobolds,

# priest
    "Psionic Probe":                kobolds,
    "Dragon Soul":                  kobolds,
    "Glided Gargoyle":              kobolds,
    "Twilight Acolyte":             kobolds,

    "Twilight's Call":              kobolds,
    "Unidentified Elixir":          kobolds,
    "Duskbreaker":                  kobolds,
    "Lesser Diamond Spellstone":    kobolds,

    "Psychic Scream":               kobolds,
    "Temporus":                     kobolds,

# rogue
    "Kingsbane":                    kobolds,
    "Cavern Shinyfinder":           kobolds,
    "Cheat Death":                  kobolds,
    "Evasion":                      kobolds,
 
    "Sudden Betrayal":              kobolds,
    "Sonya Shadowdancer":           kobolds,
    "Elven Minstrel":               kobolds,
    "Fal'dorei Strider":            kobolds,

    "Kobold Illusionist":           kobolds,
    "Lesser Onyx Spellstone":       kobolds,

# shaman
    "Unstable Evolution":           kobolds,
    "Crushing Hand":                kobolds,
    "Kobold Hermit":                kobolds,
    "Murmuring Elemental":          kobolds,

    "Healing Rain":                 kobolds,
    "Primal Talisman":              kobolds,
    "Windshear Stormcaller":        kobolds,
    "Grumble, Worldshaker":         kobolds,

    "Lesser Sappphire Spellstone":  kobolds,
    "The Runespear":                kobolds,

# warlock
    "Dark Pact":                    kobolds,
    "Kobold Librarian":             kobolds,
    "Vulgar Homunculur":            kobolds,
    "Hooked Reaver":                kobolds,

    "Lesser Amethyst Spellstone":   kobolds,
    "Cataclysm":                    kobolds,
    "Skull of the Man'ari":         kobolds,
    "Possessed Lackey":             kobolds,

    "Rin, the First Disciple":      kobolds,
    "Voidlord":                     kobolds,

# warrior
    "Bladed Gauntlet":              kobolds,
    "Drywhisker Armorer":           kobolds,
    "Kobold Barbarian":             kobolds,
    "Reckless Flurry":              kobolds,

    "Gather Your Party":            kobolds,
    "Gemstudded Golem":             kobolds,
    "Unidentid Shield":             kobolds,
    "Lesser Mithril Spellstone":    kobolds,

    "Geosculptor Yip":              kobolds,
    "Woecleaver":                   kobolds,

# neutral
    "Dire Mole":                    kobolds,
    "Feral Gibberer":               kobolds,
    "Gravesnout Knight":            kobolds,
    "Wax Elemental":                kobolds,

    "Plated Beetle":                kobolds,
    "Scorp-o-matic":                kobolds,
    "Boisterous Bard":              kobolds,
    "Dragonslayer":                 kobolds,

    "Fungal Enchanter":             kobolds,
    "Kobold Apprentice":            kobolds,
    "Lone Chamption":               kobolds,
    "Rummaging Kobold":             kobolds,

    "Sewer Crawler":                kobolds,
    "Shrieking Shroom":             kobolds,
    "Stoneskin Basilisk":           kobolds,
    "Toothy Chest":                 kobolds,

    "Void Ripper":                  kobolds,
    "Zola the Gorgan":              kobolds,
    "Cursed Disciple":              kobolds,
    "Ebon Dragonsmity":             kobolds,

    "Hoarding Dragon":              kobolds,
    "Kobold Monk":                  kobolds,
    "Shimmering Courser":           kobolds,
    "Shroom Brewer":                kobolds,

    "Sneaky Devil":                 kobolds,
    "The Darkness":                 kobolds,
    "Arcane Tyrant":                kobolds,
    "Carnivorous Cube":             kobolds,

    "Corrosive Sludge":             kobolds,
    "Fungalmancer":                 kobolds,
    "Furbolg Mossbinder":           kobolds,
    "Green Jelly":                  kobolds,

    "Guild Recruiter":              kobolds,
    "Trogg Gloomeater":             kobolds,
    "Hungry Ettin":                 kobolds,
    "Spiteful Summonner":           kobolds,

    "Corridor":                     kobolds,
    "Silver Vanguard":              kobolds,
    "Grand Archivist":              kobolds,
    "King Togwaggle":               kobolds,

    "Marin the Fox":                kobolds,
    "Violet Wurm":                  kobolds,
    "Dragonhatcher":                kobolds,
    "Master Oakheart":              kobolds,

    "Sleepy Dragon":                kobolds,

# druid
    "Witchwood Apple":              witchwood,
    "Druid of the Scythe":          witchwood,
    "Ferocious Howl":               witchwood,
    "Witching Hour":                witchwood,

    "Forest Guide":                 witchwood,
    "Wispering Woods":              witchwood,
    "Bewitched Guardian":           witchwood,
    "Duskfallen Aviana":            witchwood,

    "Gloom Stag":                   witchwood,
    "Splintergraft":                witchwood,

# hunter
    "Hunter Mastiff":               witchwood,
    "Rat Trap":                     witchwood,
    "Duskhaven Hunter":             witchwood,
    "Dire Frenzy":                  witchwood,

    "Houndmaster Shaw":             witchwood,
    "Toxmonger":                    witchwood,
    "Wing Blast":                   witchwood,
    "Carrion Drake":                witchwood,

    "Vilebrood Skitterer":          witchwood,
    "Emeriss":                      witchwood,

# mage
    "Snap Freeze":                  witchwood,
    "Archmage Arugal":              witchwood,
    "Book of Specters":             witchwood,
    "Black Cat":                    witchwood,

    "Cinderstorm":                  witchwood,
    "Arcane Keysmith":              witchwood,
    "Vex Crow":                     witchwood,
    "Bonfire Elemenatal":           witchwood,

    "Curio Collector":              witchwood,
    "Toki, Time-Tinker":            witchwood,

# paladin
    "Hidden Wisdom":                witchwood,
    "Cathedral Gargoyle":           witchwood,
    "Rebuke":                       witchwood,
    "Sound the Bells":              witchwood,

    "Paragon of Light":             witchwood,
    "Bellringer Sentry":            witchwood,
    "The Glass Knight":             witchwood,
    "Ghostly Charger":              witchwood,

    "Prince Liam":                  witchwood,
    "Silver Sword":                 witchwood,
 
# priest
    "Chameleos":                    witchwood,
    "Divine Hymn":                  witchwood,
    "Squashling":                   witchwood,
    "Vivid Nightmare":              witchwood,

    "Glitter Moth":                 witchwood,
    "Holy Water":                   witchwood,
    "Quartz Elemental":             witchwood,
    "Coffin Crasher":               witchwood,

    "Lady in White":                witchwood,
    "Nightscale Matriarch":         witchwood,

# rogue
    "Cheap Shot":                   witchwood,
    "Pick Pocket":                  witchwood,
    "Blink Fox":                    witchwood,
    "Cutthroat Buccaneer":          witchwood,

    "Face Collector":               witchwood,
    "Mistwraith":                   witchwood,
    "Spectral Cutlass":             witchwood,
    "WANTED!":                      witchwood,

    "Cursed Castaway":              witchwood,
    "Tess Greymane":                witchwood,

# shaman
    "Zap!":                         witchwood,
    "Blazing Invocation":           witchwood,
    "Witch's Apprentice":           witchwood,
    "Earthen Might":                witchwood,

    "Ghost Light Angler":           witchwood,
    "Murkspark Eel":                witchwood,
    "Totem Cruncher":               witchwood,
    "Bogshaper":                    witchwood,

    "Hagatha the Witch":            witchwood,
    "Shudderwock":                  witchwood,

# warlock
    "Dark Possession":              witchwood,
    "Witchwood Imp":                witchwood,
    "Curse of Weakness":            witchwood,
    "Duskbat":                      witchwood,

    "Fiendish Circle":              witchwood,
    "Ratcatcher":                   witchwood,
    "Blood Witch":                  witchwood,
    "Deathweb Spider":              witchwood,

    "Glinda Crowskin":              witchwood,
    "Lord Godfrey":                 witchwood,

# warrior
    "Town Crier":                   witchwood,
    "Redband Wasp":                 witchwood,
    "Warpath":                      witchwood,
    "Woodcutter's Axe":             witchwood,

    "Rabid Worgen":                 witchwood,
    "Militia Commander":            witchwood,
    "Darius Crowley":               witchwood,
    "Festeroof Hulk":               witchwood,

    "Deadly Arsenal":               witchwood,
    "Blackhowl Gunspire":           witchwood,

# neutral
    "Swampy Dragon Egg":            witchwood,
    "Swampy Leech":                 witchwood,
    "Baleful Banker":               witchwood,
    "Lost Spirit":                  witchwood,

    "Spellshifter":                 witchwood,
    "Vicious Scalehide":            witchwood,
    "Blackwald Pixie":              witchwood,
    "Hench-Clan Thug":              witchwood,

    "Marsh Drake":                  witchwood,
    "Nightmare Amalgam":            witchwood,
    "Phantom Militia":              witchwood,
    "Punpkin Peasant":              witchwood,

    "Ravencaller":                  witchwood,
    "Tanglefur":                    witchwood,
    "Voodoo Doll":                  witchwood,
    "Walnut Sprite":                witchwood,

    "Witch's Cauldron":             witchwood,
    "Felsoul Inquisitor":           witchwood,
    "Lifedrinker":                  witchwood,
    "Mad Hatter":                   witchwood,

    "Night Prowler":                witchwood,
    "Sandbinder":                   witchwood,
    "Scaleworm":                    witchwood,
    "Swift Messenger":              witchwood,

    "Unpowered Steambot":           witchwood,
    "Witchwood Piper":              witchwood,
    "Chief Inspector":              witchwood,
    "Clockwork Automaton":          witchwood,

    "Dollmaster Dorian":            witchwood,
    "Muck Hunter":                  witchwood,
    "Rotten Applebaum":             witchwood,
    "Witchwood Grizzly":            witchwood,

    "Genn Greymane":                witchwood,
    "Mossy Horrer":                 witchwood,
    "Azaline Soulthief":            witchwood,
    "Countess Ashmore":             witchwood,

    "Darkmire Moonkin":             witchwood,
    "Furious Ettin":                witchwood,
    "Worgen Abomination":           witchwood,
    "Wyrmguard":                    witchwood,

    "Cauldron Elemental":           witchwood,
    "Deranged Doctor":              witchwood,
    "Gilnean Royal Guard":          witchwood,
    "Splitting Festeroot":          witchwood,

    "Baku the Mooneater":           witchwood,

# druid
    "Biology Project":              boomsday,
    "Floop's Glorious Gloop":       boomsday,
    "Dendrologist":                 boomsday,
    "Landscaping":                  boomsday,

    "Flobbidinous Floop":           boomsday,
    "Juicy Psychmelon":             boomsday,
    "Tending Tauren":               boomsday,
    "Dreampetal Florist":           boomsday,

    "Gloop Sprayer":                boomsday,
    "Mulchmuncher":                 boomsday,

# hunter
    "Secret Plan":                  boomsday,
    "Bomb Toss":                    boomsday,
    "Cybertech Chip":               boomsday,
    "Fireworks Tech":               boomsday,

    "Goblin Prank":                 boomsday,
    "Venomizer":                    boomsday,
    "Spider Bomb":                  boomsday,
    "Necromechanic":                boomsday,

    "Boommaster Flark":             boomsday,
    "Flark's Boom-Zooka":           boomsday,

# mage
    "Shooting Star":                boomsday,
    "Astral Rift":                  boomsday,
    "Celestial Emissary":           boomsday,
    "Research Project":             boomsday,

    "Stargazer Luna":               boomsday,
    "Unexpected Result":            boomsday,
    "Cosmic Anomaly":               boomsday,
    "Meterologist":                 boomsday,

    "Astromancer":                  boomsday,
    "Luna's Pocket Galaxy":         boomsday,

# paladin
    "Autodefense Matrix":           boomsday,
    "Crystology":                   boomsday,
    "Glow-Tron":                    boomsday,
    "Crystalsmith Kangor":          boomsday,

    "Annoy-o-Module":               boomsday,
    "Primatic Lens":                boomsday,
    "Glowstone Technician":         boomsday,
    "Mechano-Egg":                  boomsday,

    "Shrink Ray":                   boomsday,
    "Kangor's Endless Army":        boomsday,

# priest
    "Topsy Turvy":                  boomsday,
    "Cloning Device":               boomsday,
    "Test Subject":                 boomsday,
    "Dead Ringer":                  boomsday,

    "Extra Arms":                   boomsday,
    "Omega Medic":                  boomsday,
    "Power Word: Replicate":        boomsday,
    "Reckless Experimenter":        boomsday,

    "Zerek, Master Cloner":         boomsday,
    "Zerek's Cloning Gallery":      boomsday,

# rogue
    "Pogo-Hopper":                  boomsday,
    "Lab Recruiter":                boomsday,
    "Violet Haze":                  boomsday,
    "Necrium Blade":                boomsday,

    "Academic Espionage":           boomsday,
    "Blightnozzle Crawler":         boomsday,
    "Crazed Chemist":               boomsday,
    "Myra Rotspring":               boomsday,

    "Myra's Unstable Element":      boomsday,
    "Necriun Vial":                 boomsday,

# shaman
    "Beakered Lightning":           boomsday,
    "Voltaic Burst":                boomsday,
    "Elementary Reaction":          boomsday,
    "Menacing Nimbus":              boomsday,

    "Omega Mind":                   boomsday,
    "Electra Stormsurge":           boomsday,
    "Storm Chaser":                 boomsday,
    "Thunderhead":                  boomsday,

    "Eureka":                       boomsday,
    "The Storm Bringer":            boomsday,

# warlock
    "Soul Infusion":                boomsday,
    "Spirit Bomb":                  boomsday,
    "The Soilarium":                boomsday,
    "Demonic Project":              boomsday,

    "Void Analyst":                 boomsday,
    "Doubling Imp":                 boomsday,
    "Nethersoul Buster":            boomsday,
    "Omega Agent":                  boomsday,

    "Dr. Morrigan":                 boomsday,
    "Ectomancy":                    boomsday,

# warrior
    "Eternium Rover":               boomsday,
    "Omega Assembly":               boomsday,
    "Rocket Boots":                 boomsday,
    "Weapons Project":              boomsday,

    "Dyn-o-matic":                  boomsday,
    "Supercollider":                boomsday,
    "Security Rover":               boomsday,
    "Beryllium Nullifier":          boomsday,

    "Dr. Boom, Mad Genius":         boomsday,
    "The Boomship":                 boomsday,

# neutral
    "Crystallizer":                 boomsday,
    "Faithful Lumi":                boomsday,
    "Goblin Bomb":                  boomsday,
    "Mecharoo":                     boomsday,

    "Skaterbot":                    boomsday,
    "Cloakscale Chemist":           boomsday,
    "Galvanizer":                   boomsday,
    "Spark Engine":                 boomsday,

    "Toxicologist":                 boomsday,
    "Unpowered Mauler":             boomsday,
    "Upgradeable Framebot":         boomsday,
    "Whirlglider":                  boomsday,

    "Augmented Elekk":              boomsday,
    "Brainstormer":                 boomsday,
    "Bronze Gatekeeper":            boomsday,
    "Electrowright":                boomsday,

    "Kaboom Bot":                   boomsday,
    "Microtech Controller":         boomsday,
    "SN1P-SN4P":                    boomsday,
    "Spring Rocket":                boomsday,

    "Coppertail Imposter":          boomsday,
    "Explodinator":                 boomsday,
    "Harbinger Celestin":           boomsday,
    "Omega Defender":               boomsday,

    "Piloted Reaper":               boomsday,
    "Replicating Menace":           boomsday,
    "Steel Rager":                  boomsday,
    "Weaponized Piñata":            boomsday,

    "Whizbang the Wonderful":       boomsday,
    "E.M.P. Operative":             boomsday,
    "Holomancer":                   boomsday,
    "Loose Specimen":               boomsday,

    "Rusty Recycler":               boomsday,
    "Seaforiun Bomber":             boomsday,
    "Subject 9":                    boomsday,
    "Wargear":                      boomsday,

    "Zilliax":                      boomsday,
    "Arcane Dynamo":                boomsday,
    "Damaged Stegotron":            boomsday,
    "Giggling Inventor":            boomsday,

    "Mechanical Whelp":             boomsday,
    "Missile Launcher":             boomsday,
    "Spark Drill":                  boomsday,
    "Star Aligner":                 boomsday,

    "Bull Dozer":                   boomsday,
    "Mecha'thun":                   boomsday,

# druid
    "Pounce":                       rumble,
    "Spirit of the Raptor":         rumble,
    "Savage Striker":               rumble,
    "Wardruid Loti":                rumble,

    "Mark of the Loa":              rumble,
    "Predatory Instincts":          rumble,
    "Treespeaker":                  rumble,
    "Stampeding Roar":              rumble,

    "Gonk, the Raptor":             rumble,
    "Ironhide Direhorn":            rumble,

# hunter
    "Springpaw":                    rumble,
    "The Beast Within":             rumble,
    "Headhunter's Hatchet":         rumble,
    "Revenge of the Wild":          rumble,

    "Bloodscalp Strategist":        rumble,
    "Master's Call":                rumble,
    "Spirit of the Lynx":           rumble,
    "Baited Arrow":                 rumble,

    "Halazzi, the Lynx":            rumble,
    "Zul'jin":                      rumble,

# mage
    "Elemental Evocation":          rumble,
    "Daring Fire-Eater":            rumble,
    "Spirit of theDragonhawk":      rumble,
    "Pyromaniac":                   rumble,

    "Splitting Image":              rumble,
    "Scorch":                       rumble,
    "Blast Ware":                   rumble,
    "Arcanosaur":                   rumble,

    "Jan'alai, the Dragonhawk":     rumble,
    "Hex Lord Malacrass":           rumble,

# paladin
    "Bloodclaw":                    rumble,
    "Flash of Light":               rumble,
    "Immortal Prelate":             rumble,
    "High Priest Thekal":           rumble,

    "Time Out!":                    rumble,
    "Spirit of the Tiger":          rumble,
    "Zandalari Templar":            rumble,
    "Farraki Battleaxe":            rumble,

    "A New Challenger...":          rumble,
    "Shirvallah, the Tiger":        rumble,

# priest
    "Regenerate":                   rumble,
    "Spirit of the Dead":           rumble,
    "Auchenai Phantasm":            rumble,
    "Seance":                       rumble,

    "Sand Drudge":                  rumble,
    "Surrender to Madness":         rumble,
    "Mass Hysteria":                rumble,
    "Bwonsamdi, the Dead":          rumble,

    "Princess Talanji":             rumble,
    "Grave Horror":                 rumble,

# rogue
    "Serrated Tooth":               rumble,
    "Bloodsail Howler":             rumble,
    "Stolen Steel":                 rumble,
    "Raiding Party":                rumble,

    "Spirit of the Shark":          rumble,
    "Walk the Plank":               rumble,
    "Gral, the Shark":              rumble,
    "Cannon Barrage":               rumble,

    "Gurubashi Hypemon":            rumble,
    "Captain Hooktusk":             rumble,

# shaman
    "Totemic Smash":                rumble,
    "Wartbringer":                  rumble,
    "Big Bad Voodoo":               rumble,
    "Likkim":                       rumble,

    "Bog Slosher":                  rumble,
    "Haunting Visions":             rumble,
    "Zentimo":                      rumble,
    "Spirit of the Frog":           rumble,

    "Krag'wa, the Frog":            rumble,
    "Rain of Toad":                 rumble,

# warlock
    "Grim Rally":                   rumble,
    "Shriek":                       rumble,
    "Spirit of the Bat":            rumble,
    "Reckless Diretroll":           rumble,

    "High Priestess Jeklik":        rumble,
    "Soulwarden":                   rumble,
    "Blood Troll Sapper":           rumble,
    "Demonbolt":                    rumble,

    "Hir'eek, the Bat":             rumble,
    "Void Contract":                rumble,

# warrior
    "Devastate":                    rumble,
    "Spirit of the Rhino":          rumble,
    "Dragon Roar":                  rumble,
    "Overlord's Whip":              rumble,

    "Smolderthorn Lancer":          rumble,
    "War Master Voone":             rumble,
    "Emberscale Drake":             rumble,
    "Heavy Netal!":                 rumble,

    "Su'thraze":                    rumble,
    "Akali, the Rhino":             rumble,

# neutral
    "Gurubashi Chicken":            rumble,
    "Gurubashi Offering":           rumble,
    "Helpless Hatchling":           rumble,
    "Saronite Taskmaster":          rumble,

    "Belligerent Gnome":            rumble,
    "Booty Bay Bookie":             rumble,
    "Cheaty Anklebiter":            rumble,
    "Dozing Marksman":              rumble,

    "Firetree Witchdoctor":         rumble,
    "Scarab Egg":                   rumble,
    "Serpent Ward":                 rumble,
    "Sharkfin Fan":                 rumble,

    "Shieldbreaker":                rumble,
    "Soup Vendeor":                 rumble,
    "Spellzerker":                  rumble,
    "Waterboy":                     rumble,

    "Banana Buffoon":               rumble,
    "Drakkari Trickster":           rumble,
    "Masked Contender":             rumble,
    "Ornery Tortoise":              rumble,

    "Untamed Beastmaster":          rumble,
    "Arena Fanatic":                rumble,
    "Arena Treasure Chest":         rumble,
    "Griftah":                      rumble,

    "Half-Time Scavenger":          rumble,
    "Ice Cream Peddler":            rumble,
    "Murloc Tastyfin":              rumble,
    "Regeneratin' Thug":            rumble,

    "Rumbletask Shaker":            rumble,
    "Ticket Scalper":               rumble,
    "Arena Patron":                 rumble,
    "Dragonmaw Scorcher":           rumble,

    "Former Champ":                 rumble,
    "Mosh'Ogg Announcer":           rumble,
    "Sightless Ranger":             rumble,
    "Snapjaw Shellfighter":         rumble,

    "Mojomaster Zihi":              rumble,
    "Amani War Bear":               rumble,
    "Crowd Roaster":                rumble,
    "Linecracker":                  rumble,

    "Rabble Bouncer":               rumble,
    "Da Undatekah":                 rumble,
    "Mosh'Ogg Enforcer":            rumble,
    "Oondasta":                     rumble,

    "Hakkar, the Soulflayer":       rumble,

# druid
    "Acornbearer":                  rise_of_shadows,
    "Crystal Power":                rise_of_shadows,
    "Crystalsong Portal":           rise_of_shadows,
    "Dreamway Guardians":           rise_of_shadows,

    "Keeper Stalladris":            rise_of_shadows,
    "Blessing of the Ancients":     rise_of_shadows,
    "Lifeweaver":                   rise_of_shadows,
    "Crystal Stag":                 rise_of_shadows,

    "Lucentbark":                   rise_of_shadows,
    "The Forest's Aid":             rise_of_shadows,

# hunter
    "Shimmerfly":                   rise_of_shadows,
    "Rapid Fire":                   rise_of_shadows,
    "Nine Lives":                   rise_of_shadows,
    "Ursatron":                     rise_of_shadows,

    "Arcane Fletcher":              rise_of_shadows,
    "Marked Shot":                  rise_of_shadows,
    "Huntling Party":               rise_of_shadows,
    "Oblivitron":                   rise_of_shadows,

    "Unleash the Beast":            rise_of_shadows,
    "Vereesa Windrunner":           rise_of_shadows,

# mage
    "Magic Trick":                  rise_of_shadows,
    "Ray of Frost":                 rise_of_shadows,
    "Khadgar":                      rise_of_shadows,
    "Magic Dart Frog":              rise_of_shadows,

    "Mana Cyclone":                 rise_of_shadows,
    "Conjurer's Calling":           rise_of_shadows,
    "Messenger Raven":              rise_of_shadows,
    "Kirin Tor Tricaster":          rise_of_shadows,

    "Kalecgos":                     rise_of_shadows,
    "Power of Creation":            rise_of_shadows,

# paladin
    "Desperate Measures":           rise_of_shadows,
    "Never Surrender!":             rise_of_shadows,
    "Lightforged Blessing":         rise_of_shadows,
    "Mysterious Blade":             rise_of_shadows,

    "Bronze Herald":                rise_of_shadows,
    "Call to Adventure":            rise_of_shadows,
    "Commander Rhyssa":             rise_of_shadows,
    "Dragon Speaker":               rise_of_shadows,

    "Duel!":                        rise_of_shadows,
    "Nozari":                       rise_of_shadows,

# priest
    "Forbidden Words":              rise_of_shadows,
    "Lazul's Scheme":               rise_of_shadows,
    "EVIL Conscripter":             rise_of_shadows,
    "Shadowy Figure":               rise_of_shadows,

    "Madame Lazul":                 rise_of_shadows,
    "Hench-Clan Shadequill":        rise_of_shadows,
    "Unsleeping Soul":              rise_of_shadows,
    "Convincing Infiltrator":       rise_of_shadows,

    "Catrina Muerte":               rise_of_shadows,
    "Mass Resurrection":            rise_of_shadows,

# rogue
    "Daring Escape":                rise_of_shadows,
    "Togwaggle's Scheme":           rise_of_shadows,
    "Underbelly Fence":             rise_of_shadows,
    "EVIL Miscreant":               rise_of_shadows,

    "Hench-Clan Burglar":           rise_of_shadows,
    "Vendetta":                     rise_of_shadows,
    "Waggle Pick":                  rise_of_shadows,
    "Heistbaron Togwaggle":         rise_of_shadows,

    "Unidentified Contract":        rise_of_shadows,
    "Tak Nozwhisker":               rise_of_shadows,

# shaman
    "Mutate":                       rise_of_shadows,
    "Sludge Slurper":               rise_of_shadows,
    "Soul of the Murloc":           rise_of_shadows,
    "Underbelly Angler":            rise_of_shadows,

    "Witch's Brew":                 rise_of_shadows,
    "Hagatha's Scheme":             rise_of_shadows,
    "Muckmorpher":                  rise_of_shadows,
    "Scargil":                      rise_of_shadows,

    "Swampqueen Hagatha":           rise_of_shadows,
    "Walking Fountain":             rise_of_shadows,

# warlock
    "EVIL Genius":                  rise_of_shadows,
    "Plot Twist":                   rise_of_shadows,
    "Impferno":                     rise_of_shadows,
    "Rafaam's Scheme":              rise_of_shadows,

    "Eager Underling":              rise_of_shadows,
    "Aranasi Broodmother":          rise_of_shadows,
    "Darkest Hour":                 rise_of_shadows,
    "Arch-Villain Rafaam":          rise_of_shadows,

    "Fel Lord Betrug":              rise_of_shadows,
    "Jumbo Imp":                    rise_of_shadows,

# warrior
    "Improve Morale":               rise_of_shadows,
    "Sweeping Strikes":             rise_of_shadows,
    "Vicious Scraphound":           rise_of_shadows,
    "Clockwork Goblin":             rise_of_shadows,

    "Dr. Boom's Scheme":            rise_of_shadows,
    "Omega Devastator":             rise_of_shadows,
    "Wrenchcalibur":                rise_of_shadows,
    "Blastmaster Boom":             rise_of_shadows,

    "Dimensional Ripper":           rise_of_shadows,
    "The Boom Reaver":              rise_of_shadows,

# neutral
    "Potion Vendor":                rise_of_shadows,
    "Toxfin":                       rise_of_shadows,
    "Arcane Servant":               rise_of_shadows,
    "Dalaran Librarian":            rise_of_shadows,

    "EVIL Cable Rat":               rise_of_shadows,
    "Hench-Clan Hogsteed":          rise_of_shadows,
    "Mana Reservoir":               rise_of_shadows,
    "Spellbook Binder":             rise_of_shadows,

    "Sunreaver Spy":                rise_of_shadows,
    "Zayle, Shadow Cloak":          rise_of_shadows,
    "Arcane Watcher":               rise_of_shadows,
    "Faceless Rager":               rise_of_shadows,

    "Flight Master":                rise_of_shadows,
    "Hench-Clan Sneak":             rise_of_shadows,
    "Magic Carpet":                 rise_of_shadows,
    "Spellward Jeweler":            rise_of_shadows,

    "Archmage Vargoth":             rise_of_shadows,
    "Hecklebot":                    rise_of_shadows,
    "Hench-Clan Hag":               rise_of_shadows,
    "Portal Keeper":                rise_of_shadows,

    "Proud Defender":               rise_of_shadows,
    "Soldier of Fortune":           rise_of_shadows,
    "Traveling Healer":             rise_of_shadows,
    "Violet Spellsword":            rise_of_shadows,

    "Azerite Elemental":            rise_of_shadows,
    "Barista Lynchen":              rise_of_shadows,
    "Dalaran Crusader":             rise_of_shadows,
    "Recurring Villain":            rise_of_shadows,

    "Sunreaver Warmage":            rise_of_shadows,
    "Eccentric Scribe":             rise_of_shadows,
    "Mad Summoner":                 rise_of_shadows,
    "Portal Overfiend":             rise_of_shadows,

    "Safeguard":                    rise_of_shadows,
    "Unseen Saboteur":              rise_of_shadows,
    "Violet Warden":                rise_of_shadows,
    "Chef Nomi":                    rise_of_shadows,

    "Exotic Mountseller":           rise_of_shadows,
    "Tunnel Blaster":               rise_of_shadows,
    "Underbelly Ooze":              rise_of_shadows,
    "Archivist Elysiana":           rise_of_shadows,

    "Batterhead":                   rise_of_shadows,
    "Heroic Innkeeper":             rise_of_shadows,
    "Jepetto Joybuzz":              rise_of_shadows,
    "Whirlwind Tempest":            rise_of_shadows,

    "Burly Shovelfist":             rise_of_shadows,
    "Big Bad Archmage":             rise_of_shadows,

# druid
    "Untapped Potential":           uldum,
    "Worthy Expedition":            uldum,
    "Crystal Merchant":             uldum,
    "BEEEES!!!":                    uldum,

    "Garden Gnome":                 uldum,
    "Anubisath Defender":           uldum,
    "Elise the Enlightened":        uldum,
    "Oasis Surger":                 uldum,

    "Hidden Oasis":                 uldum,
    "Overflow":                     uldum,

# hunter
    "Unseal the Vault":             uldum,
    "Pressure Plate":               uldum,
    "Desert Spear":                 uldum,
    "Hunter's Pack":                uldum,

    "Ramkahen Wildtamer":           uldum,
    "Hyena Alpha":                  uldum,
    "Scarlet Webweaver":            uldum,
    "Swarm of Locusts":             uldum,

    "Wild Bloodstinger":            uldum,
    "Dinotamer Brann":              uldum,

# mage
    "Raid the Sky Temple":          uldum,
    "Ancient Mysteries":            uldum,
    "Arcane Flakmage":              uldum,
    "Dune Sculptor":                uldum,

    "Flame Ward":                   uldum,
    "Cloud Prince":                 uldum,
    "Naga Sand Witch":              uldum,
    "Reno the Relicologist":        uldum,

    "Tortollan Pilgrim":            uldum,
    "Puzzle Box of Yogg-Saron":     uldum,

# paladin
    "Brazen Zealot":                uldum,
    "Making Mummies":               uldum,
    "Micro Mummy":                  uldum,
    "Sandwasp Queen":               uldum,

    "Sir Finley of the Sands":      uldum,
    "Subdue":                       uldum,
    "Salhet's Pride":               uldum,
    "Ancestral Guardian":           uldum,

    "Pharaoh's Blessing":           uldum,
    "Tip the Scales":               uldum,

# priest
    "Activate the Obelisk":         uldum,
    "Embalming Ritual":             uldum,
    "Grandmummy":                   uldum,
    "Holy Ripple":                  uldum,

    "Penance":                      uldum,
    "Wretched Reclaimer":           uldum,
    "High Priest Amet":             uldum,
    "Psychopomp":                   uldum,

    "Sandhoof Waterbearer":         uldum,
    "Plague of Death":              uldum,

# rogue
    "Bazaar Burglary":              uldum,
    "Pharaoh Cat":                  uldum,
    "Plague of Madness":            uldum,
    "Clever Disguise":              uldum,

    "Whirlkick Master":             uldum,
    "Hooked Scimitar":              uldum,
    "Sahket Sapper":                uldum,
    "Shadow of Death":              uldum,

    "Anka, the Buried":             uldum,
    "Bazaar Mugger":                uldum,

# shaman
    "Totemic Surge":                uldum,
    "Corrupt the Waters":           uldum,
    "EVIL Totem":                   uldum,
    "Sandstorm Elemental":          uldum,

    "Plague of Murlocs":            uldum,
    "Weaponized Wasp":              uldum,
    "Splitting Axe":                uldum,
    "Vessina":                      uldum,

    "Earthquake":                   uldum,
    "Mogu Fleshshaper":             uldum,

# warlock
    "Plague of Flames":             uldum,
    "Sinister Deal":                uldum,
    "Supreme Archaeology":          uldum,
    "Expired Merchant":             uldum,

    "EVIL Recruiter":               uldum,
    "Neferset Thrasher":            uldum,
    "Diseased Vulture":             uldum,
    "Impbalming":                   uldum,

    "Dark Pharaoh Tekahn":          uldum,
    "Riftcleaver":                  uldum,

# warrior
    "Hack the System":              uldum,
    "Into the Fray":                uldum,
    "Frightened Flunky":            uldum,
    "Bloodsworn Mercenary":         uldum,

    "Livewire Lance":               uldum,
    "Restless Mummy":               uldum,
    "Plague of Wrath":              uldum,
    "Armagedillo":                  uldum,

    "Armored Goon":                 uldum,
    "Tomb Warden":                  uldum,

# neutral
    "Beaming Sidekick":             uldum,
    "Jar Dealer":                   uldum,
    "Mogu Cultist":                 uldum,
    "Murmy":                        uldum,

    "Bug Collector":                uldum,
    "Fishflinger":                  uldum,
    "Injured Tol'vir":              uldum,
    "Kobold Sandtrooper":           uldum,

    "Neferset Ritualist":           uldum,
    "Questing Explorer":            uldum,
    "Quicksand Elemental":          uldum,
    "Serpent Egg":                  uldum,

    "Spitting Camel":               uldum,
    "Temple Berserker":             uldum,
    "Vilefiend":                    uldum,
    "Zephrys the Great":            uldum,

    "Candletaker":                  uldum,
    "Desert Hare":                  uldum,
    "Dwarven Archaeologist":        uldum,
    "Generous Mummy":               uldum,

    "Golden Scarab":                uldum,
    "History Buff":                 uldum,
    "Infested Goblin":              uldum,
    "Mischief Maker":               uldum,

    "Vulpera Scoundrel":            uldum,
    "Body Wrapper":                 uldum,
    "Bone Wraith":                  uldum,
    "Conjured Mirage":              uldum,

    "Sunstruck Henchman":           uldum,
    "Desert Obelisk":               uldum,
    "Faceless Lurker":              uldum,
    "Mortuary Machine":             uldum,

    "Phalanx Commander":            uldum,
    "Wasteland Assassin":           uldum,
    "Blatant Decoy":                uldum,
    "Khartut Defender":             uldum,

    "Siamat":                       uldum,
    "Wasteland Scorpid":            uldum,
    "Wrapped Golem":                uldum,
    "Octosari":                     uldum,

    "Pit Crocolisk":                uldum,
    "Anubisath Warbringer":         uldum,
    "Colossus of the Moon":         uldum,
    "King Phaoris":                 uldum,

    "Living Monument":              uldum,

# druid
    "Embiggen":                     dragons,
    "Secure the Deck":              dragons,
    "Strength in Numbers":          dragons,
    "Treenforcements":              dragons,

    "Breath of Dreams":             dragons,
    "Shrubadier":                   dragons,
    "Aeroponics":                   dragons,
    "Emerald Explorer":             dragons,

    "Goru the Mightree":            dragons,
    "Ysera, Unleashed":             dragons,

# hunter
    "Clear the Way":                dragons,
    "Dwarven Sharpshooter":         dragons,
    "Toxic Reinforcements":         dragons,
    "Corrosive Breath":             dragons,

    "Phase Stalker":                dragons,
    "Diving Gryphon":               dragons,
    "Primordial Explorer":          dragons,
    "Stormhammer":                  dragons,

    "Dragonbane":                   dragons,
    "Veranus":                      dragons,

# mage
    "Arcane Breath":                dragons,
    "Elemental Allies":             dragons,
    "Learn Draconic":               dragons,
    "Violet Spellwing":             dragons,

    "Chenvaala":                    dragons,
    "Azure Explorer":               dragons,
    "Malygos, Aspect of Magic":     dragons,
    "Rolling Fireball":             dragons,

    "Dragoncaster":                 dragons,
    "Mana Giant":                   dragons,

# paladin
    "Righteous Cause":              dragons,
    "Sand Breath":                  dragons,
    "Sanctuary":                    dragons,
    "Bronze Explorer":              dragons,

    "Dragonrider Talritha":         dragons,
    "Sky Claw":                     dragons,
    "Lightforged Zealot":           dragons,
    "Nozdormu the Timeless":        dragons,

    "Amber Watcher":                dragons,
    "Lightforged Crusader":         dragons,

# priest
    "Whispers of EVIL":             dragons,
    "Disciple of Galakrond":        dragons,
    "Envoy of Lazul":               dragons,
    "Breath of the Infinite":       dragons,

    "Mindflayer Kaahrj":            dragons,
    "Fate Weaver":                  dragons,
    "Grave Rune":                   dragons,
    "Chronobreaker":                dragons,

    "Time Rip":                     dragons,
    "Galakrond, the Unspeakable":   dragons,
    "Murozond the Infinite":        dragons,

# rogue
    "Bloodsail Flybooter":          dragons,
    "Dragon's Hoard":               dragons,
    "Praise Galakrond!":            dragons,
    "Seal Fate":                    dragons,

    "Necrium Apothecary":           dragons,
    "Umbral Skulker":               dragons,
    "Stowaway":                     dragons,
    "Waxadred":                     dragons,

    "Candle Breath":                dragons,
    "Flik Skyshiv":                 dragons,
    "Galakrond, the Nightmare":     dragons,

# shaman
    "Invocation of Frost":          dragons,
    "Storm's Wrath":                dragons,
    "Surging Tempest":              dragons,
    "Lightning Breath":             dragons,

    "Squallhunter":                 dragons,
    "Bandersmosh":                  dragons,
    "Corrupt Elementalist":         dragons,
    "Cumulo-Maximus":               dragons,

    "Dragon's Pack":                dragons,
    "Nithogg":                      dragons,
    "Galakrond, the Tempest":       dragons,

# warlock
    "Rain of Fire":                 dragons,
    "Nether Breath":                dragons,
    "Dark Skies":                   dragons,
    "Dragonblight Cultist":         dragons,

    "Fiendish Rites":               dragons,
    "Veiled Worshipper":            dragons,
    "Crazed Netherwing":            dragons,
    "Abyssal Summoner":             dragons,

    "Galakrond, the Wretched":      dragons,
    "Valdris Felgorge":             dragons,
    "Zzeraku the Warped":           dragons,

# warrior
    "Sky Raider":                   dragons,
    "Ritual Chopper":               dragons,
    "Ancharrr":                     dragons,
    "Awaken!":                      dragons,

    "EVIL Quartermaster":           dragons,
    "Ramming Speed":                dragons,
    "Scion of Ruin":                dragons,
    "Skybarge":                     dragons,

    "Molten Breath":                dragons,
    "Galakrond, the Unbreakable":   dragons,
    "Deathwing, Mad Aspect":        dragons,

# neutral
    "Blazing Battlemage":           dragons,
    "Depth Charge":                 dragons,
    "Hot Air Balloon":              dragons,
    "Dragon Breeder":               dragons,

    "Evasive Chimaera":             dragons,
    "Grizzled Wizard":              dragons,
    "Parachute Brigand":            dragons,
    "Tasty Flyfish":                dragons,

    "Transmogrifier":               dragons,
    "Wyrmrest Purifier":            dragons,
    "Bad Luck Albatross":           dragons,
    "Blowtorch Saboteur":           dragons,

    "Dread Raven":                  dragons,
    "Fire Hawk":                    dragons,
    "Goboglide Tech":               dragons,
    "Living Dragonbreath":          dragons,

    "Scalerider":                   dragons,
    "Devoted Maniac":               dragons,
    "Dragonmaw Poacher":            dragons,
    "Evasive Feywing":              dragons,

    "Frizz Kindleroost":            dragons,
    "Hippogryph":                   dragons,
    "Hoard Pillager":               dragons,
    "Troll Batrider":               dragons,

    "Wing Commander":               dragons,
    "Zul'Drak Ritualist":           dragons,
    "Big Ol' Whelp":                dragons,
    "Chromatic Egg":                dragons,

    "Cobalt Spellkin":              dragons,
    "Faceless Corruptor":           dragons,
    "Kobold Stickyfinger":          dragons,
    "Platebreaker":                 dragons,

    "Shield of Galakrond":          dragons,
    "Skyfin":                       dragons,
    "Tentacled Menace":             dragons,
    "Camouflaged Dirigible":        dragons,

    "Evasive Wyrm":                 dragons,
    "Gyrocopter":                   dragons,
    "Kronx Dragonhoof":             dragons,
    "Utgarde Grapplesniper":        dragons,

    "Evasive Drakonid":             dragons,
    "Shu'ma":                       dragons,
    "Twin Tyrant":                  dragons,
    "Dragonqueen Alexstrasza":      dragons,

    "Sathrovarr":                   dragons,

# druid
    "Rising Winds":                 galakronds_awakening,
    "Steel Beetle":                 galakronds_awakening,
    "Winged Guardian":              galakronds_awakening,

# hunter
    "Fresh Scent":                  galakronds_awakening,
    "Chopshop Copter":              galakronds_awakening,
    "Rotnest Drake":                galakronds_awakening,

# mage
    "Arcane Amplifier":             galakronds_awakening,
    "Animated Avalanche":           galakronds_awakening,
    "The Amazing Reno":             galakronds_awakening,

# paladin
    "Air Raid":                     galakronds_awakening,
    "Shotbot":                      galakronds_awakening,
    "Scalelord":                    galakronds_awakening,

# priest
    "Cleric of Scales":             galakronds_awakening,
    "Dark Prophecy":                galakronds_awakening,
    "Aeon Reaver":                  galakronds_awakening,

# rogue
    "Skyvateer":                    galakronds_awakening,
    "Waxmancy":                     galakronds_awakening,
    "Shadow Sculptor":              galakronds_awakening,

# shaman
    "Explosive Evolution":          galakronds_awakening,
    "The Fist of Ra-den":           galakronds_awakening,
    "Eye of the Storm":             galakronds_awakening,

# warlock
    "Fiendish Servant":             galakronds_awakening,
    "Twisted Knowledge":            galakronds_awakening,
    "Chaos Gazer":                  galakronds_awakening,

# warrior
    "Boom Squad":                   galakronds_awakening,
    "Risky Skipper":                galakronds_awakening,
    "Bomb Wrangler":                galakronds_awakening,

# neutral
    "Licensed Adventurer":          galakronds_awakening,
    "Skydiving Instructor":         galakronds_awakening,
    "Escaped Manasaber":            galakronds_awakening,
    "Frenzied Felwing":             galakronds_awakening,

    "Grand Lackey Erkh":            galakronds_awakening,
    "Sky Gen'ral Kragg":            galakronds_awakening,
    "Boompistol Bully":             galakronds_awakening,
    "Hailbringer":                  galakronds_awakening,

# demon hunter
    "Blur":                         initiate,
    "Battlefiend":                  initiate,
    "Consume Magic":                initiate,
    "Feast of Souls":               initiate,

    "Mana Burn":                    initiate,
    "Twin Slice":                   initiate,
    "Ur'zul Horror":                initiate,
    "Umberwing":                    initiate,

    "Blade Dance":                  initiate,
    "Eye Beam":                     initiate,
    "Wrathscale Naga":              initiate,
    "Altruis the Outcast":          initiate,

    "Illidari Felblade":            initiate,
    "Raging Felscreamer":           initiate,
    "Soul Split":                   initiate,
    "Command the Illidari":         initiate,

    "Wrathspike Brute":             initiate,
    "Flamereaper":                  initiate,
    "Hulking Overfiend":            initiate,
    "Nethrandamus":                 initiate,

# demon hunter
    "Crimson Sigil Runner":         outland,
    "Furious Felfin":               outland,
    "Immolation Aura":              outland,
    "Netherwalker":                 outland,

    "Spectral Sight":               outland,
    "Ashtongue Battlelord":         outland,
    "Kayn Sunfury":                 outland,
    "Metamorphosis":                outland,

    "Imprisoned Antaen":            outland,
    "Warglaives of Azzinoth":       outland,
    "Fel Summoner":                 outland,
    "Priestess of Fury":            outland,

    "Pit Commander":                outland,

# druid
    "Ironbark":                     outland,
    "Archspore Msshi'fn":           outland,
    "Bogbeam":                      outland,
    "Fungal Fortunes":              outland,

    "Imprisoned Satyr":             outland,
    "Germination":                  outland,
    "Glowfly Swarm":                outland,
    "Marsh Hydra":                  outland,

    "Ysiel Windsinger":             outland,

# hunter
    "Helboar":                      outland,
    "Imprisoned Felmaw":            outland,
    "Pack Tactics":                 outland,
    "Scavenger's Ingenuity":        outland,

    "Augmented Porcupine":          outland,
    "Zixor, Apex Predator":         outland,
    "Mok'Nathal Lion":              outland,
    "Scrap Shot":                   outland,

    "Beastmaster Leoroxx":          outland,
    "Nagrand Slam":                 outland,

# mage
    "Apexis Smuggler":              outland,
    "Astromancer Solarian":         outland,
    "Imprisoned Observer":          outland,
    "Netherwind Portal":            outland,

    "Apexis Blast":                 outland,
    "Deep Freeze":                  outland,

# paladin
    "Aldor Attendant":              outland,
    "Imprisoned Sungill":           outland,
    "Hand of A'dal":                outland,
    "Libram of Wisdom":             outland,

    "Murgur Murgurgle":             outland,
    "Underlight Angling Rod":       outland,
    "Aldor Truthseeker":            outland,
    "Lady Liadrin":                 outland,

    "Libram of Hope":               outland,

# priest
    "Imprisoned Homunculus":        outland,
    "Reliquary of Souls":           outland,
    "Renew":                        outland,
    "Dragonmaw Overseer":           outland,

    "Psyche Split":                 outland,
    "Skeletal Dragon":              outland,
    "Soul Mirror":                  outland,

# rogue
    "Blackjack Stunner":            outland,
    "Ambush":                       outland,
    "Ashtongue Slayer":             outland,
    "Bamboozle":                    outland,

    "Dirty Tricks":                 outland,
    "Shadowjeweler Hanar":          outland,
    "Akama":                        outland,
    "Greyheart Sage":               outland,

    "Cursed Vagrant":               outland,

# shaman
    "Bogstrok Clacker":             outland,
    "Lady Vashj":                   outland,
    "Serpentshrine Portal":         outland,
    "Totemic Reflection":           outland,

    "Torrent":                      outland,
    "Vivid Spores":                 outland,
    "Boggspine Knuckles":           outland,
    "Shattered Rumbler":            outland,

    "The Lurker Below":             outland,

# warlock
    "Shadow Council":               outland,
    "Unstable Felbolt":             outland,
    "Imprisoned Scrap Imp":         outland,
    "Kanrethad Ebonlocke":          outland,

    "Darkglare":                    outland,
    "Nightshade Matron":            outland,
    "The Dark Portal":              outland,
    "Keli'dan the Breaker":         outland,

    "Enhanced Dreadlord":           outland,

# warrior
    "Imprisoned Gan'arg":           outland,
    "Sword and Board":              outland,
    "Bladestorm":                   outland,
    "Corsair Cache":                outland,

    "Bonechewer Raider":            outland,
    "Bulwark of Azzinoth":          outland,
    "Warmaul Challenger":           outland,
    "Kargath Bladefist":            outland,

    "Scrap Golem":                  outland,
    "Bloodboil Brute":              outland,

# neutral
    "Ethereal Augmerchant":         outland,
    "Guardian Augmerchant":         outland,
    "Infectious Sporeling":         outland,
    "Soulbound Ashtongue":          outland,

    "Bonechewer Brawler":           outland,
    "Imprisoned Vilefiend":         outland,
    "Mo'arg Artificer":             outland,
    "Rustsworn Initiate":           outland,

    "Frozen Shadoweaver":           outland,
    "Teron Gorefiend":              outland,
    "Terrorguard Escapee":          outland,
    "Burrowing Scorpid":            outland,

    "Disguised Wanderer":           outland,
    "Magtheridon":                  outland,
    "Maiev Shadowsong":             outland,
    "Replicat-o-tron":              outland,

    "Rustsworn Cultist":            outland,
    "Al'ar":                        outland,
    "Waste Warden":                 outland,
    "Scavenging Shivarra":          outland,

    "Bonechewer Vanguard":          outland,
    "Scrapyard Colossus":           outland,

# demon hunter
    "Demon Companion":              scholomance,
    "Double Jump":                  scholomance,
    "Felosophy":                    scholomance,
    "Spirit Jailer":                scholomance,

    "Trueaim Crescent":             scholomance,
    "Soul Shear":                   scholomance,
    "Ace Hunter Kreen":             scholomance,
    "Magehunter":                   scholomance,

    "Shardshatter Mystic":          scholomance,
    "Glide":                        scholomance,
    "Marrowslicer":                 scholomance,
    "Star Student Stelina":         scholomance,

    "Vilefiend Trainer":            scholomance,
    "Blood Herald":                 scholomance,
    "Soulshard Lapidary":           scholomance,
    "Cycle of Hatred":              scholomance,

    "Fel Guardians":                scholomance,
    "Soulciologist Malicia":        scholomance,
    "Ancient Void Hound":           scholomance,

# druid
    "Lightning Bloom":              scholomance,
    "Adorable Infestation":         scholomance,
    "Nature Studies":               scholomance,
    "Partner Assignment":           scholomance,

    "Gibberling":                   scholomance,
    "Shan'do Wildclaw":             scholomance,
    "Speaker Gidra":                scholomance,
    "Groundskeeper":                scholomance,

    "Teacher's Pet":                scholomance,
    "Twilight Runner":              scholomance,
    "Forest Warden Omu":            scholomance,
    "Runic Carvings":               scholomance,

    "Guardian Animals":             scholomance,
    "Survival of the Fittest":      scholomance,

# hunter
    "Adorable Infestation":         scholomance,
    "Carrion Studies":              scholomance,
    "Demon Companion":              scholomance,
    "Overwhelm":                    scholomance,

    "Trueaim Crescent":             scholomance,
    "Wolpertinger":                 scholomance,
    "Ace Hunter Kreen":             scholomance,
    "Bloated Python":               scholomance,

    "Professor Slate":              scholomance,
    "Shan'do Wildclaw":             scholomance,
    "Krolusk Barkstripper":         scholomance,
    "Blood Herald":                 scholomance,

    "Teacher's Pet":                scholomance,
    "Guardian Animals":             scholomance,

# mage
    "Brain Freeze":                 scholomance,
    "Devolving Missiles":           scholomance,
    "Lab Partner":                  scholomance,
    "Primordial Studies":           scholomance,

    "Wand Thief":                   scholomance,
    "Cram Session":                 scholomance,
    "Trick Totem":                  scholomance,
    "Combustion":                   scholomance,

    "Firebrand":                    scholomance,
    "Potion of Illusion":           scholomance,
    "Wyrm Weaver":                  scholomance,
    "Jandice Barov":                scholomance,

    "Mozaki, Master Duelist":       scholomance,
    "Ras Frostwhisper":             scholomance,

# paladin
    "First Day of School":          scholomance,
    "Shield of Honor":              scholomance,
    "Wave of Apathy":               scholomance,
    "Argent Braggart":              scholomance,

    "Ceremonial Maul":              scholomance,
    "Gift of Luminance":            scholomance,
    "Goody Two-Shields":            scholomance,
    "Lord Barov":                   scholomance,

    "High Abbess Alura":            scholomance,
    "Blessing of Authority":        scholomance,
    "Devout Pupil":                 scholomance,
    "Judicious Junior":             scholomance,

    "Commencement":                 scholomance,
    "Turalyon, the Tenured":        scholomance,

# priest
    "Raise Dead":                   scholomance,
    "Draconic Studies":             scholomance,
    "Frazzled Freshman":            scholomance,
    "Wave of Apathy":               scholomance,

    "Power Word: Feast":            scholomance,
    "Gift of Luminance":            scholomance,
    "Mindrender Illucia":           scholomance,
    "Brittlebone Destroyer":        scholomance,

    "Cabal Acolyte":                scholomance,
    "Disciplinarian Gandling":      scholomance,
    "High Abbess Alura":            scholomance,
    "Devout Pupil":                 scholomance,

    "Initiation":                   scholomance,
    "Flesh Giant":                  scholomance,

# rogue
    "Brain Freeze":                 scholomance,
    "Secret Passage":               scholomance,
    "Wand Thief":                   scholomance,
    "Plagiarize":                   scholomance,

    "Coerce":                       scholomance,
    "Self-Sharpening Sword":        scholomance,
    "Vulpera Toxinblade":           scholomance,
    "Infiltrator Lilian":           scholomance,

    "Potion of Illusion":           scholomance,
    "Shifty Sophomore":             scholomance,
    "Steeldancer":                  scholomance,
    "Cutting Class":                scholomance,

    "Doctor Krastinov":             scholomance,
#    "Jandice Barov":                scholomance,

# shaman
    "Lightning Bloom":              scholomance,
    "Devolving Missiles":           scholomance,
    "Primordial Studies":           scholomance,
    "Diligent Notetaker":           scholomance,

    "Rune Dagger":                  scholomance,
    "Trick Totem":                  scholomance,
    "Instructor Fireheart":         scholomance,
    "Molten Blast":                 scholomance,

    "Speaker Gidra":                scholomance,
    "Groundskeeper":                scholomance,
    "Ras Frostwhisper":             scholomance,
    "Totem Goliath":                scholomance,

    "Runic Carvings":               scholomance,
    "Tidal Wave":                   scholomance,

# warlock
    "Raise Dead":                   scholomance,
    "Demonic Studies":              scholomance,
    "Felosophy":                    scholomance,
    "Spirit Jailer":                scholomance,

    "Boneweb Egg":                  scholomance,
    "Soul Shear":                   scholomance,
    "School Spirits":               scholomance,
    "Shadowlight Scholar":          scholomance,

    "Brittlebone Destroyer":        scholomance,
    "Disciplinarian Gandling":      scholomance,
    "Void Drinker":                 scholomance,
    "Soulciologist Malicia":        scholomance,

    "Archwitch Willow":             scholomance,
 #   "Flesh Giant":                  scholomance,

# warrior
    "Athletic Studies":             scholomance,
    "Shield of Honor":              scholomance,
    "In Formation!":                scholomance,
    "Ceremonial Maul":              scholomance,

    "Coerce":                       scholomance,
#    "Lord Barov":                   scholomance,
    "Playmaker":                    scholomance,
    "Reaper's Scythe":              scholomance,

    "Steeldancer":                  scholomance,
    "Cutting Class":                scholomance,
    "Doctor Krastinov":             scholomance,
    "Commencement":                 scholomance,

    "Troublemaker":                 scholomance,
    "Rattlegore":                   scholomance,

# neutral
    "Desk Imp":                     scholomance,
    "Animated Broomstick":          scholomance,
    "Intrepid Initiate":            scholomance,
    "Pen Flinger":                  scholomance,

    "Sphere of Sapience":           scholomance,
    "Tour Guide":                   scholomance,
    "Cult Neophyte":                scholomance,
    "Manafeeder Panthara":          scholomance,

    "Sneaky Delinquent":            scholomance,
    "Transfer Student":             scholomance,
    "Wandmaker":                    scholomance,
    "Educated Elekk":               scholomance,

    "Enchanted Cauldron":           scholomance,
    "Robes of Protection":          scholomance,
    "Voracious Reader":             scholomance,
    "Crimson Hothead":              scholomance,

    "Divine Rager":                 scholomance,
    "Fishy Flyer":                  scholomance,
    "Lorekeeper Polkelt":           scholomance,
    "Wretched Tutor":               scholomance,

    "Headmaster Kel'Thuzad":        scholomance,
    "Lake Thresher":                scholomance,
    "Ogremancer":                   scholomance,
    "Steward of Scrolls":           scholomance,

    "Vectus":                       scholomance,
    "Onyx Magescribe":              scholomance,
    "Smug Senior":                  scholomance,
    "Sorcerous Substitute":         scholomance,

    "Keymaster Alabaster":          scholomance,
    "Plagued Protodrake":           scholomance,


# demon hunter
    "Felscream Blast":              darkmoon,
    "Illidari Studies":             darkmoon,
    "Throw Glaive":                 darkmoon,
    "Felfire Deadeye":              darkmoon,

    "Redeemed Pariah":              darkmoon,
    "Acrobatics":                   darkmoon,
    "Dreadlord's Bite":             darkmoon,
    "Felsteel Executioner":         darkmoon,

    "Insatiable Felhound":          darkmoon,
    "Line Hopper":                  darkmoon,
    "Luckysoul Hoarder":            darkmoon,
    "Relentless Pursuit":           darkmoon,

    "Stiltstepper":                 darkmoon,
    "Felsaber":                     darkmoon,
    "Il'gynoth":                    darkmoon,
    "Renowned Performer":           darkmoon,

    "Zai, the Incredible":          darkmoon,
    "Bladed Lady":                  darkmoon,
    "Expendable Performers":        darkmoon,

# druid
    "Guidance":                     darkmoon,
    "Resizing Pouch":               darkmoon,
    "Guess the Weight":             darkmoon,
    "Lunar Eclipse":                darkmoon,

    "Solar Eclipse":                darkmoon,
    "Dreaming Drake":               darkmoon,
    "Faire Arborist":               darkmoon,
    "Kiri, Chosen of Elune":        darkmoon,

    "Moontouched Amulet":           darkmoon,
    "Arbor Up":                     darkmoon,
    "Greybough":                    darkmoon,
    "Umbral Owl":                   darkmoon,

    "Cenarion Ward":                darkmoon,
    "Fizzy Elemental":              darkmoon,

# hunter
    "Mystery Winner":               darkmoon,
    "Resizing Pouch":               darkmoon,
    "Bola Shot":                    darkmoon,
    "Dancing Cobra":                darkmoon,

    "Don't Feed the Animals":       darkmoon,
    "Felfire Deadeye":              darkmoon,
    "Open the Cages":               darkmoon,
    "Petting Zoo":                  darkmoon,

    "Saddlemaster":                 darkmoon,
    "Rinling's Rifle":              darkmoon,
    "Trampling Rhino":              darkmoon,
    "Maxima Blastenheimer":         darkmoon,

    "Darkmoon Tonk":                darkmoon,
    "Jewel of N'Zoth":              darkmoon,

# mage
    "Glacier Racer":                darkmoon,
    "Confection Cyclone":           darkmoon,
    "Conjure Mana Biscuit":         darkmoon,
    "Deck of Lunacy":               darkmoon,

    "Game Master":                  darkmoon,
    "Imprisoned Phoenix":           darkmoon,
    "Rigged Faire Game":            darkmoon,
    "Occult Conjurer":              darkmoon,

    "Ring Toss":                    darkmoon,
    "Firework Elemental":           darkmoon,
    "Keywarden Ivory":              darkmoon,
    "Sayge, Seer of Darkmoon":      darkmoon,

    "Mask of C'Thun":               darkmoon,
    "Grand Finale":                 darkmoon,

# paladin
    "Oh My Yogg!":                  darkmoon,
    "Redscale Dragontamer":         darkmoon,
    "Snack Run":                    darkmoon,
    "Carnival Barker":              darkmoon,

    "Day at the Faire":             darkmoon,
    "Imprisoned Celestial":         darkmoon,
    "Balloon Merchant":             darkmoon,
    "Barricade":                    darkmoon,

    "Rally!":                       darkmoon,
    "Carousel Gryphon":             darkmoon,
    "Lothraxion the Redeemed":      darkmoon,
    "Hammer of the Naaru":          darkmoon,

    "Libram of Judgment":           darkmoon,
    "High Exarch Yrel":             darkmoon,

# priest
    "Insight":                      darkmoon,
    "Fairground Fool":              darkmoon,
    "Nazmani Bloodweaver":          darkmoon,
    "Palm Reading":                 darkmoon,

    "Auspicious Spirits":           darkmoon,
    "Hysteria":                     darkmoon,
    "Lightsteed":                   darkmoon,
#    "Rally!":                       darkmoon,

    "The Nameless One":             darkmoon,
    "Dark Inquisitor Xanesh":       darkmoon,
    "Fortune Teller":               darkmoon,
    "G'huun the Blood God":         darkmoon,

    "Idol of Y'Shaarj":             darkmoon,
    "Blood of G'huun":              darkmoon,

# rogue
    "Prize Plunderer":              darkmoon,
    "Foxy Fraud":                   darkmoon,
    "Nitroboost Poison":            darkmoon,
    "Shadow Clone":                 darkmoon,

    "Shenanigans":                  darkmoon,
    "Sweet Tooth":                  darkmoon,
    "Swindle":                      darkmoon,
    "Tenwu of the Red Smoke":       darkmoon,

    "Sparkjoy Cheat":               darkmoon,
    "Ticket Master":                darkmoon,
    "Cloak of Shadows":             darkmoon,
#    "Keywarden Ivory":              darkmoon,

    "Malevolent Strike":            darkmoon,
    "Grand Empress Shek'zara":      darkmoon,

# shaman
    "Guidance":                     darkmoon,
    "Revolve":                      darkmoon,
    "Cagematch Custodian":          darkmoon,
    "Deathmatch Pavilion":          darkmoon,

    "Imprisoned Phoenix":           darkmoon,
    "Landslide":                    darkmoon,
    "Grand Totem Eys'or":           darkmoon,
    "Magicfin":                     darkmoon,

    "Pit Master":                   darkmoon,
    "Stormstrike":                  darkmoon,
    "Whack-A-Gnoll Hammer":         darkmoon,
    "Dunk Tank":                    darkmoon,

    "Inara Stormcrash":             darkmoon,
    "Mistrunner":                   darkmoon,

# warlock
    "Wicked Whispers":              darkmoon,
    "Midway Maniac":                darkmoon,
    "Backfire":                     darkmoon,
    "Free Admission":               darkmoon,

    "Luckysoul Hoarder":            darkmoon,
    "Man'ari Mosher":               darkmoon,
    "Revenant Rascal":              darkmoon,
    "Cascading Disaster":           darkmoon,
 
    "Fire Breather":                darkmoon,
    "Hysteria":                     darkmoon,
    "Deck of Chaos":                darkmoon,
    "Envoy Rustwix":                darkmoon,

    "Ring Matron":                  darkmoon,
    "Tickatus":                     darkmoon,

# warrior
    "Spiked Wheel":                 darkmoon,
    "Stage Dive":                   darkmoon,
    "Bumper Car":                   darkmoon,
    "E.T.C., God of Metal":         darkmoon,

    "Minefield":                    darkmoon,
    "Nitroboost Poison":            darkmoon,
    "Ringmaster's Baton":           darkmoon,
    "Stage Hand":                   darkmoon,

    "Feat of Strength":             darkmoon,
    "Ironclad":                     darkmoon,
    "Barricade":                    darkmoon,
    "Sword Eater":                  darkmoon,

    "Ringmaster Whatley":           darkmoon,
    "Tent Trasher":                 darkmoon,

# neutral
    "Armor Vendor":                 darkmoon,
    "Safety Inspector":             darkmoon,
    "Costumed Entertainer":         darkmoon,
    "Crabrider":                    darkmoon,

    "Horrendous Growth":            darkmoon,
    "Parade Leader":                darkmoon,
    "Prize Vendor":                 darkmoon,
    "Rock Rager":                   darkmoon,

    "Showstopper":                  darkmoon,
    "Wriggling Horror":             darkmoon,
    "Banana Vendor":                darkmoon,
    "Darkmoon Dirigible":           darkmoon,

    "Darkmoon Statue":              darkmoon,
    "Deathwarden":                  darkmoon,
    "Gyreworm":                     darkmoon,
    "Inconspicuous Rider":          darkmoon,

    "K'thir Ritualist":             darkmoon,
    "Circus Amalgam":               darkmoon,
    "Circus Medic":                 darkmoon,
    "Fantastic Firebird":           darkmoon,

    "Knife Vendor":                 darkmoon,
    "Derailed Coaster":             darkmoon,
    "Fleethoof Pearltusk":          darkmoon,
    "Moonfang":                     darkmoon,

    "Optimistic Ogre":              darkmoon,
    "Claw Machine":                 darkmoon,
    "Silas Darkmoon":               darkmoon,
    "Strongman":                    darkmoon,

    "Carnival Clown":               darkmoon,
    "N'Zoth, God of the Deep":      darkmoon,
    "Runaway Blackwing":            darkmoon,
    "C'Thun, the Shattered":        darkmoon,

    "Darkmoon Rabbit":              darkmoon,
    "Yogg-Saron, Master of Fate":   darkmoon,
    "Y'Shaarj, the Defiler":        darkmoon,

# demon hunter
    "Sigil of Silence":             barrens,
    "Fury (Rank 1)":                barrens,
    "Tuskpiercer":                  barrens,
    "Razorboar":                    barrens,

    "Sigil of Flame":               barrens,
    "Sigil of Summoning":           barrens,
    "Felrattler":                   barrens,
    "Razorfen Beastmaster":         barrens,

    "Vile Call":                    barrens,
    "Kurtrus Ashfallen":            barrens,
    "Vengeful Spirit":              barrens,
    "Death Speaker Blackthorn":     barrens,

    "Taintheart Tormenter":         barrens,

# druid
    "Living Seed (Rank 1)":         barrens,
    "Mark of the Spikeshell":       barrens,
    "Razormane Battleguard":        barrens,
    "Thorngrowth Sentries":         barrens,

    "Fangbound Druid":              barrens,
    "Guff Runetotem":               barrens,
    "Plaguemaw the Rotting":        barrens,
    "Pride's Fury":                 barrens,

    "Thickhide Kodo":               barrens,
    "Lady Anacondra":               barrens,
    "Druid of the Plains":          barrens,
    "Celestial Alignment":          barrens,

    "Deviate Dreadfang":            barrens,

# hunter
    "Serpentbloom":                 barrens,
    "Sunscale Raptor":              barrens,
    "Wound Prey":                   barrens,
    "Kolkar Pack Runner":           barrens,

    "Prospector's Caravan":         barrens,
    "Tame Beast (Rank 1)":          barrens,
    "Pack Kodo":                    barrens,
    "Tavish Stormpike":             barrens,
 
    "Piercing Shot":                barrens,
    "Sin'dorei Scentfinder":        barrens,
    "Venomstrike Bow":              barrens,   
    "Warsong Wrangler":             barrens,

    "Barak Kodobane":               barrens,

# mage
    "Flurry (Rank 1)":              barrens,
    "Wildfire":                     barrens,
    "Runed Orb":                    barrens,
    "Arcane Luminary":              barrens,

    "Frostweave Dungeoneer":        barrens,
    "Oasis Ally":                   barrens,
    "Rimetongue":                   barrens,
    "Shattering Blast":             barrens,

    "Reckless Apprentice":          barrens,
    "Varden Dawngrasp":             barrens,
    "Refreshing Spring Water":      barrens,
    "Floecaster":                   barrens,

    "Mordresh Fire Eye":            barrens,

# paladin
    "Conviction (Rank 1)":          barrens,
    "Galloping Savior":             barrens,
    "Judgment of Justice":          barrens,
    "Knight of Anointment":         barrens,

    "Soldier's Caravan":            barrens,
    "Sword of the Fallen":          barrens,
    "Northwatch Commander":         barrens,
    "Seedcloud Buckler":            barrens,

    "Cariel Roame":                 barrens,
    "Invigorating Sermon":          barrens,
    "Veteran Warmedic":             barrens,
    "Cannonmaster Smythe":          barrens,

    "Party Up!":                    barrens,

# priest
    "Desperate Prayer":             barrens,
    "Cleric of An'she":             barrens,
    "Condemn (Rank 1)":             barrens,
    "Serena Bloodfeather":          barrens,

    "Soothsayer's Caravan":         barrens,
    "Devouring Plague":             barrens,
    "Devout Dungeoneer":            barrens,
    "Void Flayer":                  barrens,

    "Xyrella":                      barrens,
    "Against All Odds":             barrens,
    "Priest of An'she":             barrens,
    "Lightshower Elemental":        barrens,

    "Power Word: Fortitude":        barrens,

# rogue
    "Paralytic Poison":             barrens,
    "Savory Deviate Delight":       barrens,
    "Silverleaf Poison":            barrens,
    "Yoink!":                       barrens,

    "Wicked Stab (Rank 1)":         barrens,
    "Efficient Octo-bot":           barrens,
    "Field Contact":                barrens,
    "Shroud of Concealment":        barrens,

    "Swinetusk Shank":              barrens,
    "Water Moccasin":               barrens,
    "Apothecary Helbrim":           barrens,
    "Oil Rig Ambusher":             barrens,

    "Scabbs Cutterbutter":          barrens,

# shaman
    "Spawnpool Forager":            barrens,
    "Wailing Vapor":                barrens,
    "Chain Lightning (Rank 1)":     barrens,
    "Perpetual Flame":              barrens,

    "South Coast Chieftain":        barrens,
    "Tinyfin's Caravan":            barrens,
    "Arid Stormer":                 barrens,
    "Nofin Can Stop Us":            barrens,

    "Primal Dungeoneer":            barrens,
    "Bru'kan":                      barrens,
    "Earth Revenant":               barrens,
    "Firemancer Flurgl":            barrens,

    "Lilypad Lurker":               barrens,

# warlock
    "Altar of Fire":                barrens,
    "Final Gasp":                   barrens,
    "Grimoire of Sacrifice":        barrens,
    "Apothecary's Caravan":         barrens,

    "Imp Swarm (Rank 1)":           barrens,
    "Unstable Shadow Blast":        barrens,
    "Blood Shard Bristleback":      barrens,
    "Kabal Outfitter":              barrens,

    "Tamsin Roame":                 barrens,
    "Soul Rend":                    barrens,
    "Stealer of Souls":             barrens,
    "Neeru Fireblade":              barrens,

    "Barrens Scavenger":            barrens,

# warrior
    "Warsong Envoy":                barrens,
    "Whetstone Hatchet":            barrens,
    "Bulk Up":                      barrens,
    "Conditioning (Rank 1)":        barrens,

    "Man-at-Arms":                  barrens,
    "Rokara":                       barrens,
    "Outrider's Axe":               barrens,
    "Rancor":                       barrens,

    "Whirling Combatant":           barrens,
    "Mor'shan Elite":               barrens,
    "Stonemaul Anchorman":          barrens,
    "Kresh, Lord of Turtling":      barrens,

    "Overlord Saurfang":            barrens,

# neutral
    "Kindling Elemental":           barrens,
    "Meeting Stone":                barrens,
    "Far Watch Post":               barrens,
    "Hecklefang Hyena":             barrens,

    "Lushwater Murcenary":          barrens,
    "Lushwater Scout":              barrens,
    "Oasis Thrasher":               barrens,
    "Peon":                         barrens,

    "Talented Arcanist":            barrens,
    "Toad of the Wilds":            barrens,
    "Archdruid Naralex":            barrens,
    "Barrens Trapper":              barrens,

    "Crossroads Gossiper":          barrens,
    "Death's Head Cultist":         barrens,
    "Devouring Ectoplasm":          barrens,
    "Hog Rancher":                  barrens,

    "Horde Operative":              barrens,
    "Mankrik":                      barrens,
    "Mor'shan Watch Post":          barrens,
    "Ratchet Privateer":            barrens,

    "Sunwell Initiate":             barrens,
    "Venomous Scorpid":             barrens,
    "Blademaster Samuro":           barrens,
    "Crossroads Watch Post":        barrens,

    "Darkspear Berserker":          barrens,
    "Gruntled Patron":              barrens,
    "Injured Marauder":             barrens,
    "Kazakus, Golem Shaper":        barrens,

    "Southsea Scoundrel":           barrens,
    "Spirit Healer":                barrens,
    "Barrens Blacksmith":           barrens,
    "Burning Blade Acolyte":        barrens,

    "Gold Road Grunt":              barrens,
    "Razormane Raider":             barrens,
    "Shadow Hunter Vol'jin":        barrens,
    "Taurajo Brave":                barrens,

    "Kargal Battlescar":            barrens,
    "Mutanus the Devourer":         barrens,
    "Selfless Sidekick":            barrens,
    "Primordial Protector":         barrens,

# demon hunter
    "Final Showdown":               stormwind,
    "Metamorfin":                   stormwind,
    "Sigil of Alacrity":            stormwind,
    "Fel Barrage":                  stormwind,

    "Chaos Leech":                  stormwind,
    "Crow's Nest Lookout":          stormwind,
    "Lion's Frenzy":                stormwind,
    "Felgorger":                    stormwind,

    "Persistent Peddler":           stormwind,
    "Need for Greed":               stormwind,
    "Proving Grounds":              stormwind,
    "Irebound Brute":               stormwind,

    "Jace Darkweaver":              stormwind,

# druid
    "Druid of the Reef":            stormwind,
    "Lost in the Park":             stormwind,
    "Sow the Soil":                 stormwind,
    "Vibrant Squirrel":             stormwind,

    "Composting":                   stormwind,
    "Jerry Rig Carpenter":          stormwind,
    "Moonlit Guidance":             stormwind,
    "Wickerclaw":                   stormwind,

    "Oracle of Elune":              stormwind,
    "Kodo Mount":                   stormwind,
    "Park Panther":                 stormwind,
    "Best in Shell":                stormwind,

    "Sheldras Moontree":            stormwind,

# hunter
    "Devouring Swarm":              stormwind,
    "Defend the Dwarven District":  stormwind,
    "Leatherworking Kit":           stormwind,
    "Doggie Biscuit":               stormwind,

    "Aimed Shot":                   stormwind,
    "Ramming Mount":                stormwind,
    "Stormwind Piper":              stormwind,
    "Monstrous Parrot":             stormwind,

    "Rodent Nest":                  stormwind,
    "Defias Blastfisher":           stormwind,
    "Imported Tarantula":           stormwind,
    "The Rat King":                 stormwind,

    "Rats of Extraordinary Size":   stormwind,

# mage
    "Hot Streak":                   stormwind,
    "First Flame":                  stormwind,
    "Sorcerer's Gambit":            stormwind,
    "Celestial Ink Set":            stormwind,

    "Ignite":                       stormwind,
    "Prestor's Pyromancer":         stormwind,
    "Deepwater Evoker":             stormwind,
    "Fire Sale":                    stormwind,

    "Arcane Overflow":              stormwind,
    "Sanctum Chandler":             stormwind,
    "Grey Sage Parrot":             stormwind,
    "Clumsy Courier":               stormwind,

    "Grand Magus Antonidas":        stormwind,

# paladin
    "Blessed Goods":                stormwind,
    "Prismatic Jewel Kit":          stormwind,
    "Rise to the Occasion":         stormwind,
    "City Tax":                     stormwind,

    "Noble Mount":                  stormwind,
    "Alliance Bannerman":           stormwind,
    "Catacomb Guard":               stormwind,
    "Lightbringer's Hammer":        stormwind,

    "Righteous Defense":            stormwind,
    "First Blade of Wrynn":         stormwind,
    "Sunwing Squawker":             stormwind,
    "Wealth Redistributor":         stormwind,

    "Highlord Fordragon":           stormwind,

# priest
    "Call of the Grave":            stormwind,
    "Seek Guidance":                stormwind,
    "Shadowcloth Needle":           stormwind,
    "Shard of the Naaru":           stormwind,

    "Voidtouched Attendant":        stormwind,
    "Defias Leper":                 stormwind,
    "Twilight Deceptor":            stormwind,
    "Amulet of Undying":            stormwind,

    "Copycat":                      stormwind,
    "Psyfiend":                     stormwind,
    "Void Shard":                   stormwind,
    "Darkbishop Benedictus":        stormwind,

    "Elekk Mount":                  stormwind,

# rogue
    "Blackwater Cutlass":           stormwind,
    "Find the Imposter":            stormwind,
    "Parrrley":                     stormwind,
    "SI:7 Extortion":               stormwind,

    "Garrote":                      stormwind,
    "Maestra of the Masquerade":    stormwind,
    "Loan Shark":                   stormwind,
    "SI:7 Operative":               stormwind,

    "Counterfeit Blade":            stormwind,
    "Edwin, Defias Kingpin":        stormwind,
    "SI:7 Informant":               stormwind,
    "Sketchy Information":          stormwind,

    "SI:7 Assassin":                stormwind,

# shaman
    "Command the Elements":         stormwind,
    "Investment Opportunity":       stormwind,
    "Overdraft":                    stormwind,
    "Auctionhouse Gavel":           stormwind,

    "Bolner Hammerbeak":            stormwind,
    "Brilliant Macaw":              stormwind,
    "Charged Call":                 stormwind,
    "Cookie the Cook":              stormwind,

    "Canal Slogger":                stormwind,
    "Granite Forgeborn":            stormwind,
    "Spirit Alpha":                 stormwind,
    "Suckerhook":                   stormwind,

    "Tiny Toys":                    stormwind,

# warlock
    "Shadowblade Slinger":          stormwind,
    "The Demon Seed":               stormwind,
    "Touch of the Nathrezim":       stormwind,
    "Wicked Shipment":              stormwind,

    "Bloodbound Imp":               stormwind,
    "Dreaded Mount":                stormwind,
    "Dark Alley Pact":              stormwind,
    "Demonic Assault":              stormwind,

    "Hullbreaker":                  stormwind,
    "Runed Mithril Rod":            stormwind,
    "Shady Bartender":              stormwind,
    "Anetheron":                    stormwind,

    "Entitled Customer":            stormwind,

# warrior
    "Provoke":                      stormwind,
    "Raid the Docks":               stormwind,
    "Shiver Their Timbers!":        stormwind,
    "Harbor Scamp":                 stormwind,

    "Man the Cannons":              stormwind,
    "Cargo Guard":                  stormwind,
    "Defias Cannoneer":             stormwind,
    "Heavy Plate":                  stormwind,

    "Stormwind Freebooter":         stormwind,
    "Blacksmithing Hammer":         stormwind,
    "Remote-Controlled Golem":      stormwind,
    "Cowardly Grunt":               stormwind,

    "Lothar":                       stormwind,

# neutral
    "Elwynn Boar":                  stormwind,
    "Peasant":                      stormwind,
    "Stockades Guard":              stormwind,
    "Auctioneer Jaxon":             stormwind,

    "Deeprun Engineer":             stormwind,
    "Encumbered Pack Mule":         stormwind,
    "Florist":                      stormwind,
    "Mailbox Dancer":               stormwind,

    "Pandaren Importer":            stormwind,
    "SI:7 Skulker":                 stormwind,
    "Stockades Prisoner":           stormwind,
    "Enthusiastic Banker":          stormwind,

    "Entrapped Sorceress":          stormwind,
    "Flightmaster Dungar":          stormwind,
    "Golakka Glutton":              stormwind,
    "Impatient Shopkeep":           stormwind,

    "Nobleman":                     stormwind,
    "Northshire Farmer":            stormwind,
    "Package Runner":               stormwind,
    "Rustrot Viper":                stormwind,

    "Traveling Merchant":           stormwind,
    "Two-Faced Investor":           stormwind,
    "Cheesemonger":                 stormwind,
    "Guild Trader":                 stormwind,

    "Multicaster":                  stormwind,
    "Royal Librarian":              stormwind,
    "Spice Bread Baker":            stormwind,
    "Stubborn Suspect":             stormwind,

    "Lion's Guard":                 stormwind,
    "Stormwind Guard":              stormwind,
    "Battleground Battlemaster":    stormwind,
    "City Architect":               stormwind,

    "Cornelius Roame":              stormwind,
    "Lady Prestor":                 stormwind,
    "Mr. Smite":                    stormwind,
    "Goliath, Sneed's Masterpiece": stormwind,

    "Maddest Bomber":               stormwind,
    "Mo'arg Forgefiend":            stormwind,
    "Varian, King of Stormwind":    stormwind,
    "Goldshire Gnoll":              stormwind,

# demon hunter
    "Dreadprison Glaive":           alterac_valley,
    "Wings of Hate (Rank 1)":       alterac_valley,
    "Battleworn Vanguard":          alterac_valley,
    "Field of Strife":              alterac_valley,

    "Keen Reflex":                  alterac_valley,
    "Flag Runner":                  alterac_valley,
    "Flanking Maneuver":            alterac_valley,
    "Razorglaive Sentinel":         alterac_valley,

    "Warden of Chains":             alterac_valley,
    "Sigil of Reckoning":           alterac_valley,
    "Caria Felsoul":                alterac_valley,
    "Kurtrus, Demon-Render":        alterac_valley,

    "Ur'zul Giant":                 alterac_valley,

# druid
    "Capture Coldtooth Mine":       alterac_valley,
    "Clawfury Adept":               alterac_valley,
    "Frostwolf Kennels":            alterac_valley,
    "Heart of the Wild":            alterac_valley,

    "Pathmaker":                    alterac_valley,
    "Pride Seeker":                 alterac_valley,
    "Dire Frostwolf":               alterac_valley,
    "Raid Negotiator":              alterac_valley,

    "Wing Commander Mulverick":     alterac_valley,
    "Boomkin":                      alterac_valley,
    "Wildheart Guff":               alterac_valley,
    "Frostsaber Matriarch":         alterac_valley,

    "Scale of Onyxia":              alterac_valley,

# hunter
    "Bloodseeker":                  alterac_valley,
    "Dragonbane Shot":              alterac_valley,
    "Dun Baldar Bunker":            alterac_valley,
    "Furious Howl":                 alterac_valley,

    "Ice Trap":                     alterac_valley,
    "Ram Tamer":                    alterac_valley,
    "Revive Pet":                   alterac_valley,
    "Spring the Trap":              alterac_valley,

    "Stormpike Battle Ram":         alterac_valley,
    "Pet Collector":                alterac_valley,
    "Beaststalker Tavish":          alterac_valley,
    "Mountain Bear":                alterac_valley,

    "Wing Commander Ichman":        alterac_valley,

# mage
    "Shivering Sorceress":          alterac_valley,
    "Amplified Snowflurry":         alterac_valley,
    "Siphon Mana":                  alterac_valley,
    "Build a Snowman":              alterac_valley,

    "Arcane Brilliance":            alterac_valley,
    "Deep Breath":                  alterac_valley,
    "Balinda Stonehearth":          alterac_valley,
    "Magister Dawngrasp":           alterac_valley,

    "Mass Polymorph":               alterac_valley,
    "Haleh, Matron Protectorate":   alterac_valley,
    "Rune of the Archmage":         alterac_valley,
    "Drakefire Amulet":             alterac_valley,

    "Iceblood Tower":               alterac_valley,

# paladin
    "Battle Vicar":                 alterac_valley,
    "Ring of Courage":              alterac_valley,
    "Vitality Surge":               alterac_valley,
    "Hold the Bridge":              alterac_valley,

    "Saidan the Scarlet":           alterac_valley,
    "Stonehearth Vindicator":       alterac_valley,
    "Stormwind Avenger":            alterac_valley,
    "Dun Baldar Bridge":            alterac_valley,

    "Cavalry Horn":                 alterac_valley,
    "Protect the Innocent":         alterac_valley,
    "Brasswing":                    alterac_valley,
    "Lightforged Cariel":           alterac_valley,

    "Templar Captain":              alterac_valley,

# priest
    "Gift of the Naaru":            alterac_valley,
    "Shadow Word: Devour":          alterac_valley,
    "Bless":                        alterac_valley,
    "Luminous Geode":               alterac_valley,

    "Deliverance":                  alterac_valley,
    "Horn of Wrathion":             alterac_valley,
    "Stormpike Aid Station":        alterac_valley,
    "Lightmaw Netherdrake":         alterac_valley,

    "Najak Hexxen":                 alterac_valley,
    "Spirit Guide":                 alterac_valley,
    "Mi'da, Pure Light":            alterac_valley,
    "Undying Disciple":             alterac_valley,

    "Xyrella, the Devout":          alterac_valley,

# rogue
    "Forsaken Lieutenant":          alterac_valley,
    "Reconnaissance":               alterac_valley,
    "The Lobotomizer":              alterac_valley,
    "Tooth of Nefarian":            alterac_valley,

    "Coldtooth Yeti":               alterac_valley,
    "Double Agent":                 alterac_valley,
    "SI:7 Smuggler":                alterac_valley,
    "Snowfall Graveyard":           alterac_valley,

    "Cera'thine Fleetrunner":       alterac_valley,
    "Contraband Stash":             alterac_valley,
    "Wildpaw Gnoll":                alterac_valley,
    "Shadowcrafter Scabbs":         alterac_valley,

    "Smokescreen":                  alterac_valley,

# shamam
    "Windchill":                    alterac_valley,
    "Bracing Cold":                 alterac_valley,
    "Frostbite":                    alterac_valley,
    "Sleetbreaker":                 alterac_valley,

    "Spirit Mount":                 alterac_valley,
    "Cheaty Snobold":               alterac_valley,
    "Snowball Fight!":              alterac_valley,
    "Wildpaw Cavern":               alterac_valley,

    "Don't Stand in the Fire!":     alterac_valley,
    "Glaciate":                     alterac_valley,
    "Snowfall Guardian":            alterac_valley,
    "Bearon Gla'shear":             alterac_valley,

    "Bru'kan of the Elements":      alterac_valley,

# warlock
    "Curse of Agony":               alterac_valley,
    "Grave Defiler":                alterac_valley,
    "Seeds of Destruction":         alterac_valley,
    "Desecrated Graveyard":         alterac_valley,

    "Full-Blown Evil":              alterac_valley,
    "Sacrificial Summoner":         alterac_valley,
    "Tamsin's Phylactery":          alterac_valley,
    "Felfire in the Hole!":         alterac_valley,

    "Hollow Abomination":           alterac_valley,
    "Spawn of Deathwing":           alterac_valley,
    "Dreadlich Tamsin":             alterac_valley,
    "Felwalker":                    alterac_valley,

    "Impfestation":                 alterac_valley,

# warrior
    "Hit It Very Hard":             alterac_valley,
    "Shoulder Check":               alterac_valley,
    "Frozen Buckler":               alterac_valley,
    "Iceblood Garrison":            alterac_valley,

    "To the Front!":                alterac_valley,
    "Glory Chaser":                 alterac_valley,
    "Scrapsmith":                   alterac_valley,
    "Snowed In":                    alterac_valley,

    "Axe Berserker":                alterac_valley,
    "Onyxian Drake":                alterac_valley,
    "Captain Galvangar":            alterac_valley,
    "Rokara, the Valorous":         alterac_valley,

    "Shield Shatter":               alterac_valley,

# neutral
    "Gnome Private":                alterac_valley,
    "Irondeep Trogg":               alterac_valley,
    "Ivus, the Forest Lord":        alterac_valley,
    "Corporal":                     alterac_valley,

    "Gankster":                     alterac_valley,
    "Ram Commander":                alterac_valley,
    "Sneaky Scout":                 alterac_valley,
    "Stormpike Quartermaster":      alterac_valley,

    "Bunker Sergeant":              alterac_valley,
    "Direwolf Commander":           alterac_valley,
    "Grimtotem Bounty Hunter":      alterac_valley,
    "Kobold Taskmaster":            alterac_valley,

    "Piggyback Imp":                alterac_valley,
    "Popsicooler":                  alterac_valley,
    "Reflecto Engineer":            alterac_valley,
    "Snowblind Harpy":              alterac_valley,

    "Whelp Bonker":                 alterac_valley,
    "Drek'Thar":                    alterac_valley,
    "Frostwolf Warmaster":          alterac_valley,
    "Frozen Mammoth":               alterac_valley,

    "Gear Grubber":                 alterac_valley,
    "Herald of Lokholar":           alterac_valley,
    "Ice Revenant":                 alterac_valley,
    "Korrak the Bloodrager":        alterac_valley,

    "Stormpike Marshal":            alterac_valley,
    "Tower Sergeant":               alterac_valley,
    "Vanndar Stormpike":            alterac_valley,
    "Blood Guard":                  alterac_valley,

    "Frantic Hippogryph":           alterac_valley,
    "Knight-Captain":               alterac_valley,
    "Onyxian Warder":               alterac_valley,
    "Spammy Arcanist":              alterac_valley,

    "Icehoof Protector":            alterac_valley,
    "Legionnaire":                  alterac_valley,
    "Humongous Owl":                alterac_valley,
    "Abominable Lieutenant":        alterac_valley,

    "Kazakusan":                    alterac_valley,
    "Troll Centurion":              alterac_valley,
    "Lokholar the Ice Lord":        alterac_valley,
    "Raid Boss Onyxia":             alterac_valley,

# druid
    "Cenarion Hold":                caverns,
 #   "Addled Grizzly":               caverns,
    "Invigorate":                   caverns,
    "Rat Sensei":                   caverns,

#    "Lotus Agents":                 caverns,
#    "Jade Spirit":                  caverns,    
#    "Klaxxi Amber-Weaver":          caverns,
#    "Mire Keeper":                  caverns,

#    "Virmen Sensei":                caverns,
#    "Jade Behemoth":                caverns,
#    "Menagerie Warden":             caverns,
#    "Aya Blackpaw":                 caverns,

#    "Dark Arakkoa":                 caverns,    
#    "Knight of the Wild":           caverns,
#    "Malorne":                      caverns,
#    "Aviana":                       caverns,

# hunter
#    "Lock and Load":                caverns,
#    "Smuggler's Crate":             caverns,
#    "Explorer's Hat":               caverns,
#    "Grimestreet Informant":        caverns,

#    "Snipe":                        caverns,
    "Time-Lost Raptor":             caverns,
#    "Acidmaw":                      caverns,
#    "Ball of Spiders":              caverns,

#    "Dreadscale":                   caverns,
    "Durnholde Imposter":           caverns,
#    "King of Beasts":               caverns,
#    "Shaky Zipgunner":              caverns,

#    "Cobra Shot":                   caverns,
#    "Don Han'Cho":                  caverns,
    "Trial of the Jormungars":      caverns,

# mage
#    "Mana Wyrm":                    caverns,
    "Black Morass Imposter":        caverns,
#    "Kabal Courier":                caverns,
#    "Soot Spewer":                  caverns,

#    "Spellslinger":                 caverns,
#    "Cabalist's Tome":              caverns,
#    "Dalaran Aspirant":             caverns,
#    "Goblin Blastmage":             caverns,

#    "Flame Lance":                  caverns,
#    "Servant of Yogg-Saron":        caverns,
    "Chromie, Timehopper":          caverns,
    "Disco at the End of Time":     caverns,

#    "Kabal Crystal Runner":         caverns,
#    "Flame Leviathan":              caverns,

# paladin
#    "A Light in the Darkness":      caverns,
#    "Grimestreet Informant":        caverns,
    "Enter the Coliseum":           caverns,
#    "Keeper of Uldaman":            caverns,

    "Runi, Time Explorer":          caverns,
#    "Silvermoon Portal":            caverns,
    "Steward of Darkshire":         caverns,
    "Wickerflame Burnbristle":      caverns,

    "Bronze Dragonknight":          caverns,
#    "Grimestreet Enforcer":         caverns,
#    "Ivory Knight":                 caverns,
    "Timeless Blessing":            caverns,

#    "Don Han'Cho":                  caverns,
#    "Mysterious Challenger":        caverns,
    "Lay on Hands":                 caverns,

# priest
#    "Shadowbomber":                 caverns,
    "Ship's Chirurgeon":            caverns,
#    "Kabal Courier":                caverns,
#    "Museum Curator":               caverns,

    "Shadow Word: Forbid":          caverns,
#    "Shadowfiend":                  caverns,
#    "Shrinkmeister":                caverns,
#    "Convert":                      caverns,

#    "Hooded Acolyte":               caverns,
#    "Onyx Bishop":                  caverns,
#    "Darkshire Alchemist":          caverns,
#    "Spawn of Shadows":             caverns,

    "Confessor Paletress":          caverns,
    "Murozond, Thief of Time":      caverns,

# rogue
#    "Jade Swarmer":                 caverns,
#    "Jade Shuriken":                caverns,
#    "Poisoned Blade":               caverns,
#    "Undercity Huckster":           caverns,

#    "Burgle":                       caverns,
#    "Lotus Agents":                 caverns,
#    "Dark Iron Skulker":            caverns,
#    "Jade Spirit":                  caverns,

    "Jade Telegram":                caverns,
    "Mount Hyjal Imposter":         caverns,
    "The Scarab Lord":              caverns,
#    "Tomb Pillager":                caverns,

#    "Shado-Pan Rider":              caverns,
#    "Aya Blackpaw":                 caverns,
#    "Blade of C'Thun":              caverns,
    "Anub'arak":                    caverns,

# shamam
#    "Reincarnate":                  caverns,
    "Pebbly Page":                  caverns,
#    "Tuskarr Totemic":              caverns,
#    "Call in the Finishers":        caverns,

#    "Charged Hammer":               caverns,
#    "Healing Wave":                 caverns,
#    "Jade Lightning":               caverns,
#    "Lotus Agents":                 caverns,

#    "Wicked Witchdoctor":           caverns,
#    "Jade Spirit":                  caverns,
    "Totally Totems":               caverns,
#    "Hallazeal the Ascended":       caverns,

#    "Thunder Bluff Valiant":        caverns,
#    "Aya Blackpaw":                 caverns,
#    "Jade Chieftain":               caverns,
    "Al'Akir, the Winds of Time":   caverns,

# warlock
#    "Dark Peddler":                 caverns,
#    "Darkbomb":                     caverns,
    "Demonfuse":                    caverns,
#    "Kabal Courier":                caverns,

#    "Tiny Knight of Evil":          caverns,
    "Chamber of Viscidus":          caverns,
#    "Silverware Golem":             caverns,
#    "Spreading Madness":            caverns,

#    "Dark Bargain":                 caverns,
#    "Usher of Souls":               caverns,
    "Witch of the Arch-Thief":      caverns,
#    "Bane of Doom":                 caverns,

#    "Cho'gall":                     caverns,
#    "Krul the Unshackled":          caverns,

# warrior
#    "I Know a Guy":                 caverns,
#    "Bouncing Blade":               caverns,
#    "Grimestreet Informant":        caverns,
#    "Stolen Goods":                 caverns,

#    "Brass Knuckles":               caverns,
#    "Grimy Gadgeteer":              caverns,
#    "Axe Flinger":                  caverns,
    "Blast from the Past":          caverns,

#    "Hobart Grapplehammer":         caverns,
#    "Ironforge Portal":             caverns,
    "Ivory Rook":                   caverns,
#    "Alley Armorsmith":             caverns,

#    "Don Han'Cho":                  caverns,
#    "Ancient Shieldbearer":         caverns,
#    "Iron Juggernaut":              caverns,

# neutral
#    "Small-Time Buccaneer":         caverns,
    "Shark Puncher":                caverns,
#    "Twilight Geomancer":           caverns,
#    "Acolyte of Pain":              caverns,

#    "Disciple of C'Thun":           caverns,
    "Menagerie Mug":                caverns,
#    "Street Trickster":             caverns,
    "Timeline Accelerator":         caverns,

#    "Crazed Worshipper":            caverns,
#    "C'Thun's Chosen":              caverns,
#    "Cult Apothecary":              caverns,
    "Eyestalk of C'Thun":           caverns,

    "Future Emissary":              caverns,
    "Infinite Amalgam":             caverns,
#    "Worgen Greaser":               caverns,
#    "Blackwing Corruptor":          caverns,

#    "Emperor Thaurissan":           caverns,
    "Menagerie Jug":                caverns,
#    "Mukla's Champion":             caverns,
    "Sludge Belcher":               caverns,

    "Soridormi":                    caverns,
    "Valstann Staghelm":            caverns,
#    "Twin Emperor Vek'lor":         caverns,
#    "C'Thun":                       caverns,

# death knight
    "Body Bagger":                  core,
    "Heart Strike":                 core,
    "Noxious Cadaver":              core,
    "Plagued Grain":                core,

    "Runeforging":                  core,
    "Skeletal Sidekick":            core,
    "Battlefield Necromancer":      core,
    "Bonedigger Geist":             core,

    "Defrost":                      core,
    "Frost Strike":                 core,
    "Harbinger of Winter":          core,
    "Obliterate":                   core,

    "Anti-Magic Shell":             core,
    "Chillfallen Baron":            core,
    "Graveyard Shift":              core,
    "Rimefang Sword":               core,

    "Ymirjar Deathbringer":         core,
    "Death Strike":                 core,
    "Grave Strength":               core,
    "Remorseless Winter":           core,

    "Repulsive Gargantuan":         core,
    "Thassarian":                   core,
    "Army of the Dead":             core,
    "Deathbringer Saurfang":        core,

    "Possessifier":                 core,
    "Rime Sculptor":                core,
    "Corrupted Ashbringer":         core,
    "Gnome Muncher":                core,

    "Overseer Frigidara":           core,
    "Patchwerk":                    core,
#    "Lord Marrowgar":               core,
    "Stitched Giant":               core,

# demon hunter
#    "Coordinated Strike":           core,
#    "Eye Beam":                     core,
    "Gan'arg Glaivesmith":          core,
#    "Wrathscale Naga":              core,

#    "Kayn Sunfury":                 core,
#    "Metamorphosis":                core,
#    "Raging Felscreamer":           core,
#    "Flamereaper":                  core,

    "Illidari Inquisitor":          core,

# druid
#    "Innervate":                    core,
#    "Pounce":                       core,
#    "Living Roots":                 core,
#    "Witchwood Apple":              core,

    "Lunar Eclipse":                core,
#    "Mark of the Wild":             core,
#    "Power of the Wild":            core,
#    "Solar Eclipse":                core,

#    "Wild Growth":                  core,
#    "Wrath":                        core,
#    "Feral Rage":                   core,
#    "Kiri, Chosen of Elune":        core,

#    "Soul of the Forest":           core,
#    "Nourish":                      core,
#    "Druid of the Claw":            core,
#    "Ancient of Lore":              core,

#    "Cenarius":                     core,

# hunter
#    "Arcane Shot":                  core,
#    "Candleshot":                   core,
#    "Jeweled Macaw":                core,
#    "Tracking":                     core,

#    "Cat Trick":                    core,
#    "Doggie Biscuit":               core,
#    "Explosive Trap":               core,
#    "Freezing Trap":                core,

#    "Quick Shot":                   core,
    "Selective Breeder":            core,
#    "Wandering Monster":            core,
#    "Animal Companion":             core,

#    "Deadly Shot":                  core,
#    "Dragonbane":                   core,
#    "Marked Shot":                  core,
#    "Savannah Highmane":            core,

#    "King Krush":                   core,

# mage
#    "Arcane Artificer":             core,
#    "Babbling Book":                core,
#    "Flame Geyser":                 core,
#    "Shooting Star":                core,

#    "Snap Freeze":                  core,
#    "Arcanologist":                 core,
#    "Arcane Intellect":             core,
#    "Counterspell":                 core,

#    "Explosive Runes":              core,
#    "Ice Barrier":                  core,
#    "Stargazer Luna":               core,
#    "Fire Sale":                    core,

#    "Fireball":                     core,
    "Aegwynn, the Guardian":        core,
#    "Blizzard":                     core,
#    "Firelands Portal":             core,

#    "Pyroblast":                    core,

# paladin
#    "Righteous Protector":          core,
    "Argent Protector":             core,
#    "Equality":                     core,
#    "Flash of Light":               core,

#    "Grimestreet Outfitter":        core,
#    "Hand of A'dal":                core,
    "Resistance Aura":              core,
#    "Bronze Explorer":              core,

#    "Consecration":                 core,
#    "Hammer of Wrath":              core,
#    "Muster for Battle":            core,
#    "Warhorse Trainer":             core,

#    "Blessing of Kings":            core,
    "Crusader Aura":                core,
#    "Stand Against Darkness":       core,
#    "Truesilver Champion":          core,

#    "Amber Watcher":                core,
#    "Lothraxion the Redeemed":      core,
#    "Tirion Fordring":              core,

# priest
#    "Shadow Word: Death":           core,
    "Thrive in the Shadows":        core,
#    "Holy Nova":                    core,
    "Shadowed Spirit":              core,

#    "Drakonid Operative":           core,
#    "Shadow Word: Ruin":            core,
#    "Darkbishop Benedictus":        core,
#    "Lightbomb":                    core,

#    "Catrina Muerte":               core,

# rogue
#    "Backstab":                     core,
#    "Preparation":                  core,
#    "Shadowstep":                   core,
#    "Buccaneer":                    core,

#    "Deadly Poison":                core,
#    "Swashburglar":                 core,
#    "Ambush":                       core,
#    "Cheat Death":                  core,

#    "Eviscerate":                   core,
#    "Fan of Knives":                core,
    "Plagiarize":                   core,
#    "Shadowjeweler Hanar":          core,

#    "SI:7 Agent":                   core,
#    "Assassinate":                  core,
#    "Elven Minstrel":               core,
#    "Hench-Clan Burglar":           core,

#    "Tess Greymane":                core,

# shamam
#    "Zap!":                         core,
#    "Lightning Bolt":               core,
    "Novice Zapper":                core,
#    "Overdraft":                    core,

#    "Ancestral Knowledge":          core,
#    "Flametongue Totem":            core,
#    "Menacing Nimbus":              core,
#    "Far Sight":                    core,

#    "Feral Spirit":                 core,
#    "Grand Totem Eys'or":           core,
#    "Hex":                          core,
    "Lightning Storm":              core,

#    "Bloodlust":                    core,
#    "Doomhammer":                   core,
#    "Fire Elemental":               core,
#    "Thing from Below":             core,

#    "Al'Akir the Windlord":         core,

# warlock
#    "Flame Imp":                    core,
#    "Mortal Coil":                  core,
#    "Spirit Bomb":                  core,
#    "Voidwalker":                   core,

#    "Defile":                       core,
#    "Drain Soul":                   core,
#    "Fiendish Circle":              core,
#    "Hellfire":                     core,

#    "Imp Gang Boss":                core,
#    "Lakkari Felhound":             core,
#    "Siphon Soul":                  core,
#    "Voidcaller":                   core,

#    "Abyssal Enforcer":             core,
#    "Enhanced Dreadlord":           core,
#    "Lord Jaraxxus":                core,
#    "Twisting Nether":              core,

#    "Mal'Ganis":                    core,

# warrior
#    "Execute":                      core,
#    "Shield Slam":                  core,
#    "Slam":                         core,
#    "Whirlwind":                    core,

#    "Bash":                         core,
#    "Bladestorm":                   core,
#    "Cruel Taskmaster":             core,
#    "Frightened Flunky":            core,

#    "Shield Block":                 core,
#    "Woodcutter's Axe":             core,
#    "Frothing Berserker":           core,
#    "Heavy Plate":                  core,

#    "Sword Eater":                  core,
#    "Brawl":                        core,
#    "Dyn-o-matic":                  core,
#    "Armagedillo":                  core,

#    "Grommash Hellscream":          core,

# neutral
#    "Snowflipper Penguin":          core,
#    "Abusive Sergeant":             core,
#    "Armor Vendor":                 core,
#    "Beaming Sidekick":             core,

#    "Elven Archer":                 core,
    "Emerald Skytalon":             core,
#    "Glacial Shard":                core,
#    "Murloc Tidecaller":            core,

#    "Murmy":                        core,
#    "Tour Guide":                   core,
#    "Voodoo Doctor":                core,
#    "Worgen Infiltrator":           core,

#    "Annoy-o-Tron":                 core,
#    "Bloodmage Thalnos":            core,
#    "Bloodsail Raider":             core,
#    "Crazed Alchemist":             core,

#    "Cult Neophyte":                core,
#    "Dire Wolf Alpha":              core,
#    "Dirty Rat":                    core,
#    "Doomsayer":                    core,

#    "Explosive Sheep":              core,
#    "Faerie Dragon":                core,
    "Fogsail Freebooter":           core,
#    "Injured Tol'vir":              core,

#    "Kobold Geomancer":             core,
    "Loot Hoarder":                 core,
#    "Mad Bomber":                   core,
#    "Murloc Tidehunter":            core,

#    "Nerubian Egg":                 core,
#    "Plated Beetle":                core,
    "Redgill Razorjaw":             core,
#    "Wild Pyromancer":              core,

#    "Youthful Brewmaster":          core,
#    "Acolyte of Pain":              core,
#    "Bronze Gatekeeper":            core,
#    "Coldlight Seer":               core,

#    "Gorillabot A-3":               core,
#    "Hench-Clan Thug":              core,
#    "Humongous Razorleaf":          core,
#    "Murloc Warleader":             core,

#    "Raid Leader":                  core,
#    "Rustrot Viper":                core,
#    "Southsea Captain":             core,
#    "Tar Creeper":                  core,

#    "Vulpera Scoundrel":            core,
    "Zola the Gorgon":              core,
#    "Big Game Hunter":              core,
#    "Chillwind Yeti":               core,

#    "Defender of Argus":            core,
#    "Dread Corsair":                core,
#    "Eater of Secrets":             core,
#    "Footman":                      core,

#    "Grim Necromancer":             core,
#    "Lifedrinker":                  core,
#    "Replicating Menace":           core,
#    "Royal Librarian":              core,

#    "Sen'jin Shieldmasta":          core,
#    "The Black Knight":             core,
#    "Twilight Drake":               core,
#    "Azure Drake":                  core,

#    "Night Elf Huntress":           core,
    "Overlord Runthak":             core,
#    "Rotten Applebaum":             core,
#    "Stranglethorn Tiger":          core,

    "Taelan Fordring":              core,
#    "Wargear":                      core,
#    "Warsong Grunt":                core,
#    "Zilliax":                      core,

#    "Cairne Bloodhoof":             core,
#    "Gnomelia, S.A.F.E. Pilot":     core,
#    "Baron Geddon":                 core,
#    "Dr. Boom":                     core,

    "Nozdormu the Eternal":         core,
#    "Stormwind Champion":           core,
#    "Primordial Drake":             core,
#    "Ragnaros the Firelord":        core,

    "Alexstrasza the Life-Binder":  core,
    "Malygos the Spellweaver":      core,
    "Onyxia the Broodmother":       core,
#    "Sleepy Dragon":                core,

    "Ysera the Dreamer":            core,
    "Deathwing the Destroyer":      core,
#    "Sea Giant":                    core,

# demon hunter
    "Fossil Fanatic":               sunken_city,
    "Multi-Strike":                 sunken_city,
    "Predation ":                   sunken_city,
    "Wayward Sage":                 sunken_city,

    "Abyssal Depths":               sunken_city,
    "Herald of Chaos":              sunken_city,
    "Lady S'theno":                 sunken_city,
    "Azsharan Defector":            sunken_city,

    "Glaiveshark":                  sunken_city,
    "Topple the Idol":              sunken_city,
    "Bone Glaive":                  sunken_city,
    "Coilskar Commander":           sunken_city,

    "Xhilag of the Abyss":          sunken_city,

# druid
    "Aquatic Form":                 sunken_city,
    "Azsharan Gardens":             sunken_city,
    "Bottomfeeder":                 sunken_city,
    "Dozing Kelpkeeper":            sunken_city,

    "Moonbeam":                     sunken_city,
    "Spirit of the Tides":          sunken_city,
    "Herald of Nature":             sunken_city,
    "Seaweed Strike":               sunken_city,

    "Flipper Friends":              sunken_city,
    "Green-Thumb Gardener":         sunken_city,
    "Colaque":                      sunken_city,
    "Hedra the Heretic":            sunken_city,

    "Miracle Growth":               sunken_city,

# hunter
    "Barbed Nets":                  sunken_city,
    "Urchin Spines":                sunken_city,
    "Emergency Maneuvers":          sunken_city,
    "K9-0tron":                     sunken_city,

    "Raj Naz'jan":                  sunken_city,
    "Ancient Krakenbane":           sunken_city,
    "Conch's Call":                 sunken_city,
    "Harpoon Gun":                  sunken_city,

    "Naga's Pride":                 sunken_city,
    "Shellshot":                    sunken_city,
    "Azsharan Saber":               sunken_city,
    "Twinbow Terrorcoil":           sunken_city,

    "Hydralodon":                   sunken_city,

# mage
    "Trench Surveyor":              sunken_city,
    "Gifts of Azshara":             sunken_city,
    "Spellcoiler":                  sunken_city,
    "Submerged Spacerock":          sunken_city,

    "Volcanomancy":                 sunken_city,
    "Azsharan Sweeper":             sunken_city,
    "Mecha-Shark":                  sunken_city,
    "Polymorph: Jellyfish":         sunken_city,

    "Seafloor Gateway":             sunken_city,
    "Commander Sivara":             sunken_city,
    "Spitelash Siren":              sunken_city,
    "Lady Naz'jar":                 sunken_city,

    "Gaia, the Techtonic":          sunken_city,

# paladin
    "Holy Maki Roll":               sunken_city,
    "Kotori Lightblade":            sunken_city,
    "Radar Detector":               sunken_city,
    "Seafloor Savior":              sunken_city,

    "Azsharan Mooncatcher":         sunken_city,
    "Myrmidon":                     sunken_city,
    "Shimmering Sunfish":           sunken_city,
    "Bubblebot":                    sunken_city,

    "Immortalized in Stone":        sunken_city,
    "The Leviathan":                sunken_city,
    "Front Lines":                  sunken_city,
    "Lightray":                     sunken_city,

    "The Garden's Grace":           sunken_city,

# priest
    "Illuminate":                   sunken_city,
    "Priestess Valishj":            sunken_city,
    "Serpent Wig":                  sunken_city,
    "Whispers of the Deep":         sunken_city,

    "Queensguard":                  sunken_city,
    "Handmaiden":                   sunken_city,
    "Herald of Light":              sunken_city,
    "Azsharan Ritual":              sunken_city,

    "Disarming Elemental":          sunken_city,
    "Drown":                        sunken_city,
    "Switcheroo":                   sunken_city,
    "Blackwater Behemoth":          sunken_city,

    "Whirlpool":                    sunken_city,

# rogue
    "Filletfighter":                sunken_city,
    "Gone Fishin'":                 sunken_city,
    "Shattershambler":              sunken_city,
    "Inkveil Ambusher":             sunken_city,

    "Jackpot!":                     sunken_city,
    "Cutlass Courier":              sunken_city,
    "Swordfish":                    sunken_city,
    "Swiftscale Trickster":         sunken_city,

    "Azsharan Vessel":              sunken_city,
    "Bootstrap Sunkeneer":          sunken_city,
    "Blood in the Water":           sunken_city,
    "Crabatoa":                     sunken_city,

    "Pirate Admiral Hooktusk":      sunken_city,

# shamam
    "Azsharan Scroll":              sunken_city,
    "Scalding Geyser":              sunken_city,
    "Schooling":                    sunken_city,
    "Anchored Totem":               sunken_city,

    "Clownfish":                    sunken_city,
    "Piranha Poacher":              sunken_city,
    "Radiance of Azshara":          sunken_city,
    "Bioluminescence":              sunken_city,

    "Tidelost Burrower":            sunken_city,
    "Command of Neptulon":          sunken_city,
    "Coral Keeper":                 sunken_city,
    "Glugg the Gulper":             sunken_city,

    "Wrathspine Enchanter":         sunken_city,

# warlock
    "Rock Bottom":                  sunken_city,
    "Azsharan Scavenger":           sunken_city,
    "Chum Bucket":                  sunken_city,
    "Voidgill":                     sunken_city,

    "Bloodscent Vilefin":           sunken_city,
    "Dragged Below":                sunken_city,
    "Herald of Shadows":            sunken_city,
    "Sira'kess Cultist":            sunken_city,

    "Immolate":                     sunken_city,
    "Commander Ulthok":             sunken_city,
    "Za'qul":                       sunken_city,
    "Abyssal Wave":                 sunken_city,

    "Gigafin":                      sunken_city,

# warrior
    "Forged in Flame":              sunken_city,
    "Guard the City":               sunken_city,
    "Obsidiansmith":                sunken_city,
    "The Fires of Zin-Azshari":     sunken_city,

    "Azsharan Trident":             sunken_city,
    "Clash of the Colossals":       sunken_city,
    "From the Depths":              sunken_city,
    "Igneous Lavagorger":           sunken_city,

    "Lady Ashvane":                 sunken_city,
    "Blackscale Brute":             sunken_city,
    "Nellie, the Great Thresher":   sunken_city,
    "Tidal Revenant":               sunken_city,

    "Trenchstalker":                sunken_city,

# neutral
    "Bubbler":                      sunken_city,
    "Click-Clocker":                sunken_city,
    "Helmet Hermit":                sunken_city,
    "Pelican Diver":                sunken_city,

    "Piranha Swarmer":              sunken_city,
    "Sir Finley, Sea Guide":        sunken_city,
    "Vicious Slitherspear":         sunken_city,
    "Amalgam of the Deep":          sunken_city,

    "Murkwater Scribe":             sunken_city,
    "Naval Mine":                   sunken_city,
    "Rainbow Glowscale":            sunken_city,
    "Security Automaton":           sunken_city,

    "Tuskarrrr Trawler":            sunken_city,
    "Crushclaw Enforcer":           sunken_city,
    "Pufferfist":                   sunken_city,
    "Reefwalker":                   sunken_city,

    "Seascout Operator":            sunken_city,
    "Slimescale Diver":             sunken_city,
    "Smothering Starfish":          sunken_city,
    "Snapdragon":                   sunken_city,

    "Treasure Guard":               sunken_city,
    "Twin-fin Fin Twin":            sunken_city,
    "Ambassador Faelin":            sunken_city,
    "Baba Naga":                    sunken_city,

    "Blademaster Okani":            sunken_city,
    "Coilfang Constrictor":         sunken_city,
    "Excavation Specialist":        sunken_city,
    "School Teacher":               sunken_city,

    "Selfish Shellfish":            sunken_city,
    "Azsharan Sentinel":            sunken_city,
    "Gangplank Diver":              sunken_city,
    "Gorloc Ravager":               sunken_city,

    "Ini Stormcoil":                sunken_city,
    "Queen Azshara":                sunken_city,
    "Barbaric Sorceress":           sunken_city,
    "Mothership":                   sunken_city,

    "Slithering Deathscale":        sunken_city,
    "Ozumat":                       sunken_city,
    "Neptulon the Tidehunter":      sunken_city,
    "Naga Giant":                   sunken_city,

# demon hunter
    "Dispose of Evidence":          castle_nathria,
    "Relic of Extinction":          castle_nathria,
    "Bibliomite":                   castle_nathria,
    "Relic Vault":                  castle_nathria,

    "Sinful Brand":                 castle_nathria,
    "Magnifying Glaive":            castle_nathria,
    "Relic of Phantasms":           castle_nathria,
    "Kryxis the Voracious":         castle_nathria,

    "Prosecutor Mel'tranix":        castle_nathria,
    "Sightless Magistrate":         castle_nathria,
    "All Fel Breaks Loose":         castle_nathria,
    "Relic of Dimensions":          castle_nathria,

    "Artificer Xy'mox":             castle_nathria,

# druid
    "Planted Evidence":             castle_nathria,
    "Attorney-at-Maw":              castle_nathria,
    "Natural Causes":               castle_nathria,
    "Hedge Maze":                   castle_nathria,

    "Incarceration":                castle_nathria,
    "Plot of Sin":                  castle_nathria,
    "Dew Process":                  castle_nathria,
    "Widowbloom Seedsman":          castle_nathria,

    "Death Blossom Whomper":        castle_nathria,
    "Topior the Shrubbagazzor":     castle_nathria,
    "Nightshade Bud":               castle_nathria,
    "Sesselie of the Fae Court":    castle_nathria,

    "Convoke the Spirits":          castle_nathria,

# hunter
    "Batty Guest":                  castle_nathria,
    "Castle Kennels":               castle_nathria,
    "Frenzied Fangs":               castle_nathria,
    "Motion Denied":                castle_nathria,

    "Spirit Poacher":               castle_nathria,
    "Stag Charge":                  castle_nathria,
    "Wild Spirits":                 castle_nathria,
    "Stonebound Gargon":            castle_nathria,

    "Ara'lon":                      castle_nathria,
    "Shadehound":                   castle_nathria,
    "Defense Attorney Nathanos":    castle_nathria,
    "Huntsman Altimor":             castle_nathria,

    "Collateral Damage":            castle_nathria,

# mage
    "Suspicious Alchemist":         castle_nathria,
    "Frozen Touch":                 castle_nathria,
    "Nightcloak Sanctum":           castle_nathria,
    "Objection!":                   castle_nathria,

    "Solid Alibi":                  castle_nathria,
    "Vengeful Visage":              castle_nathria,
    "Chatty Bartender":             castle_nathria,
    "Cold Case":                    castle_nathria,

    "Life Sentence":                castle_nathria,
    "Orion, Mansion Manager":       castle_nathria,
    "Contract Conjurer":            castle_nathria,
    "Deathborne":                   castle_nathria,

    "Kel'Thuzad, the Inevitable":   castle_nathria,

# paladin
    "Promotion":                    castle_nathria,
    "Sinful Sous Chef":             castle_nathria,
    "Class Action Lawyer":          castle_nathria,
    "Great Hall":                   castle_nathria,

    "Order in the Court":           castle_nathria,
    "Jury Duty":                    castle_nathria,
    "Muckborn Servant":             castle_nathria,
    "Service Bell":                 castle_nathria,

    "Stewart the Steward":          castle_nathria,
    "Buffet Biggun":                castle_nathria,
    "Elitist Snob":                 castle_nathria,
    "Divine Toll":                  castle_nathria,

    "The Countess":                 castle_nathria,

# priest
    "Suspicious Usher":             castle_nathria,
    "The Light! It Burns!":         castle_nathria,
    "Theft Accusation":             castle_nathria,
    "Mysterious Visitor":           castle_nathria,

    "Cathedral of Atonement ":      castle_nathria,
    "Clear Conscience":             castle_nathria,
    "Identity Theft":               castle_nathria,
    "Pelagos":                      castle_nathria,

    "The Harvester of Envy":        castle_nathria,
    "Boon of the Ascended":         castle_nathria,
    "Incriminating Psychic":        castle_nathria,
    "Partner in Crime":             castle_nathria,

    "Clean the Scene":              castle_nathria,

# rogue
    "Door of Shadows":              castle_nathria,
    "Double Cross":                 castle_nathria,
    "Kidnap":                       castle_nathria,
    "Murder Accusation":            castle_nathria,

    "Perjury":                      castle_nathria,
    "Serrated Bone Spike":          castle_nathria,
    "Sticky Situation":             castle_nathria,
    "Ghastly Gravedigger":          castle_nathria,

    "Sinstone Graveyard":           castle_nathria,
    "Halkias":                      castle_nathria,
    "Private Eye":                  castle_nathria,
    "Necrolord Draka":              castle_nathria,

    "Scribbling Stenographer":      castle_nathria,

# shamam
    "Convincing Disguise":          castle_nathria,
    "Muck Pools":                   castle_nathria,
    "Totemic Evidence":             castle_nathria,
    "Carving Chisel":               castle_nathria,

    "Framester":                    castle_nathria,
    "Party Favor Totem":            castle_nathria,
    "Primordial Wave":              castle_nathria,
    "Baroness Vashj":               castle_nathria,

    "Crud Caretaker":               castle_nathria,
    "The Stonewright":              castle_nathria,
    "Criminal Lineup":              castle_nathria,
    "Torghast Custodian":           castle_nathria,

    "Gigantotem":                   castle_nathria,

# warlock
    "Flustered Librarian":          castle_nathria,
    "Arson Accusation":             castle_nathria,
    "Impending Catastrophe":        castle_nathria,
    "Imp-oster":                    castle_nathria,

    "Vile Library":                 castle_nathria,
    "Habeas Corpses":               castle_nathria,
    "Shadowborn":                   castle_nathria,
    "Suffocating Shadows":          castle_nathria,

    "Mischievous Imp":              castle_nathria,
    "Lady Darkvein":                castle_nathria,
    "Shadow Waltz":                 castle_nathria,
    "Imp King Rafaam":              castle_nathria,

    "Tome Tampering":               castle_nathria,

# warrior
    "Call to the Stand":            castle_nathria,
    "Sanguine Depths ":             castle_nathria,
    "Anima Extractor":              castle_nathria,
    "Conqueror's Banner":           castle_nathria,

    "Crazed Wretch":                castle_nathria,
    "Riot!":                        castle_nathria,
    "Imbued Axe":                   castle_nathria,
    "Suspicious Pirate":            castle_nathria,

    "Weapons Expert":               castle_nathria,
    "Burden of Pride":              castle_nathria,
    "Mawsworn Bailiff":             castle_nathria,
    "Decimator Olgra":              castle_nathria,

    "Remornia, Living Blade":       castle_nathria,

# neutral
    "Dredger Staff":                castle_nathria,
    "Sinstone Totem":               castle_nathria,
    "Anonymous Informant":          castle_nathria,
    "Crooked Cook":                 castle_nathria,

    "Maze Guide":                   castle_nathria,
    "Priest of the Deceased":       castle_nathria,
    "Roosting Gargoyle":            castle_nathria,
    "Sketchy Stranger":             castle_nathria,

    "Volatile Skeleton":            castle_nathria,
    "Afterlife Attendant":          castle_nathria,
    "Ashen Elemental":              castle_nathria,
    "Creepy Painting":              castle_nathria,

    "Murloc Holmes":                castle_nathria,
    "Prince Renathal":              castle_nathria,
    "Tight-Lipped Witness":         castle_nathria,
    "Dispossessed Soul":            castle_nathria,

    "Murlocula":                    castle_nathria,
    "Scuttlebutt Ghoul":            castle_nathria,
    "Famished Fool":                castle_nathria,
    "Muck Plumber":                 castle_nathria,

    "Sinrunner":                    castle_nathria,
    "Soul Seeker":                  castle_nathria,
    "Steamcleaner":                 castle_nathria,
    "Stoneborn Accuser":            castle_nathria,

    "Bog Beast":                    castle_nathria,
    "Masked Reveler":               castle_nathria,
    "Sylvanas, the Accused":        castle_nathria,
    "Theotar, the Mad Duke":        castle_nathria,

    "Red Herring":                  castle_nathria,
    "Sinfueled Golem":              castle_nathria,
    "Kael'thas Sinstrider":         castle_nathria,
    "Party Crasher":                castle_nathria,

    "Insatiable Devourer":          castle_nathria,
    "Sire Denathrius":              castle_nathria,
    "Stoneborn General":            castle_nathria,
    "The Jailer":                   castle_nathria,

# death knight
    "Horn of Winter":               path_of_arthas,
    "Bone Breaker":                 path_of_arthas,
    "Icy Touch":                    path_of_arthas,
    "Ymirjar Frostbreaker":         path_of_arthas,

    "Blood Tap":                    path_of_arthas,
    "Dark Transformation":          path_of_arthas,
    "Deathchiller":                 path_of_arthas,
    "Hematurge":                    path_of_arthas,

    "Plague Strike":                path_of_arthas,
    "Unholy Frenzy":                path_of_arthas,
    "Vicious Bloodworm":            path_of_arthas,
    "Asphyxiate":                   path_of_arthas,

    "Darkfallen Neophyte":          path_of_arthas,
    "Glacial Advance":              path_of_arthas,
    "Howling Blast":                path_of_arthas,
    "Lady Deathwhisper":            path_of_arthas,

    "Malignant Horror":             path_of_arthas,
    "Might of Menethil":            path_of_arthas,
    "Nerubian Swarmguard":          path_of_arthas,
    "Tomb Guardians":               path_of_arthas,

    "Blood Boil":                   path_of_arthas,
    "Corpse Bride":                 path_of_arthas,
    "Frostmourne":                  path_of_arthas,
    "Marrow Manipulator":           path_of_arthas,

    "Frostwyrm's Fury":             path_of_arthas,
    "The Scourge":                  path_of_arthas,

# death knight
    "Necrotic Mortician":           lich_king,
    "Vampiric Blood":               lich_king,
    "Acolyte of Death":             lich_king,
    "Blightfang":                   lich_king,

    "Construct Quarter":            lich_king,
    "Meat Grinder":                 lich_king,
    "Rimescale Siren":              lich_king,
    "Soulbreaker":                  lich_king,

    "Corpse Explosion":             lich_king,
    "Alexandros Mograine":          lich_king,
    "Frost Queen Sindragosa":       lich_king,
    "Boneguard Commander":          lich_king,

    "Soulstealer":                  lich_king,

# demon hunter
    "Calamity's Grasp":             lich_king,
    "Fierce Outsider":              lich_king,
    "Shambling Chow":               lich_king,
    "Unleash Fel":                  lich_king,

    "Mark of Scorn":                lich_king,
    "Wretched Exile":               lich_king,
    "Fel'dorei Warband":            lich_king,
    "Felerin, the Forgotten":       lich_king,

    "Souleater's Scythe":           lich_king,
    "Deal with a Devil":            lich_king,
    "Felscale Evoker":              lich_king,
    "Vengeful Walloper":            lich_king,

    "Brutal Annihilan":             lich_king,

# druid
    "Lingering Zombie":             lich_king,
    "Wither":                       lich_king,
    "Chitinous Plating":            lich_king,
    "Nerubian Flyer":               lich_king,

    "Rake":                         lich_king,
    "Beetlemancy":                  lich_king,
    "Elder Nadox":                  lich_king,
    "Death Beetle":                 lich_king,

    "Life from Death":              lich_king,
    "Unending Swarm":               lich_king,
    "Underking":                    lich_king,
    "Anub'Rekhan":                  lich_king,

    "Crypt Keeper":                 lich_king,

# hunter
    "Ricochet Shot":                lich_king,
    "Scourge Tamer":                lich_king,
    "Trinket Tracker":              lich_king,
    "Arcane Quiver":                lich_king,

    "Conjured Arrow":               lich_king,
    "Silvermoon Farstrider":        lich_king,
    "ZOMBEEEES!!!":                 lich_king,
    "Halduron Brightwing":          lich_king,

    "Keeneye Spotter":              lich_king,
    "Eversong Portal":              lich_king,
    "Shockspitter":                 lich_king,
    "Hope of Quel'Thalas":          lich_king,

    "Faithful Companions":          lich_king,

# mage
    "Arcane Bolt":                  lich_king,
    "Arcane Wyrm":                  lich_king,
    "Magister's Apprentice":        lich_king,
    "Prismatic Elemental":          lich_king,

    "Vast Wisdom":                  lich_king,
    "Arcsplitter":                  lich_king,
    "Energy Shaper":                lich_king,
    "Spectral Trainee":             lich_king,

    "Whirlweaver":                  lich_king,
    "Tear Reality":                 lich_king,
    "Vexallus":                     lich_king,
    "Arcane Defenders":             lich_king,

    "Grand Magister Rommath":       lich_king,

# paladin
    "Feast and Famine":             lich_king,
    "Flight of the Bronze":         lich_king,
    "Sanguine Soldier":             lich_king,
    "Blood Matriarch Liadrin":      lich_king,

    "For Quel'Thalas!":             lich_king,
    "Knight of the Dead":           lich_king,
    "Seal of Blood":                lich_king,
    "Timewarden":                   lich_king,

    "Daring Drake":                 lich_king,
    "Goldwing":                     lich_king,
    "The Purator":                  lich_king,
    "Blood Crusader":               lich_king,

    "Anachronos":                   lich_king,

# priest
    "Undying Allies":               lich_king,
    "Animate Dead":                 lich_king,
    "Crystalsmith Cultist":         lich_king,
    "Mind Sear":                    lich_king,

    "Mind Eater":                   lich_king,
    "Haunting Nightmare":           lich_king,
    "Bonecaller":                   lich_king,
    "Cannibalize":                  lich_king,

    "Grave Digging":                lich_king,
    "High Cultist Basaleph":        lich_king,
    "Rotting Necromancer":          lich_king,
    "Shadow Word: Undeath":         lich_king,

    "Sister Svalna":                lich_king,

# rogue
    "Ransack":                      lich_king,
    "Shadow of Demise":             lich_king,
    "Concoctor":                    lich_king,
    "Ghostly Strike":               lich_king,

    "Jolly Roger":                  lich_king,
    "Ghoulish Alchemist":           lich_king,
    "Potion Belt":                  lich_king,
    "Potionmaster Putricide":       lich_king,

    "Rotten Rodent":                lich_king,
    "Stitched Creation":            lich_king,
    "Vile Apothecary":              lich_king,
    "Noxious Infiltrator":          lich_king,

    "Scourge Illusionist":          lich_king,

# shamam
    "Blazing Transmutation":        lich_king,
    "Cold Storage":                 lich_king,
    "Scourge Troll":                lich_king,
    "Deathweaver Aura":             lich_king,

    "Frostfin Chomper":             lich_king,
    "Shadow Suffusion":             lich_king,
    "Unliving Champion":            lich_king,
    "Prescience":                   lich_king,

    "Harkener of Dread":            lich_king,
    "Rotgill":                      lich_king,
    "Blightblood Berserker":        lich_king,
    "From De Other Side":           lich_king,

    "Overlord Drakuru":             lich_king,

# warlock
    "Devourer of Souls":            lich_king,
    "Nofin's Imp-ossible":          lich_king,
    "Shallow Grave":                lich_king,
    "Suspicious Peddler":           lich_king,

    "Plague Eruption":              lich_king,
    "Scourge Supplies":             lich_king,
    "Walking Dead":                 lich_king,
    "Twisted Tether":               lich_king,

    "Amorphous Slime":              lich_king,
    "Infantry Reanimator":          lich_king,
    "Savage Ymirjar":               lich_king,
    "Soul Barrage":                 lich_king,

    "Dar'Khan Drathir":             lich_king,

# warrior
    "Last Stand":                   lich_king,
    "Sunfury Champion":             lich_king,
    "Training Session":             lich_king,
    "Blazing Power":                lich_king,

    "Embers of Strength":           lich_king,
    "Asvedon, the Grandshield":     lich_king,
    "Hookfist-3000":                lich_king,
    "Light of the Phoenix":         lich_king,

    "Sunfire Smithing":             lich_king,
    "Thori'belore":                 lich_king,
    "Disruptive Spellbreaker":      lich_king,
    "Fleshshaper":                  lich_king,

    "Silverfury Stalwart":          lich_king,

# neutral
    "Arms Dealer":                  lich_king,
    "Banshee":                      lich_king,
    "Foul Egg":                     lich_king,
    "Mistake":                      lich_king,

    "Astalor Bloodsworn":           lich_king,
    "Bone Flinger":                 lich_king,
    "Coroner":                      lich_king,
    "Incorporeal Corporal":         lich_king,

    "Infected Peasant":             lich_king,
    "Umbral Geist":                 lich_king,
    "Vrykul Necrolyte":             lich_king,
    "Amber Whelp":                  lich_king,

    "Bloodied Knight":              lich_king,
    "Brittleskin Zombie":           lich_king,
    "Crystal Broker":               lich_king,
    "Darkfallen Shadow":            lich_king,

    "Drakkari Embalmer":            lich_king,
    "Enchanter":                    lich_king,
    "Hawkstrider Rancher":          lich_king,
    "Nerubian Vizier":              lich_king,

    "Scourge Rager":                lich_king,
    "Silvermoon Arcanist":          lich_king,
    "Silvermoon Sentinel":          lich_king,
    "Sunfury Clergy":               lich_king,

    "Lost Exarch":                  lich_king,
    "Plaguespreader":               lich_king,
    "Sanctum Spellbender":          lich_king,
    "Silvermoon Armorer":           lich_king,

    "Street Sweeper":               lich_king,
    "Infectious Ghoul":             lich_king,
    "Tenacious San'layn":           lich_king,
    "Translocation Instructor":     lich_king,

    "Bonelord Frostwhisper":        lich_king,
    "Rivendare, Warrider":          lich_king,
    "Shatterskin Gargoyle":         lich_king,
    "Invincible":                   lich_king,

    "Flesh Behemoth":               lich_king,
    "Thaddius, Monstrosity":        lich_king,

# death knight
    "Death Growl":                  festival,
    "Cold Feet":                    festival,
    "Dead Air":                     festival,
    "Mosh Pit":                     festival,

    "Arcanite Ripper":              festival,
    "Death Metal Knight":           festival,
    "Hardcore Cultist":             festival,
    "Harmonic Metal":               festival,

    "Cool Ghoul":                   festival,
    "Yelling Yodeler":              festival,
    "Boneshredder":                 festival,
    "Screaming Banshee":            festival,

    "Hollow Hound":                 festival,
    "Cage Head":                    festival,
    "Climactic Necrotic Explosion": festival,

# demon hunter
    "Through Fel and Flames":       festival,
    "Taste of Chaos":               festival,
    "Eye of Shadow":                festival,
    "SECURITY!!":                   festival,

    "Snakebite":                    festival,
    "Rush the Stage":               festival,
    "Tough Crowd":                  festival,
    "Glaivetar":                    festival,

    "Going Down Swinging":          festival,
    "Halveria Darkraven":           festival,
    "Instrument Smasher":           festival,
    "Rhythmdancer Risa":            festival,

    "Guitar Soloist":               festival,
    "Remixed Rhapsody":             festival,
    "Abyssal Bassist":              festival,

# druid
    "Free Spirit":                  festival,
    "Funnel Cake":                  festival,
    "Peaceful Piper":               festival,
    "Fanboy":                       festival,

    "Groovy Cat":                   festival,
    "Harmonic Mood":                festival,
    "Popular Pixie":                festival,
    "Rhythm and Roots":             festival,

    "Spread the Word":              festival,
    "Timber Tambourine":            festival,
    "Blood Treant":                 festival,
    "Summer Flowerchild":           festival,

    "Doomkin":                      festival,
    "Drum Circle":                  festival,
    "Zok Fogsnout":                 festival,

# hunter
    "Bunch of Bananas":             festival,
    "Costumed Singer":              festival,
    "Thornmantle Musician":         festival,
    "Arrow Smith":                  festival,

    "Barrel of Monkeys":            festival,
    "Hidden Meaning":               festival,
    "Harmonica Soloist":            festival,
    "Jungle Jammer":                festival,

#    "Yelling Yodeler":              festival,
    "Big Dreams":                   festival,
    "Star Power":                   festival,
#    "Hollow Hound":                 festival,

    "Mister Mukla":                 festival,
    "Stranglethorn Heart":          festival,
    "Banjosaur":                    festival,

# mage
#    "Costumed Singer":              festival,
    "Synthesize":                   festival,
    "Audio Splitter":               festival,
    "Cosmic Keyboard":              festival,

    "Infinitize the Maxitude":      festival,
    "Rewind":                       festival,
    "Fiddlefire Imp":               festival,
    "Holotechnician":               festival,

    "Lightshow":                    festival,
    "Remixed Dispense-o-bot":       festival,
    "Reverberations":               festival,
    "Keyboard Soloist":             festival,

    "Volume Up":                    festival,
#    "Star Power":                   festival,
    "DJ Manastorm":                 festival,

# paladin
    "Cold Feet":                    festival,
    "Dance Floor":                  festival,
    "Jukebox Totem":                festival,
    "Spotlight":                    festival,

    "Disco Maul":                   festival,
    "Funkfin":                      festival,
    "Starlight Groove":             festival,
    "Boogie Down":                  festival,

#    "Cool Ghoul":                   festival,
    "Jitterbug":                    festival,
    "Harmonic Disco":               festival,
    "Kangor, Dancing King":         festival,

    "Horn of the Windlord":         festival,
    "Lead Dancer":                  festival,
    "Annoy-o-Troupe":               festival,

# priest
    "Deafen":                       festival,
    "Fan Club":                     festival,
#    "Funnel Cake":                  festival,
    "Idol's Adoration":             festival,

    "Dreamboat":                    festival,
#    "Fanboy":                       festival,
    "Power Chord: Synchronize":     festival,
    "Heartthrob":                   festival,

    "Love Everlasting":             festival,
    "Plagiarizarrr":                festival,
    "Shadow Chord: Distort":        festival,
    "Ambient Lightspawn":           festival,

    "Fight Over Me":                festival,
    "Heartbreaker Hedanis":         festival,
    "Harmonic Pop":                 festival,

# rogue
    "Breakdance":                   festival,
#    "Deafen":                       festival,
    "Mixtape":                      festival,
    "Disc Jockey":                  festival,

    "Harmonic Hip Hop":             festival,
    "One Hit Wonder":               festival,
    "Beatboxer":                    festival,
    "Bounce Around (ft. Garona)":   festival,

    "Mic Drop":                     festival,
#    "Plagiarizarrr":                festival,
    "Record Scratcher":             festival,
    "Rhyme Spinner":                festival,

#    "Tough Crowd":                  festival,
#    "Rhythmdancer Risa":            festival,
    "MC Blingtron":                 festival,

# shamam
    "Melomania":                    festival,
    "Flowrider":                    festival,
    "Saxophone Soloist":            festival,
    "Jam Session":                  festival,

#    "Jukebox Totem":                festival,
    "Chill Vibes":                  festival,
    "Jazz Bass":                    festival,
    "Remixed Totemcarver":          festival,

    "Backstage Bouncer":            festival,
    "Brass Elemental":              festival,
    "Altered Chord":                festival,
    "Inzah":                        festival,

    "JIVE, INSECT!":                festival,
#    "Horn of the Windlord":         festival,
    "Pack the House":               festival,

# warlock
    "Felstring Harp":               festival,
    "Void Virtuoso":                festival,
    "Baritone Imp":                 festival,
    "Crescendo":                    festival,

    "Demonic Dynamics":             festival,
#    "Fiddlefire Imp":               festival,
#    "Reverberations":               festival,
    "Crazed Conductor":             festival,

#    "Blood Treant":                 festival,
    "Opera Soloist":                festival,
    "Rin, Orchestrator of Doom":    festival,
    "Symphony of Sins":             festival,

    "Dirge of Despair":             festival,
#    "Doomkin":                      festival,
    "Fanottem, Lord of the Opera":  festival,

# warrior
#    "Through Fel and Flames":       festival,
    "Razorfen Rockstar":            festival,
    "Verse Riff":                   festival,
#    "Jam Session":                  festival,

    "Remixed Tuning Fork":          festival,
    "Roaring Applause":             festival,
    "Chorus Riff":                  festival,
    "Power Slider":                 festival,

    "Rock Master Voone":            festival,
#    "Backstage Bouncer":            festival,
    "Kodohide Drumkit":             festival,
    "Blackrock 'n' Roll":           festival,

    "Bridge Riff":                  festival,
    "Drum Soloist":                 festival,
#    "Abyssal Bassist":              festival,

# neutral
    "Air Guitarist":                festival,
    "Annoying Fan":                 festival,
    "Crowd Surfer":                 festival,
    "Frequency Oscillator":         festival,

    "Audio Amplifier":              festival,
    "Audio Medic":                  festival,
    "Hipster":                      festival,
    "Instrument Tech":              festival,

    "Party Animal":                 festival,
    "Rolling Stone":                festival,
    "Stereo Totem":                 festival,
    "Cowbell Soloist":              festival,

    "Festival Security":            festival,
    "Metrognome":                   festival,
    "Outfit Tailor":                festival,
    "Paparazzi":                    festival,

    "Photographer Fizzle":          festival,
    "Remixed Musician":             festival,
    "Rowdy Fan":                    festival,
    "Static Waveform":              festival,

    "Worgen Roadie":                festival,
    "Candleraiser":                 festival,
    "Cover Artist":                 festival,
    "Freebird":                     festival,

    "Grimtotem Buzzkill":           festival,
    "Merch Seller":                 festival,
    "Obsessive Fan":                festival,
    "Pyrotechnician":               festival,

    "Speaker Stomper":              festival,
    "Elite Tauren Champion":        festival,
    "Ghost Writer":                 festival,
    "Magatha, Bane of Music":       festival,

    "Unpopular Has-Been":           festival,
    "The One-Amalgam Band":         festival,
    "Tony, King of Piracy":         festival,
    "Concert Promo-Drake":          festival,

    "Mish-Mash Mosher":             festival,
    "Amplified Elekk":              festival,

# death knight
    "Runes of Darkness":            titans,
    "Staff of the Primus":          titans,
    "Distressed Kvaldir":           titans,
    "Down with the Ship":           titans,

    "Frozen Over":                  titans,
    "Northern Navigation":          titans,
    "Eulogizer":                    titans,
    "Sickly Grimewalker":           titans,

    "Helya":                        titans,
    "Sinister Soulcage":            titans,
    "Tomb Traitor":                 titans,
    "The Primus":                   titans,

    "Chained Guardian":             titans,

# demon hunter
    "Crystalline Statue":           titans,
    "Runic Adornment":              titans,
    "Saronite Shambler":            titans,
    "Argunite Golem":               titans,

    "Disciple of Argus":            titans,
    "Sigil of Time":                titans,
    "Weight of the World":          titans,
    "Eredar Deceptor":              titans,

    "Mindbender":                   titans,
    "Jotun, the Eternal":           titans,
    "Momentum":                     titans,
    "Argus, the Emerald Star":      titans,

    "Mythical Terror":              titans,

# druid
    "Forbidden Fruit":              titans,
    "Forest Seedlings":             titans,
    "Embrace of Nature":            titans,
    "Lifebinder's Gift":            titans,

    "Aerosoilizer":                 titans,
    "Conservator Nymph":            titans,
    "Contaminated Lasher":          titans,
    "Frost Lotus Seedling":         titans,

    "Disciple of Eonar":            titans,
    "Ancient of Growth":            titans,
    "Cultivation":                  titans,
    "Freya, Keeper of Nature":      titans,

    "Eonar, the Life-Binder":       titans,

# hunter
    "Awakening Tremors":            titans,
    "Absorbent Parasite":           titans,
    "Always a Bigger Jormungar":    titans,
    "Bait and Switch":              titans,

    "Bestial Madness":              titans,
    "Observer of Myths":            titans,
    "Titanforged Traps":            titans,
    "Celestial Shot":               titans,

    "Fable Stablehand":             titans,
    "Twisted Frostwing":            titans,
    "Aggramar, the Avenger":        titans,
    "Starstrung Bow":               titans,

    "Hodir, Father of Giants":      titans,

# mage
    "Discovery of Magic":           titans,
    "Aqua Archivist":               titans,
    "Chill-o-matic":                titans,
    "Void Scripture":               titans,

    "Meddlesome Servant":           titans,
    "Molten Rune":                  titans,
    "Unchained Gladiator":          titans,
    "Inquisitive Creation":         titans,

    "Tainted Remnant":              titans,
    "Wisdom of Norgannon":          titans,
    "Norgannon":                    titans,
    "Sif":                          titans,

    "Elemental Inspiration":        titans,

# paladin
    "X-21 Repairbot":               titans,
    "Inventor's Aura":              titans,
    "Noble Minibot":                titans,
    "Muscle-o-Tron":                titans,

    "Alarmed Securitybot":          titans,
    "Astral Serpent":               titans,
    "Judge Unworthy":               titans,
    "Stoneheart King":              titans,

    "Disciple of Amitus":           titans,
    "Keeper's Strength":            titans,
    "Tyr's Tears":                  titans,
    "Tyr":                          titans,

    "Amitus, the Peacekeeper":      titans,

# priest
    "Astral Automaton":             titans,
    "Shadowtouched Kvaldir":        titans,
    "Creation Protocol":            titans,
    "False Disciple":               titans,

    "Twilight Torrent":             titans,
    "Grace of the Highfather":      titans,
    "Soulburner Varia":             titans,
    "Student of the Stars":         titans,

    "The Stars Align":              titans,
    "Serenity":                     titans,
    "Ra-den":                       titans,
    "Aman'Thul":                    titans,

    "Shapeless Constellation":      titans,

# rogue
    "Assembly Line":                titans,
    "Gear Shift":                   titans,
    "Tar Slick":                    titans,
    "From the Scrapheap":           titans,

    "Kaja'mite Creation":           titans,
    "Pit Stop":                     titans,
    "Tentacle Grip":                titans,
    "Coppertail Snoop":             titans,

    "Mimiron, the Mastermind":      titans,
    "SP-3Y3-D3R":                   titans,
    "Tiny Worldbreaker":            titans,
    "Lab Constructor":              titans,

    "V-07-TR-0N Prime":             titans,

# shamam
    "Lightning Reflexes":           titans,
    "Shock Hopper":                 titans,
    "Conductivity":                 titans,
    "Disciple of Golganneth":       titans,

    "Flash of Lightning":           titans,
    "Thorim, Stormlord":            titans,
    "Turn the Tides":               titans,
    "Champion of Storms":           titans,

    "Infested Watcher":             titans,
    "Tempest Hammer":               titans,
    "Crash of Thunder":             titans,
    "Golganneth, the Thunderer":    titans,

    "Thorignir Drake":              titans,

# warlock
    "Chaotic Consumption":          titans,
    "Curse of Flesh":               titans,
    "Monstrous Form":               titans,
    "Thornveil Tentacle":           titans,

    "Encroaching Insanity":         titans,
    "Forge of Wills":               titans,
    "Mortal Eradication":           titans,
    "Tentacle Tender":              titans,

    "Disciple of Sargeras":         titans,
    "Wing Welding":                 titans,
    "Loken, Jailer of Yogg-Saron":  titans,
    "Imprisoned Horror":            titans,

    "Sargeras, the Destroyer":      titans,

# warrior
    "Furious Furnace":              titans,
    "Smelt":                        titans,
    "Stoneskin Armorer":            titans,
    "Battleworn Faceless":          titans,

    "Bellowing Flames":             titans,
    "Steam Guardian":               titans,
    "Craftsman's Hammer":           titans,
    "Sanitize":                     titans,

    "Khaz'goroth":                  titans,
    "Minotauren":                   titans,
    "Trial by Fire":                titans,
    "General Vezax":                titans,

    "Odyn, Prime Designate":        titans,

# neutral
    "Ancient Totem":                titans,
    "Chaotic Tendril":              titans,
    "Drone Deconstructor":          titans,
    "Victorious Vrykul":            titans,

    "Celestial Projectionist":      titans,
    "Flame Revenant":               titans,
    "Invent-o-matic":               titans,
    "Watcher of the Sun":           titans,

    "Careless Mechanist":           titans,
    "Cyclopian Crusher":            titans,
    "Fate Splitter":                titans,
    "Mecha-Leaper":                 titans,

    "Melted Maker":                 titans,
    "Ravenous Kraken":              titans,
    "Razorscale":                   titans,
    "Relentless Worg":              titans,

    "Sharp-Eyed Seeker":            titans,
    "Starlight Whelp":              titans,
    "Trogg Exile":                  titans,
    "Algalon the Observer":         titans,

    "Angry Helhound":               titans,
    "Ignis, the Eternal Flame":     titans,
    "Imposing Anubisath":           titans,
    "Mechagnome Guide":             titans,

    "Prison Breaker":               titans,
    "Runefueled Golem":             titans,
    "Saronite Tol'vir":             titans,
    "Time-Lost Protodrake":         titans,

    "Disguised K'thir":             titans,
    "Eye of Chaos":                 titans,
    "Tram Operator":                titans,
    "XB-488 Disposalbot":           titans,

    "Cho'gall, Twilight Chieftain": titans,
    "Flame Behemoth":               titans,
    "Containment Unit":             titans,
    "Prison of Yogg-Saron":         titans,

    "Kologarn":                     titans,
    "Son of Hodir":                 titans,
    "Storm Giant":                  titans,
    "Yogg-Saron, Unleashed":        titans,

# death knight
    "Fistful of Corpses":           badlands,
    "Mining Casualties":            badlands,
    "Pile of Bones":                badlands,
    "Corpse Farm":                  badlands,

    "Crop Rotation":                badlands,
    "Farm Hand":                    badlands,
    "Mismatched Fossils":           badlands,
    "Prosthetic Hand":              badlands,

    "Reap What You Sow":            badlands,
    "Maw and Paw":                  badlands,
    "Quartzite Crusher":            badlands,
    "Skeleton Crew":                badlands,

    "Obsidian Revenant":            badlands,
    "Harrowing Ox":                 badlands,
    "Reska, the Pit Boss":          badlands,

# demon hunter
    "Burning Heart":                badlands,
    "Oasis Outlaws ":               badlands,
    "Shadestone Skulker":           badlands,
    "Bartend-O-Bot":                badlands,

    "Parched Desperado":            badlands,
    "Pocket Sand":                  badlands,
    "Quick Pick":                   badlands,
    "Load the Chamber":             badlands,

    "Snake Eyes":                   badlands,
    "Blindeye Sharpshooter":        badlands,
    "Crimson Expanse":              badlands,
    "Fan the Hammer":               badlands,

    "Fel Fissure":                  badlands,
    "Gunslinger Kurtrus":           badlands,
    "Midnight Wolf":                badlands,

# druid
    "Cactus Construct":             badlands,
    "Dragon Tales":                 badlands,
    "Rehydrate":                    badlands,
    "Splish-Splash Whelp":          badlands,

    "Pendant of Earth":             badlands,
    "Take to the Skies":            badlands,
    "Trogg Gemtosser":              badlands,
    "Desert Nestmatron":            badlands,

    "Gloomstone Guardian":          badlands,
    "Spinetail Drake":              badlands,
    "Shattered Reflections":        badlands,
    "Crystal Cluster":              badlands,

    "Dragon Golem":                 badlands,
    "Rheastrasza":                  badlands,
    "Fye, the Setting Sun":         badlands,

# hunter
    "Shimmer Shot":                 badlands,
    "Sneaky Snakes":                badlands,
    "Messenger Buzzard":            badlands,
    "Ten Gallon Hat":               badlands,

    "Bovine Skeleton":              badlands,
    "Elemental Companion":          badlands,
    "Mismatched Fossils":           badlands,
    "Saddle Up!":                   badlands,

    "Silver Serpent":               badlands,
    "Camouflage Mount":             badlands,
    "Starshooter":                  badlands,
    "Theldurin the Lost":           badlands,

    "Mantle Shaper":                badlands,
    "Spurfang":                     badlands,
    "Obsidian Revenant":            badlands,

# mage
    "Soulfreeze":                   badlands,
    "Cryopreservation":             badlands,
    "Heat Wave":                    badlands,
    "Stargazing":                   badlands,

    "Azerite Vein":                 badlands,
    "Elemental Companion":          badlands,
    "Summoning Ward":               badlands,
    "Overflow Surger":              badlands,

    "Reliquary Researcher":         badlands,
    "Tae'thelan Bloodwatcher":      badlands,
    "Mantle Shaper":                badlands,
    "Blastmage Miner":              badlands,

    "Chaos Creation":               badlands,
    "Mes'Adune the Fractured":      badlands,
    "Sunset Volley":                badlands,

# paladin
    "Hi Ho Silverwing":             badlands,
    "Lay Down the Law":             badlands,
    "Mining Casualties":            badlands,
    "Showdown!":                    badlands,

    "Deputization Aura":            badlands,
    "Holy Cowboy":                  badlands,
    "Prosthetic Hand":              badlands,
    "Shroomscavate":                badlands,

    "Sir Finley, the Intrepid":     badlands,
    "Spirit of the Badlands":       badlands,
    "Fossilized Kaleidosaur":       badlands,
    "Lawful Longarm":               badlands,

    "The Badlands Bandits":         badlands,
    "Prismatic Beam":               badlands,
    "Living Horizon":               badlands,

# priest
    "Hidden Gem":                   badlands,
    "Holy Springwater":             badlands,
    "Benevolent Banker":            badlands,
    "Injured Hauler":               badlands,

    "Pendant of Earth":             badlands,
    "Pip the Potent":               badlands,
    "Glowstone Gyreworm":           badlands,
    "Invasive Shadeleaf":           badlands,

    "Posse Possession":             badlands,
    "Tram Heist":                   badlands,
    "Shadow Word: Steal":           badlands,
    "Shattered Reflections":        badlands,

    "Swarm of Lightbugs":           badlands,
    "Thirsty Drifter":              badlands,
    "Elise, Badlands Savior":       badlands,

# rogue
    "Bloodrock Co. Shovel":         badlands,
    "Fool's Gold":                  badlands,
    "Shadestone Skulker":           badlands,
    "Stick Up":                     badlands,

    "Dart Throw":                   badlands,
    "Hidden Gem":                   badlands,
    "Quick Pick":                   badlands,
    "Shell Game":                   badlands,

    "Antique Flinger":              badlands,
    "Bounty Wrangler":              badlands,
    "Velarok Windblade":            badlands,
    "Drilly the Kid":               badlands,

    "Shadow Word: Steal":           badlands,
    "Wishing Well":                 badlands,
    "Triple Sevens":                badlands,

# shamam
    "Amphibious Elixir":            badlands,
    "Cactus Cutter":                badlands,
    "Needlerock Totem":             badlands,
    "Trusty Companion":             badlands,

    "Dehydrate":                    badlands,
    "Minecart Cruiser":             badlands,
    "Shroomscavate":                badlands,
    "Sir Finley, the Intrepid":     badlands,

    "Aftershocks":                  badlands,
    "Digging Straight Down":        badlands,
    "Doctor Holli'dae":             badlands,
    "Living Prairie":               badlands,

    "Giant Tumbleweed!!!":          badlands,
    "Skarr, the Catastrophe":       badlands,
    "Walking Mountain":             badlands,

# warlock
    "Fracking":                     badlands,
    "Smokestack":                   badlands,
    "Soulfreeze":                   badlands,
    "Disposal Assistant":           badlands,

    "Elementium Geode":             badlands,
    "Furnace Fuel":                 badlands,
    "Sludge on Wheels":             badlands,
    "Trogg Gemtosser":              badlands,

    "Trolley Problem":              badlands,
    "Gloomstone Guardian":          badlands,
    "Mo'arg Drillfist":             badlands,
    "Pop'gar the Putrid":           badlands,

    "Waste Remover":                badlands,
    "Chaos Creation":               badlands,
    "Tram Conductor Gerry":         badlands,

# warrior
    "Burning Heart":                badlands,
    "Blast Charge":                 badlands,
    "Misfire":                      badlands,
    "Needlerock Totem":             badlands,

    "Unlucky Powderman":            badlands,
    "Battlepickaxe":                badlands,
    "Reinforced Plating":           badlands,
    "Aftershocks":                  badlands,

    "Crimson Expanse":              badlands,
    "Detonation Juggernaut":        badlands,
    "Slagmaw the Slumbering":       badlands,
    "Blast Tortoise":               badlands,

    "Deepminer Brann":              badlands,
    "Badlands Brawler":             badlands,
    "Boomboss Tho'grun":            badlands,

# neutral
    "Miracle Salesman":             badlands,
    "Tram Mechanic":                badlands,
    "Bunny Stomper":                badlands,
    "Cactus Rager":                 badlands,

    "Dryscale Deputy":              badlands,
    "Flint Firearm":                badlands,
    "Gold Panner":                  badlands,
    "Greedy Partner":               badlands,

    "Horseshoe Slinger":            badlands,
    "Howdyfin":                     badlands,
    "Kobold Miner":                 badlands,
    "Saloon Brewmaster":            badlands,

    "Shale Spider":                 badlands,
    "Trapdoor Spider":              badlands,
    "Whelp Wrangler":               badlands,
    "Bounty Board":                 badlands,

    "Eroded Sediment":              badlands,
    "Gaslight Gatekeeper":          badlands,
    "Gattlesnake":                  badlands,
    "High Noon Duelist":            badlands,

    "Iridescent Gyreworm":          badlands,
    "Linedance Partner":            badlands,
    "Ogre-Gang Outlaw":             badlands,
    "Azerite Chain Gang":           badlands,

    "Dang-Blasted Elemental":       badlands,
    "Ogre-Gang Rider":              badlands,
    "Rowdy Partner":                badlands,
    "Sheriff Barrelbrim":           badlands,

    "Snake Oil Seller":             badlands,
    "Burrow Buster":                badlands,
    "Cattle Rustler":               badlands,
    "Ogre-Gang Ace":                badlands,

    "Kingpin Pud":                  badlands,
    "Stone Drake":                  badlands,
    "Sunspot Dragon":               badlands,
    "Maruut Stonebinder":           badlands,

    "Therazane":                    badlands,
    "Azerite Giant":                badlands,
    "Reno, Lone Ranger":            badlands,
    "Thunderbringer":               badlands,

# death knight
    "Arthas's Gift":                event,

# demon hunter
    "Illidan's Gift":               event,

# druid
    "Malfurion's Gift":             event,

# hunter
    "Rexxar's Gift":                event,

# mage
    "Jaina's Gift":                 event,

# paladin
    "Uther's Gift":                 event,

# priest
    "Anduin's Gift":                event,

# rogue
    "Valeera's Gift":               event,

# shamam
    "Thrall's Gift":                event,

# warlock
    "Gul'dan's Gift":               event,

# warrior
    "Garrosh's Gift":               event,

# neutral
    "Harth Stonebrew":              event,

# death knight
    "Lesser Spinel Spellstone":     whizbangs_workshop,
    "Threads of Despair":           whizbangs_workshop,
    "Shambling Zombietank":         whizbangs_workshop,
    "Silk Stitching":               whizbangs_workshop,

    "Rainbow Seamstress":           whizbangs_workshop,
    "Rambunctious Stuffy":          whizbangs_workshop,
    "Darkthorn Quilter":            whizbangs_workshop,
    "Amateur Puppeteer":            whizbangs_workshop,

    "Dr. Stitchensew":              whizbangs_workshop,
    "The Headless Horseman":        whizbangs_workshop,

# demon hunter
    "Red Card":                     whizbangs_workshop,
    "Blind Box":                    whizbangs_workshop,
    "Lesser Opal Spellstone":       whizbangs_workshop,
    "Spirit of the Team":           whizbangs_workshop,

    "Umpire's Grasp":               whizbangs_workshop,
    "Ball Hog":                     whizbangs_workshop,
    "Ci'Cigi":                      whizbangs_workshop,
    "Workshop Mishap":              whizbangs_workshop,

    "Window Shopper":               whizbangs_workshop,
    "Magtheridon, Unreleased":      whizbangs_workshop,

# druid
    "Jade Display":                 whizbangs_workshop,
    "Magical Dollhouse":            whizbangs_workshop,
    "Bottomless Toy Chest":         whizbangs_workshop,
    "Wind-Up Sapling":              whizbangs_workshop,

    "Ensmallen":                    whizbangs_workshop,
    "Sparkling Phial":              whizbangs_workshop,
    "Chia Drake":                   whizbangs_workshop,
    "Woodland Wonders":             whizbangs_workshop,

    "Sky Mother Aviana":            whizbangs_workshop,
    "Owlonius":                     whizbangs_workshop,

# hunter
    "Fetch!":                       whizbangs_workshop,
    "Jungle Gym":                   whizbangs_workshop,
    "Painted Canvasaur":            whizbangs_workshop,
    "Patchwork Pals":               whizbangs_workshop,

    "Remote Control":               whizbangs_workshop,
    "R.C. Rampage":                 whizbangs_workshop,
    "Hemet, Foam Marksman":         whizbangs_workshop,
    "Mystery Egg":                  whizbangs_workshop,

    "Toyrannosaurus":               whizbangs_workshop,
    "King Plush":                   whizbangs_workshop,

# mage
    "Hidden Objects":               whizbangs_workshop,
    "Spot the Difference":          whizbangs_workshop,
    "Triplewick Trickster":         whizbangs_workshop,
    "Watercolor Artist":            whizbangs_workshop,

    "Frost Lich Cross-Stitch":      whizbangs_workshop,
    "Sleet Skater":                 whizbangs_workshop,
    "Manufacturing Error":          whizbangs_workshop,
    "Puzzlemaster Khadgar":         whizbangs_workshop,

    "Yogg in the Box":              whizbangs_workshop,
    "The Galactic Projection Orb":  whizbangs_workshop,

# paladin
    "Fancy Packaging":              whizbangs_workshop,
    "Tigress Plushy":               whizbangs_workshop,
    "Trinket Artist":               whizbangs_workshop,
    "Cardboard Golem":              whizbangs_workshop,

    "Flash Sale":                   whizbangs_workshop,
    "Painter's Virtue":             whizbangs_workshop,
    "Toy Captain Tarim":            whizbangs_workshop,
    "Wind-Up Enforcer":             whizbangs_workshop,

    "Crafter's Aura":               whizbangs_workshop,
    "Pipsi Painthoof":              whizbangs_workshop,

# priest
    "Purifying Power":              whizbangs_workshop,
    "Scale Replica":                whizbangs_workshop,
    "Careless Crafter":             whizbangs_workshop,
    "Papercraft Angel":             whizbangs_workshop,

    "Chalk Artist":                 whizbangs_workshop,
    "Fly Off the Shelves":          whizbangs_workshop,
    "Raza the Resealed":            whizbangs_workshop,
    "Timewinder Zarimi":            whizbangs_workshop,

    "Clay Matriarch":               whizbangs_workshop,
    "Repackage":                    whizbangs_workshop,

# rogue
    "Dig for Treasure":             whizbangs_workshop,
    "Thistle Tea Set":              whizbangs_workshop,
    "Toy Boat":                     whizbangs_workshop,
    "Bargain Bin Buccaneer":        whizbangs_workshop,

    "The Crystal Cove":             whizbangs_workshop,
    "Sonya Waterdancer":            whizbangs_workshop,
    "Watercannon":                  whizbangs_workshop,
    "Sandbox Scoundrel":            whizbangs_workshop,

    "Shoplifter Goldbeard":         whizbangs_workshop,
    "Everything Must Go!":          whizbangs_workshop,

# shamam
    "Pop-Up Book":                  whizbangs_workshop,
    "Fairy Tale Forest":            whizbangs_workshop,
    "Incredible Value":             whizbangs_workshop,
    "Baking Soda Volcano":          whizbangs_workshop,

    "Sand Art Elemental":           whizbangs_workshop,
    "Hagatha the Fabled":           whizbangs_workshop,
    "Once Upon a Time...":          whizbangs_workshop,
    "Shudderblock":                 whizbangs_workshop,

    "Shining Sentinel":             whizbangs_workshop,
    "Wish Upon a Star":             whizbangs_workshop,

# warlock
    "Endgame":                      whizbangs_workshop,
    "Cursed Campaign":              whizbangs_workshop,
    "Malefic Rook":                 whizbangs_workshop,
    "Sketch Artist":                whizbangs_workshop,

    "Tabletop Roleplayer":          whizbangs_workshop,
    "Game Master Nemsy":            whizbangs_workshop,
    "Wheel of DEATH!!!":            whizbangs_workshop,
    "Wretched Queen":               whizbangs_workshop,

    "Crane Game":                   whizbangs_workshop,
    "Table Flip":                   whizbangs_workshop,

# warrior
    "Quality Assurance":            whizbangs_workshop,
    "Safety Goggles":               whizbangs_workshop,
    "Wreck'em and Deck'em":         whizbangs_workshop,
    "Boom Wrench":                  whizbangs_workshop,

    "Lab Patron":                   whizbangs_workshop,
    "Chemical Spill":               whizbangs_workshop,
    "Fireworker":                   whizbangs_workshop,
    "Testing Dummy":                whizbangs_workshop,

    "Inventor Boom":                whizbangs_workshop,
    "Botface":                      whizbangs_workshop,

# neutral
    "Zilliax Deluxe 3000":          whizbangs_workshop,
    "Corridor Sleeper":             whizbangs_workshop,
    "Giftwrapped Whelp":            whizbangs_workshop,
    "Scarab Keychain":              whizbangs_workshop,

    "Tar Slime":                    whizbangs_workshop,
    "Treasure Distributor":         whizbangs_workshop,
    "Sing-Along Buddy":             whizbangs_workshop,
    "Bucket of Soldiers":           whizbangs_workshop,

    "Card Grader":                  whizbangs_workshop,
    "Clearance Promoter":           whizbangs_workshop,
    "Cosplay Contestant":           whizbangs_workshop,
    "Floppy Hydra":                 whizbangs_workshop,

    "Messmaker":                    whizbangs_workshop,
    "Nostalgic Initiate":           whizbangs_workshop,
    "Observer of Mysteries":        whizbangs_workshop,
    "Plucky Paintfin":              whizbangs_workshop,

    "Rumble Enthusiast":            whizbangs_workshop,
    "Sweetened Snowflurry":         whizbangs_workshop,
    "Caricature Artist":            whizbangs_workshop,
    "Giggling Toymaker":            whizbangs_workshop,

    "Nesting Golem":                whizbangs_workshop,
    "Nostalgic Gnome":              whizbangs_workshop,
    "Origami Crane":                whizbangs_workshop,
    "Splendiferous Whizbang":       whizbangs_workshop,

    "Forgotten Animatronic":        whizbangs_workshop,
    "Nostalgic Clown":              whizbangs_workshop,
    "Origami Frog":                 whizbangs_workshop,
    "Workshop Janitor":             whizbangs_workshop,

    "Li'Na, Shop Manager":          whizbangs_workshop,
    "Origami Dragon":               whizbangs_workshop,
    "Wind-Up Musician":             whizbangs_workshop,
    "Colifero the Artist":          whizbangs_workshop,

    "Joymancer Jepetto":            whizbangs_workshop,
    "Factory Assemblybot":          whizbangs_workshop,
    "Playhouse Giant":              whizbangs_workshop,
}

# while true; do ./hearthstone.py; done

# edit here!
a = cards["Sharp-Eyed Seeker"]

b = cards["Uther of the Ebon Blade"]

if a < b:
    print("Left")
else:
    print("Right")
