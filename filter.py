#!/usr/bin/env python3

import subprocess
import sys

################################################################################

def write_to_clipboard(output):
    subprocess.Popen('pbcopy', env = {'LANG': 'en_US.UTF-8'},		        \
        stdin = subprocess.PIPE).communicate(output.encode('utf-8'))

# ls -l /Applications/Hearthstone/Logs/*/Hearthstone.log
filename = sys.argv[1]

f = subprocess.Popen(['tail', '-F', filename], stdout = subprocess.PIPE, 	\
    stderr = subprocess.PIPE)

str = "name="
str_len = len(str)

while True:
    line = f.stdout.readline().decode("UTF-8")

    if str in line:
        a = line.find(str) + str_len	# leading
        b = line.find(" cardId")	    # trailing
        c = line[a:b]

        write_to_clipboard(c)

        print(c)
